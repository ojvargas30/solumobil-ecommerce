import Head from 'next/head';
import { useContext, useEffect } from 'react'
import { DataContext } from '../../store/GlobalState'
import { useRouter } from 'next/router'
import { postData } from '../../utils/fetchData';

const ActiveAccount = () => {

    const { dispatch } = useContext(DataContext)
    const router = useRouter()
    const { id } = router.query
    console.log(id);

    useEffect(() => {
        if (id) {

            const activationEmail = async () => {

                const res = await postData('auth/activeaccount', { activation_token: id}) // datos de respuesta
                if (res.err == 'jwt expired') return dispatch({ type: 'NOTIFY', payload: { error: 'Tiquete vencido, recupera tu contraseña' } }) // erro del back
                if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back
                dispatch({ type: 'NOTIFY', payload: { success: res.msg } })

                router.push('/signin')
            }

            activationEmail()
        }
    }, [id])

    return (
        <>
            <Head>
                <title>Solumobil | Activación de cuenta</title>
            </Head>
        </>
    );
}

export default ActiveAccount;
