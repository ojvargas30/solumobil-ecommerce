import { getData } from '../utils/fetchData'
import Head from 'next/head'
import { useState, useEffect, useContext } from 'react';
import ProductItem from '../components/product/ProductItem'
import ModernProductCard from '../components/product/ModernProductCard'
import AdvancedTab from '../components/panelTabs/AdvancedTab'
import { DataContext } from '../store/GlobalState'
import ConfirmMsg from '../components/ConfirmMsg'
import filterSearch from '../utils/filterSearch'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { NextSeo } from 'next-seo';

// import Swiper core and required modules
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y, EffectFade, Autoplay } from 'swiper';

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// install Swiper modules
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, EffectFade, Autoplay]);

// Las props(LOS PRODUCTOS) vienen del método de abajo getServerSideProps
const Home = (props) => {


  const [products, setProducts] = useState(props.products)

  // Seleccionar y actuar sobre varios productos
  const [isCheck, setIsCheck] = useState(false);
  const [page, setPage] = useState(1)
  const router = useRouter()

  const { state, dispatch } = useContext(DataContext)
  const { auth, categories, selectedCurrency, currencies } = state;

  useEffect(() => {
    setProducts(props.products)
  }, [props.products])

  useEffect(() => {
    if (Object.keys(router.query).length === 0) setPage(1)
    // else setPage(Number(router.query.page))
  }, [router.query])

  // Confirmación de acción
  const [isOpen, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleDialogClose = () => {
    setOpen(false);
  };
  // END Confirmación de acción

  const handleCheck = id => {
    // console.log(id)

    products.forEach(product => {
      // Checked
      if (product._id === id) product.checked = !product.checked
    })

    // Actualizamos el estado
    setProducts([...products])
  };

  const handleCheckALL = () => {
    products.forEach(product => product.checked = !isCheck)
    setProducts([...products])
    setIsCheck(!isCheck)
  }

  const handleDeleteAll = () => {
    let deleteArr = []

    products.forEach(product => {
      if (product.checked) {
        deleteArr.push({
          data: '',
          id: product._id,
          title: product.title,
          type: 'INACTIVE_PRODUCT'
        })
      }
    })

    handleClickOpen()
    dispatch({
      type: 'ADD_CONFIRM',
      payload: deleteArr
    })
  }

  const handleLoadmore = () => {
    setPage(page + 1)
    filterSearch({ router, page: page + 1 })
    // console.log(router)
  }

  // const DEFAULT_SEO = {
  //   title: 'Solumobil | Tienda en línea celular Engativeña',
  //   description: 'Compra tu nuevo celular ejecutivo en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia',
  //   openGraph: {
  //     type: 'website',
  //     locale: 'es_CO',
  //     url: 'https://www.solumobil.com',
  //     title: 'Compra tu nuevo celular ejecutivo en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia',
  //     description: 'Compra tu nuevo celular ejecutivo en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia',
  //     image:
  //       'https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg',
  //     site_name: 'Solumobil Tu Movil',
  //     imageWidth: 1200,
  //     imageHeight: 1200
  //   },
  //   twitter: {
  //     handle: '@OscarVa96229360',
  //     cardType: 'Ahorra mas y lleva tu movil en línea por precios exorbitantes. Brindamos soluciones al alcance de tu bolsillo'
  //   }
  // };

  return (
    <>
      <NextSeo
        title='Solumobil | Tienda en línea celular Engativeña'
        description='Compra tu nuevo celular ejecutivo en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia'
        openGraph={{
          type: 'website',
          locale: 'es_CO',
          url: 'https://www.solumobil.com',
          title: 'Compra tu nuevo celular ejecutivo en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia',
          description: 'Compra tu nuevo celular ejecutivo en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia',
          image:
            'https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg',
          site_name: 'Solumobil Tu Movil',
          imageWidth: 1200,
          imageHeight: 1200
        }}
        twitter={{
          handle: '@OscarVa96229360',
          cardType: 'Ahorra mas y lleva tu movil en línea por precios exorbitantes. Brindamos soluciones al alcance de tu bolsillo'
        }}
      />

      <Head>
        <meta name="Keywords" lang="es" content="Celular ejecutivo en línea, Parlantes, Empanadas, Accesorios de tecnología, Estuches, Airpods, nuevas tendencias en tabletas, Engativá, Bogotá, Colombia" />
        <meta name="Description" lang="es" content="Compra tu nuevo celular ejecutivo en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia. Ahorra mas y lleva tu movil en línea por precios exorbitantes. Brindamos soluciones al alcance de tu bolsillo" />
      </Head>

      {/* ADS */}
      <Swiper
        // loop={true}
        centeredSlides={true}
        // autoplay={{
        //   delay: 500,
        // }}
        // style={{ width: "87%" }}
        effect="fade"
        spaceBetween={20}
        className="my-3 container"
        // slidesPerView={3}
        navigation
        pagination={{ clickable: true }}
        scrollbar={{ draggable: true }}
      // autoHeight={true}
      // onSlideChange={() => console.log('slide change')}
      // onSwiper={(swiper) => console.log(swiper)}
      >
        <SwiperSlide
        // style={{ height: "400px" }}
        // autoHeight={true}
        >
          {/* <div className="btn_inside_img"> */}
          <img
            className="w-100 h-100"
            src="https://res.cloudinary.com/linda-leblanc/image/upload/v1624635797/esolumobil/j9j5ifnfytaay5l30wuv.jpg"
            alt="Compra JUQU en Solumobil"
          />
          {/* <a className="btn btn-danger">Comprar ahora</a>
          </div> */}
        </SwiperSlide>

        <SwiperSlide
        // style={{ height: "400px" }}
        >
          <img className="w-100 h-100"
            src="https://res.cloudinary.com/linda-leblanc/image/upload/v1624064683/esolumobil/zxaogywwvtp5x5dvycdw.jpg" alt="Compra Xiaomi" />
        </SwiperSlide>

      </Swiper>

      {/* <Swiper
        centeredSlides={true}
        style={{ width: "87%" }}
      >
        <div className="wrap-banner style-twin-default" >
          <div className="banner-item">
            <Link href="/">
              <a className="link-banner banner-effect-1">
                <figure>
                  <img
                    // style={{ height: "160px" }}
                    src="https://res.cloudinary.com/linda-leblanc/image/upload/v1624635797/esolumobil/j9j5ifnfytaay5l30wuv.jpg" alt="" width="580" height="190" />
                </figure>
              </a>
            </Link>
          </div>
          <div className="banner-item">
            <Link href="/">
              <a className="link-banner banner-effect-1">
                <figure>
                  <img
                    // style={{ height: "160px" }}
                    src="https://res.cloudinary.com/linda-leblanc/image/upload/v1624064683/esolumobil/zxaogywwvtp5x5dvycdw.jpg" alt="" width="580" height="190" />
                </figure>
              </a>
            </Link>
          </div>
        </div>

      </Swiper> */}

      {
        auth.user && auth.user.role === 'admin' &&
        <div className="delete_all btn btn-danger mt-2" style={{ marginBottom: '-10px' }}>
          <input type="checkbox" checked={isCheck} onChange={handleCheckALL}
            style={{ width: '25px', height: '25px', transform: 'translateY(8px)' }} />

          <button className="btn btn-danger ml-2"
            onClick={handleDeleteAll}
          >
            <i className="fas fa-trash"></i>
          </button>
        </div>
      }

      {/* START MAIN */}
      <main id="main mt-5">
        <div className="container border mb-5">
          <div className="style-1 has-countdown">
            <h3 className="title-box text-white fw-bold mb-0"
              style={{ backgroundColor: "#FF2832", fontSize: "1rem", padding: "0.7rem" }}
            >
              Ofertas Engativá - Bogotá
            </h3>
            {/* <div className="wrap-countdown border-left border-right border-bottom p-1 m-0 w-25"></div> */}

            {/* <div className="products"> */}
            <Swiper
              // effect="fade"
              // className="pl-5 ml-5"
              spaceBetween={20}
              slidesPerView={3}
              navigation
              pagination={{ clickable: true }}
              scrollbar={{ draggable: true }}
            >

              {
                products.length === 0
                  ? <h2 className="p-3 m-5 text-center text-uppercase">Sin productos en Ofertas Engativá - Bogotá</h2>
                  : products.map(product => (

                    // USAR UNA FUNCIÓN DESDE OTRO COMPONENTE HIJO
                    <SwiperSlide key={product._id} className="py-4 my-4">
                      <div style={{ marginLeft: "2.1rem" }} className="slide-content">
                        <ProductItem
                          product={product}
                          handleCheck={handleCheck}
                        />
                      </div>
                    </SwiperSlide>
                  ))
              }
              {
                props.result < page * 6 ? ""
                  : <button onClick={handleLoadmore} type="button"
                    className="btn btn-outline-primary d-block mx-auto mb-4">
                    Cargar mas
                  </button>
              }
            </Swiper>

            {/* </div> */}


          </div>
        </div>
      </main>

      <div className="container border mb-5 pb-5">
        <div className="mt-0 wrap-show-advance-info-box style-1 w-100">
          <h3 className="title-box mt-0">CATEGORÍAS DE PRODUCTO</h3>
          <AdvancedTab
            products={products}
            categories={categories}
          />

          {
            props.result < page * 6 ? ""
              : <button onClick={handleLoadmore} type="button"
                className="btn btn-outline-primary d-block mx-auto mb-4">
                Cargar mas
              </button>
          }
        </div>
      </div>

      <div className="container mt-5 pt-5">

        <div className="products">
          {/* <div id="grid"> */}
          {
            products.length === 0
              ? ''
              : products.map(product => (

                // USAR UNA FUNCIÓN DESDE OTRO COMPONENTE HIJO
                <ModernProductCard
                  key={product._id}
                  product={product}
                  handleCheck={handleCheck}
                />
              ))
          }

          {/* </div> */}
        </div>


        {
          props.result < page * 6 ? ""
            : <button onClick={handleLoadmore} type="button"
              className="btn btn-outline-primary d-block mx-auto mb-4">
              Cargar mas
            </button>
        }
      </div>

      <ConfirmMsg
        isOpen={isOpen}
        handleClose={handleDialogClose}
        subtitle='¿Deseas eliminar los productos seleccionados?'
      />
    </>
  );
}

export async function getServerSideProps({ query }) { // Server side rendering -> no console.log()

  const page = query.page || 1
  const category = query.category || 'all'
  const sort = query.sort || ''
  const search = query.search || 'all'

  const res = await getData(
    `product?limit=${page * 6}&category=${category}&sort=${sort}&title=${search}`
  )

  // const productCategories = await getData(
  //   `product?category=${category}&sort=${sort}&title=${search}`
  // )
  // server side rendering
  return {
    props: {
      products: res.products,
      result: res.result
    }, // will be passed to the page component as props
  }
}

export default Home