import Head from 'next/head';
import { useState, useContext, useEffect } from 'react'
import { DataContext } from '../../store/GlobalState'
import { useRouter } from 'next/router'
import OrderDetail from '../../components/OrderDetail'

const DetailOrder = () => {

    const { state, dispatch } = useContext(DataContext),
        { orders, auth } = state,

        // Ruteador de NextJS
        router = useRouter(),

        [orderDetail, setOrderDetail] = useState([]);

    useEffect(() => {

        // Validar que el id de la url se encuentre entre los ids de las ordenes del DataProvider o estado Global
        const orderQueryUrl = orders.filter(order => order._id === router.query.id)

        setOrderDetail(orderQueryUrl) // Llenamos un arreglo con la orden

    }, [orders])

    console.log(router);

    if(!auth.user) return null;

    return (
        <>
            <Head>
                <title>Detalle de la orden</title>
            </Head>

            <div style={{paddingLeft: '1.5%', paddingTop: '1%'}}>
                <button className="btn btn-dark"
                onClick={() => router.back()}
                >
                    <i className="fas fa-long-arrow-alt-left" aria-hidden="true"></i> Volver
                </button>
            </div>

            <OrderDetail orderDetail={orderDetail} state={state} dispatch={dispatch}/>
        </>
    );
}

export default DetailOrder;
