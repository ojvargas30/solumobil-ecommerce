// NODE
// import { Product } from '../../../db/models/index'
import connectDB from '../../../utils/connectDB'
import Product from '../../../models/productModel'
import auth from '../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case "GET":
            await getProduct(req, res)
            break;
        case "PUT":
            await updateProduct(req, res)
            break;
    }
}

const updateProduct = async (req, res) => {
    try {

        const userAuth = await auth(req, res)
        if (userAuth.role !== 'admin')
            return res.status('400').json({ err: 'Autenticación Invalida' })

        const { id } = req.query

        const { title, price, inStock, desc, brand, category, images } = req.body

        if (!title || !price || !inStock || !desc ||
            category === 'all' || brand === 'all' || images.length === 0)
            res.status('400').json({ err: 'Por favor llena todos los campos.' })

        await Product.findOneAndUpdate({ _id: id }, {
            title: title.toUpperCase(), price, inStock, desc, brand, category, images
        })

        res.json({ msg: '¡Exito! Producto Actualizado' })
    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}

const getProduct = async (req, res) => {
    try {
        // Obtener el detalle del producto
        const { id } = req.query
            // , product = await Product.findByPk(id);
            , product = await Product.findById(id);
        console.log(product);

        if (!product) return res.status(400).json({ err: 'El producto no existe.' })

        res.json({ product })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}