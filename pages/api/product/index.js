// NODE
// import { Product } from '../../../db/models/index' // SEQUELIZE
import connectDB from '../../../utils/connectDB'
import Product from '../../../models/productModel'
import User from '../../../models/userModel'
import auth from '../../../middleware/auth'
import sendMail from '../../../utils/sendMail'
// import ProductHTMLEmail from '../../../components/emailHTMLTmp/ProductHTMLEmail'
import { getProductHTMLTmpl, convertCurrency, genBasicProductHTML } from '../../../utils/funcs'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case "GET":
            await getProducts(req, res)
            break;
        case "POST":
            await createProduct(req, res)
            break;
    }
}

class APIfeatures {
    constructor(query, queryString, count) {
        this.query = query;
        this.queryString = queryString;
        this.count = count;
    }

    filtering() {
        const queryObj = { ...this.queryString }

        const excludeFields = ['page', 'sort', 'limit']
        excludeFields.forEach(el => delete (queryObj[el]))

        if (queryObj.category !== 'all')
            this.query.find({ category: queryObj.category })

        if (queryObj.brand !== 'all')
            this.query.find({ brand: queryObj.brand })

        if (queryObj.title !== 'all' && queryObj.title != undefined)
            this.query.find({ title: { $regex: queryObj.title.toUpperCase() } })

        this.query.find()
        return this;
    }

    sorting() {
        if (this.queryString.sort) {
            const sortBy = this.queryString.sort.split(',').join('')
            this.query = this.query.sort(sortBy)
        } else {
            this.query = this.query.sort('-createdAt')
        }

        return this;
    }

    paginating() {
        console.log(this.count)
        const page = this.queryString.page * 1 || 1
        const limit = this.queryString.limit * 1 || parseInt(this.count)
        const skip = (page - 1) * limit;
        this.query = this.query.skip(skip).limit(limit)
        return this;
    }
}

const createProduct = async (req, res) => {
    try {

        const userAuth = await auth(req, res)

        if (userAuth.role !== 'admin')
            return res.status(400).json({ err: 'Autenticación Invalida' })

        const { title, price, inStock, desc, category, brand, images, sendEmails } = req.body

        if (!title || !price || !inStock || !desc
            || category === 'all' || !category || images.length === 0)
            return res.status('400').json({ err: 'Por favor llena todos los campos.' })

        const newProduct = new Product({
            title, price, inStock, desc, category, brand, images
        })

        const insertedProduct = await newProduct.save()
        // console.log(insertedProduct)

        if (sendEmails) {
            // const relatedProducts = await Product.find({ category, isActive: true }).limit(3)
            const relatedProducts = await Product.find({ $or: [{ category }, { isActive: true }, { brand }] }).limit(6)

            if (insertedProduct && insertedProduct !== undefined) {
                if (insertedProduct._id) {
                    // let htmlProduct = getProductHTMLTmpl(insertedProduct._id, title, content, images, relatedProducts);
                    let htmlProduct = genBasicProductHTML({
                        product: insertedProduct,
                        relatedProducts
                    })

                    // Send Mails
                    await sendMail({
                        to: 'all',
                        subject: `Aprovecha lo nuevo con ${title}`,
                        html: htmlProduct,
                    });
                }
            }
        }

        res.json({ msg: `¡Exito! Producto Creado. ${sendEmails ? "Se enviaron correos promocionales" : 'No se enviaron correos'}` })
    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}

const getProducts = async (req, res) => {
    try {
        // Obtener todos los productos
        // const products = await Product.findAll()
        // const products = await Product.find({ isActive: true })
        // const products = await Product.find()

        // console.log(req.query)

        let features;

        if (req.query.sort != 'orderall') {
            features = new APIfeatures(Product.find({ isActive: true }), req.query, Product.find({ isActive: true }).count())
                .filtering().sorting().paginating()

        } else {
            features = new APIfeatures(Product.find({ isActive: true }), req.query, Product.find({ isActive: true }).count())
                .filtering().paginating()
        }

        const products = await features.query

        // console.log(products);
        res.json({
            status: "success",
            result: products.length,
            products
        })
    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}