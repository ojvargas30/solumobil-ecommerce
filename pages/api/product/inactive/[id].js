import connectDB from '../../../../utils/connectDB'
import Product from '../../../../models/productModel'
import auth from '../../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case 'PUT':
            await softdProduct(req, res)
            break;
    }
}

const softdProduct = async (req, res) => {
    try {
        console.log('Inactivar Producto', {req, res})

        const userAuth = await auth(req, res)
        if (userAuth.role !== 'admin')
            return res.status(400).json({ err: 'Autenticación Invalida' })

        const { id } = req.query

        await Product.findByIdAndUpdate({ _id: id }, { isActive: false })

        res.json({ msg: '¡Exito! Producto(s) Eliminado(s)' })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}
