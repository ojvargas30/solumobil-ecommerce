import connectDB from '../../../utils/connectDB'
import Category from '../../../models/categoryModel'
import auth from '../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case 'PUT':
            await updateCategory(req, res)
            break;
    }
}

const updateCategory = async (req, res) => {
    try {
        const userAuth = await auth(req, res)
        if (userAuth.role !== 'admin')
            return res.status(400).json({ err: 'Autenticación Invalida' })

        const { id } = req.query
        const { name } = req.body

        const newCategory = await Category.findOneAndUpdate({ _id: id }, { name })

        res.json({
            msg: '¡Exito! Categoría Actualizada',
            category: {
                ...newCategory._doc,
                name
            }
        })
    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}
