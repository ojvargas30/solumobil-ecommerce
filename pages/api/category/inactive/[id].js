import connectDB from '../../../../utils/connectDB'
import Category from '../../../../models/categoryModel'
import Product from '../../../../models/productModel'
import auth from '../../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case 'PUT':
            await softdCategory(req, res)
            break;
    }
}

const softdCategory = async (req, res) => {
    try {

        const userAuth = await auth(req, res)
        if (userAuth.role !== 'admin')
            return res.status(400).json({ err: 'Autenticación Invalida' })

        const { id } = req.query // Id de la categoría

        const relatedProducts = await Product.findOne({ category: id })
        if (relatedProducts)
            return res.status(400).json({ err: 'Por favor elimina los productos que tienen relación con esta categoría' })

        await Category.findByIdAndUpdate({ _id: id }, { isActive: false })

        res.json({ msg: '¡Exito! Categoría Borrada' })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}
