import connectDB from '../../../utils/connectDB'
import Category from '../../../models/categoryModel'
import auth from '../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case 'POST':
            await createCategory(req, res)
            break;
        case 'GET':
            await getCategories(req, res)
            break;
    }
}

const createCategory = async (req, res) => {
    try {
        const userAuth = await auth(req, res)

        if (userAuth.role !== 'admin')
            return res.status(400).json({ err: 'Autenticación Invalida' })

        // Obtener nombre
        const { name } = req.body

        if (!name)
            return res.status(400).json({ err: 'El nombre no puede quedar vacío' })

        const category = await Category.findOne({ name })
        if (category) return res.status(400).json({ err: 'La categoría ya existe.' })

        // lA CREAMOS
        const newCategory = new Category({ name })

        await newCategory.save()

        res.json({
            msg: '¡Exito! Nueva Categoría Creada.',
            newCategory // Para actualizar la vista(con fetchData)
        })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}

const getCategories = async (req, res) => {
    try {
        const categories = await Category.find({ isActive: true })

        // console.log(categories)

        res.json({ categories }) // Para actualizar la vista(con fetchData)

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}
