import connectDB from '../../../../utils/connectDB'
import Brand from '../../../../models/categoryModel'
import Product from '../../../../models/productModel'
import auth from '../../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case 'PUT':
            await softdBrand(req, res)
            break;
    }
}

const softdBrand = async (req, res) => {
    try {

        const userAuth = await auth(req, res)
        if (userAuth.role !== 'admin')
            return res.status(400).json({ err: 'Autenticación Invalida' })

        const { id } = req.query // Id de la marca

        const relatedProducts = await Product.findOne({ brand: id })
        if (relatedProducts)
            return res.status(400).json({ err: 'Por favor elimina los productos que tienen relación con esta marca' })

        await Brand.findByIdAndUpdate({ _id: id }, { isActive: false })

        res.json({ msg: '¡Exito! Marca Borrada' })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}
