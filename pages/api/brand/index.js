import connectDB from '../../../utils/connectDB'
import Brand from '../../../models/brandModel'
import auth from '../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case 'POST':
            await createBrand(req, res)
            break;
        case 'GET':
            await getBrands(req, res)
            break;
    }
}

const createBrand = async (req, res) => {
    try {
        const userAuth = await auth(req, res)

        if (userAuth.role !== 'admin')
            return res.status(400).json({ err: 'Autenticación Invalida' })

        // Obtener nombre
        const { name, category } = req.body

        if (!name || !category || category === 'all')
            return res.status(400).json({ err: 'Completa todos los campos' })

        const brand = await Brand.findOne({ name })
        if (brand) return res.status(400).json({ err: 'La marca ya existe.' })

        // lA CREAMOS
        const newBrand = new Brand({ name, category })

        await newBrand.save()

        res.json({
            msg: '¡Exito! Nueva Marca Creada.',
            newBrand // Para actualizar la vista(con fetchData)
        })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}

const getBrands = async (req, res) => {
    try {
        const brands = await Brand.find({ isActive: true })

        // console.log(brands)

        res.json({ brands }) // Para actualizar la vista(con fetchData)

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}
