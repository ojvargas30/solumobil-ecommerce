import connectDB from '../../../utils/connectDB'
import Brand from '../../../models/brandModel'
import auth from '../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case 'PUT':
            await updateBrand(req, res)
            break;
    }
}

const updateBrand = async (req, res) => {
    try {
        const userAuth = await auth(req, res)
        if (userAuth.role !== 'admin')
            return res.status(400).json({ err: 'Autenticación Invalida' })

        const { id } = req.query
        const { name, category } = req.body

        if (!name)
            res.status('400').json({ err: 'Por favor llena el campo nombre de marca.' })

        const newBrand = await Brand.findOneAndUpdate({ _id: id }, { name, category })

        res.json({
            msg: '¡Exito! Marca Actualizada',
            brand: {
                ...newBrand._doc,
                name,
                category
            }
        })
    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}
