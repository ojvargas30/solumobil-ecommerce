// NODE
// import { User } from '../../../db/models/index' // SEQUELIZE
import connectDB from '../../../utils/connectDB'
import User from '../../../models/userModel'
import auth from '../../../middleware/auth'
import bcrypt from 'bcryptjs'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case "PATCH":
            await resetPassword(req, res)
            break;
    }
}

const resetPassword = async (req, res) => {
    try {
        // Actualizar contraseña
        const result = await auth(req, res), // Validamos que este autenticado

            { password } = req.body, // obtenemos la contraseña que quiere como nueva

            // La encriptamos
            newPassword = await bcrypt.hash(password, 12),

        // Le actualizamos la clave donde el id del usuario en sesión sea igual al de alguno de la bd
        changed = await User.findOneAndUpdate({ _id: result.id }, { password: newPassword });

        console.log(changed);

        res.json({
            msg: '¡Contraseña cambiada con exito!'
        })
    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}