// NODE
import connectDB from '../../../utils/connectDB'
import User from '../../../models/userModel'
import auth from '../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case "PATCH":
            await updateRole(req, res)
            break;
        case "PUT":
            await softdUser(req, res)
            break;
    }
}

const updateRole = async (req, res) => {
    try {
        // Actualizar rol
        const isAuth = await auth(req, res);

        if (isAuth.role !== 'admin' || !isAuth.root)
            return res.status(400).json({ err: 'Autenticación Invalida' })

        const { id } = req.query
        const { role } = req.body

        await User.findOneAndUpdate({ _id: id }, { role })

        res.json({ msg: 'Actualización Exitosa' })
    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}

const softdUser = async (req, res) => {
    try {

        const authUser = await auth(req, res) // Usuario Autenticado

        if (authUser.role !== 'admin' || !authUser.root)
            return res.status(400).json({ err: 'Autenticación Invalida' })

        const { id } = req.query

        console.log(id)

        await User.findByIdAndUpdate({ _id: id }, { isActive: false })

        res.json({ msg: 'Borrado Exitoso' })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}