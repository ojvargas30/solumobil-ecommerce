import connectDB from '../../../utils/connectDB'
import Usernewsletter from '../../../models/usernewsletterModel'
import User from '../../../models/userModel'
import { validateEmail } from '../../../utils/funcs'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case 'POST':
            await registerEmailNewsletter(req, res)
            break;
    }
}

const registerEmailNewsletter = async (req, res) => {
    try {

        const { email } = req.body

        // PENDIENTE VALIDAR EMAIL CON REGEX
        if (!email)
            return res.status(400).json({ err: 'El correo no puede quedar vacío' })

        const newsletter = await Usernewsletter.findOne({ email })
        const user = await User.findOne({ email })
        if (newsletter || user) return res.status(400).json({ err: 'El correo ya existe.' })

        const newUsernewsletter = new Usernewsletter({ email })

        await newUsernewsletter.save()

        res.json({
            msg: `¡Exito! recibiras nuestro boletín y productos nuevos al correo ${email}`,
        })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}