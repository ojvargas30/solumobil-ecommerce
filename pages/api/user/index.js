// NODE
import connectDB from '../../../utils/connectDB'
import User from '../../../models/userModel'
import auth from '../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case "PATCH":
            await uploadInfor(req, res)
            break;
        case "GET":
            await getUsers(req, res)
            break;
    }
}

const getUsers = async (req, res) => {
    try {

        const isAuth = await auth(req, res)

        if (isAuth.role !== 'admin')
            return res.status(400).json({ err: 'Autenticación Invalida' });

        const users = await User.find({isActive: true}).select('-password') // Excluimos la contraseña con el simbolo -

        res.json({ users })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}

const uploadInfor = async (req, res) => {
    try {
        // Actualizar información del perfil Ej: avatar image o nombre
        const result = await auth(req, res), // validamos que el usaurio este logueado
            { name, avatar } = req.body,
            userUpdated = await User.findOneAndUpdate({ _id: result.id }, { name, avatar }); // le ponemos el id del usuario en sesión

        res.json({
            msg: '¡Actualización exitosa!',
            user: {
                name,
                avatar,
                email: userUpdated.email,
                role: userUpdated.role
            }
        })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}