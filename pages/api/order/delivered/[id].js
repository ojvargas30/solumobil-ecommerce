import connectDB from '../../../../utils/connectDB'
import Order from '../../../../models/orderModel'
import auth from '../../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case 'PATCH':
            await deliveredOrder(req, res)
            break;
    }
}

const deliveredOrder = async (req, res) => {
    try {

        // Siempre saber si esta autenticado el usuario que hace la petición o request
        const isAuth = await auth(req, res)

        console.log(isAuth)

        // Solo el admin chequea que si se le haya entregado al cliente
        if (isAuth.role !== 'admin') return res.status(400).json({ err: 'Su autenticación es Invalida' })

        // Id de la orden para actualizar que ya fue pagada y fecha de pago ahora
        const { id } = req.query;


        const order = await Order.findOne({ _id: id })

        // SI YA FUE PAGADA
        if (order.paid) {

            // Actualizar la orden
            const orderDelivered = await Order.findOneAndUpdate({ _id: id }, { delivered: true })

            console.log(orderDelivered) // FUE ENTREGADA CON EXITO?

            return orderDelivered
                ? res.json({
                    msg: 'Actualización Satisfactoria',
                    result: {
                        paid: true,
                        delivered: true,
                        dateOfPayment: new Date(),
                        method: order.method
                    }
                })
                : res.status(400).json({ err: 'Hubo un problema con la actualización.' })

        } else {

            // Actualizar la orden
            const orderPaidAndDelivered = await Order.findOneAndUpdate({ _id: id }, {
                paid: true,
                delivered: true,
                dateOfPayment: new Date(),
                method: 'Dinero recibido'
            })

            console.log(orderPaidAndDelivered) // FUE PAGADA Y ENTREGADA CON EXITO?

            return orderPaidAndDelivered
                ? res.json({
                    msg: 'Actualización Satisfactoria',
                    result: {
                        paid: true,
                        delivered: true,
                        dateOfPayment: new Date(),
                        method: 'Dinero recibido'
                    }
                })
                : res.status(400).json({ err: 'Hubo un problema con la actualización.' })
        }

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}