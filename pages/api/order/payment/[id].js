import connectDB from '../../../../utils/connectDB'
import Order from '../../../../models/orderModel'
import User from '../../../../models/userModel'
import auth from '../../../../middleware/auth'
import sendSms from '../../../../utils/sendSms'
import sendMail from '../../../../utils/sendMail'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case 'PATCH':
            await paymentOrder(req, res)
            break;
    }
}

const paymentOrder = async (req, res) => {
    try {

        // Siempre saber si esta autenticado el usuario que hace la petición o request
        const isAuth = await auth(req, res)

        // Id de la orden para actualizar que ya fue pagada y fecha de pago ahora
        if (isAuth.role === 'user') { // El unico que puede pagar es el cliente

            const { id } = req.query,
                { paymentId } = req.body;

            // Actualizar la orden
            const orderPaid = await Order.findOneAndUpdate({ _id: id }, {
                paid: true,
                dateOfPayment: new Date(),
                paymentId,
                method: 'Paypal'
            })

            const orderClient = await User.findOne({ _id: orderPaid.user })

            // console.log(orderPaid) // FUE PAGADA CON EXITO?
            console.log(orderClient)

            let Bougthproducts = " ";

            orderPaid.cart.forEach(item => Bougthproducts += "Producto: " + item.title + " - Cantidad: " + item.quantity + " - Stock: " + item.inStock + ".       ")

            if (!orderPaid.delivered && process.env.CLIENT_URL !== "http://localhost:3000") {
                sendSms({
                    to: '573133043714',
                    text: `Orden de compra pagada y por entregar No. ${orderPaid._id}
                    desde sitio web www.solumobil.com
                    \nNombre Cliente: ${orderClient.name},
                    \nContacto: ${orderPaid.mobile},
                    \nDomicilio: ${orderPaid.address},
                    \nProductos: ${Bougthproducts ? Bougthproducts : " Error, contacte con tecnología"},
                    \nTotal: ${orderPaid.total}`
                })

                await sendMail({
                    to: 'admin',
                    subject: 'Orden de compra <strong>PAGADA</strong> y por entregar desde tu sitio web Solumobil',
                    html: `
                           <p><strong>No. de Orden: </strong> ${orderPaid._id}</p>
                           <p><strong>Cliente: </strong> ${orderClient.name}</p>
                           <p><strong>Correo Cliente: </strong> ${orderClient.email}</p>
                           <p><strong>Teléfono Cliente: </strong> ${orderPaid.mobile}</p>
                           <p><strong>Dirección Cliente: </strong> ${orderPaid.address}</p>
                           <p><strong>Total: </strong> ${orderPaid.total}</p>
                           <p><strong>Productos: </strong>  ${Bougthproducts ? Bougthproducts : " Error, contacte con tecnología"}</p>
                           `,
                });
            }

            return orderPaid
                ? res.json({ msg: '¡Pago exitoso, gracias por tu compra!' })
                : res.status(400).json({ err: 'Hubo un problema con el pago.' })
        }

        return res.status(400).json({ err: 'Autenticación Invalida' })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}