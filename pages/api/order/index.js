import connectDB from '../../../utils/connectDB'
import Order from '../../../models/orderModel'
import Product from '../../../models/productModel'
import User from '../../../models/userModel'
import auth from '../../../middleware/auth'
import sendMail from '../../../utils/sendMail'

connectDB()

export default async (req, res) => {
    switch (req.method) {
        case 'POST':
            await createOrder(req, res)
            break;
        case 'GET':
            await getOrders(req, res)
            break;
    }
}

const getOrders = async (req, res) => {
    try {

        // Traer las ordenes del usuario en sesión
        const result = await auth(req, res);

        let orders;

        if (result.role !== 'admin') {
            orders = await Order.find({ user: result.id }).populate("user", "-password") // inner join con usuario excluyendo password field
        } else {
            orders = await Order.find().populate("user", "-password")
        }

        console.log(orders)

        res.json({ orders })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}

const createOrder = async (req, res) => {
    try {

        const result = await auth(req, res),
            { address, mobile, cart, total } = req.body,

            newOrder = new Order({
                user: result.id, address, mobile, cart, total
            });

        console.log(cart)

        // console.log({ result });
        cart.filter(item => {
            return sold(item._id, item.quantity, item.inStock, item.sold)
        })

        await newOrder.save()

        const user = await User.findOne({ _id: result.id })

        if (process.env.CLIENT_URL !== "http://localhost:3000") {
            await sendMail({
                to: 'admin',
                subject: 'Nueva orden de compra desde tu sitio web Solumobil',
                html: `
                       <p><strong>No. de Orden: </strong> ${newOrder._id}</p>
                       <p><strong>Cliente: </strong> ${user.name}</p>
                       <p><strong>Correo Cliente: </strong> ${user.email}</p>
                       <p><strong>Teléfono Cliente: </strong> ${mobile}</p>
                       <p><strong>Dirección Cliente: </strong> ${address}</p>
                       `,
            });
        }

        res.json({
            msg: '¡Pedido exitoso! Nos contactaremos contigo para confirmar la orden',
            newOrder
        })
    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}

const sold = async (id, quantity, oldInStock, oldSold) => {
    await Product.findOneAndUpdate({ _id: id }, {
        inStock: oldInStock - quantity, // Actualizamos el Stock del producto de acuerdo a la cantidad comprada
        sold: quantity + oldSold // Actualizamos los que se han vendido
    })
}
