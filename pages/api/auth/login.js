// NODE
// import { User } from '../../../db/models/index'
import connectDB from '../../../utils/connectDB'
import User from '../../../models/userModel'

connectDB()

import bcrypt from 'bcryptjs'

// Para la autenticación
import { createAccessToken, createRefreshToken } from '../../../utils/createTokens'

export default async (req, res) => { // Plantilla POST
    switch (req.method) { // Método POST
        case "POST":
            await login(req, res);
            break;
    }
}

const login = async (req, res) => {
    try {
        // const { email, password } = req.body, user = await User.findOne({ where: { email } }) // Search possible matches with the email
        const { email, password } = req.body,
            user = await User.findOne({ email }) // Search possible matches with the email

        if (!user) return res.status(400).json({ err: 'El usuario no existe' })

        const isMatch = await bcrypt.compare(password, user.password) // Primero la que llega por post

        if (!isMatch) return res.status(400).json({ err: "La contraseña es incorrecta" })

        if (!user.isActive)
            return res.status(400).json({ err: 'Cuenta Inactiva.' })

        const access_token = createAccessToken({ id: user._id }),
            refresh_token = createRefreshToken({ id: user._id });

        res.json({
            msg: "Inicio de sesión exitoso",
            refresh_token,
            access_token,
            user: {
                name: user.name,
                email: user.email,
                role: user.role,
                avatar: user.avatar,
                root: user.root
            }
        })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}