// NODE
// import { User } from '../../../db/models/index'
import connectDB from '../../../utils/connectDB'
import User from '../../../models/userModel'
import valid from '../../../utils/valid'
import bcrypt from 'bcryptjs'
import { createActivateAccountToken } from '../../../utils/createTokens'
import sendMail from '../../../utils/sendMail'

// import { google } from 'googleapis'
// const { OAuth2 } = google.auth
// const OAUTH_PLAYGROUND = 'https://developers.google.com/oauthplayground'

connectDB() // Conexión base de datos MongoDB o Proximamente MySql

export default async (req, res) => { // Plantilla POST
    switch (req.method) { // Método POST
        case "POST":
            await register(req, res);
            break;
    }
}

const register = async (req, res) => {
    try {
        const { name, email, password, cf_password } = req.body,
            errMsg = valid(name, email, password, cf_password)

        if (errMsg) return res.status(400).json({ err: errMsg })

        // const user = await User.findOne({ limit: 1, where: { email } })
        const user = await User.findOne({ email })
        if (user) return res.status(400).json({ err: 'El correo ya existe.' })

        const passwordHash = await bcrypt.hash(password, 12)

        const newUser = {
            name, email, password: passwordHash
        }

        // const newUser = new User({ name, email, password: passwordHash, cf_password })

        // await newUser.save()

        // Verificación por email
        // const activation_token = createActivateAccountToken({ id: newUser._id })
        const activation_token = createActivateAccountToken(newUser)

        const activation_url = `${process.env.CLIENT_URL}/activeaccount/${activation_token}`;

        await sendMail({
            to: newUser.email,
            subject: 'Activación de cuenta Solumobil',
            html: `<div></div><h1 style="width="100%">Activa tu cuenta y disfruta de calidad en tu producto</h1><div></div><a href="${activation_url}" style="margin-bottom: 1rem;cursor: pointer;text-decoration:none;padding:1rem;background: #DC3545;color:white;">Verificar cuenta</a><p></p><div></div><p><h4>Si por alguna razón no funciono lo anterior, haz clic en el siguiente enlace:</h4></p><a target="_blank" href="${activation_url}">${activation_url}</a><p>Este tiquete vence en 5 minutos</p>`,
        });

        // newUser = await User.create({
        //     name, email, password: passwordHash, cf_password
        // }, { fields: ['name', 'email', 'password', 'cf_password'] });

        res.json({ msg: "Hemos enviado un correo de activación. Verifica tu bandeja principal o spam" })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}