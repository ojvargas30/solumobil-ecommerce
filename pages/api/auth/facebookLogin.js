// NODE
// import { User } from '../../../db/models/index'
import connectDB from '../../../utils/connectDB'
import User from '../../../models/userModel'
const fetch = require('node-fetch')

connectDB()

import bcrypt from 'bcryptjs'

// Para la autenticación
import { createAccessToken, createRefreshToken } from '../../../utils/createTokens'

export default async (req, res) => { // Plantilla POST
    switch (req.method) { // Método POST
        case "POST":
            await fbLogin(req, res);
            break;
    }
}

const fbLogin = async (req, res) => {
    try {

        const { accessToken, userID } = req.body

        const URL = `https://graph.facebook.com/v2.9/${userID}/?fields=id,name,email,picture&access_token=${accessToken}`

        const fbData = await fetch(URL).then(res => res.json()).then(res => { return res })

        const { email, name, picture } = fbData

        const password = email + process.env.FB_SECRET

        const passwordHash = await bcrypt.hash(password, 12)

        const user = await User.findOne({ email })

        if (user) {
            const isMatch = await bcrypt.compare(password, user.password) // Primero la que llega por post
            if (!isMatch) return res.status(400).json({ err: "La contraseña es incorrecta" })

            if (!user.isActive)
                return res.status(400).json({ err: 'Cuenta Inactiva.' })

            const access_token = createAccessToken({ id: user._id }),
                refresh_token = createRefreshToken({ id: user._id });

            res.json({
                msg: "Inicio de sesión exitoso",
                refresh_token,
                access_token,
                user: {
                    name: user.name,
                    email: user.email,
                    role: user.role,
                    avatar: user.avatar,
                    root: user.root
                }
            })
        } else {
            // REGISTRO DE USUARIOS
            const newUser = new User({
                name, email, password: passwordHash, avatar: picture.data ? picture.data.url : ''
            })

            await newUser.save()

            const access_token = createAccessToken({ id: newUser._id }),
                refresh_token = createRefreshToken({ id: newUser._id });

            res.json({
                msg: "Inicio de sesión exitoso",
                refresh_token,
                access_token,
                user: {
                    name: newUser.name,
                    email: newUser.email,
                    role: newUser.role,
                    avatar: newUser.avatar,
                    root: newUser.root
                }
            })
        }

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}