// NODE
import connectDB from '../../../../utils/connectDB'
import User from '../../../../models/userModel'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'

connectDB() // Conexión base de datos MongoDB o Proximamente MySql

export default async (req, res) => { // Plantilla POST
    switch (req.method) { // Método POST
        case "POST":
            await resetPassword(req, res);
            break;
    }
}

const resetPassword = async (req, res) => {
    try {
        const { recovery_token, password, cf_password } = req.body;

        if (recovery_token == undefined || recovery_token == '')
            return res.status(400).json({ err: 'Tiquete vencido' })

        if (password.length < 8)
            return res.status(400).json({ err: 'La contraseña debe tener al menos 8 caracteres' })

        if (password !== cf_password)
            return res.status(400).json({ err: 'La contraseña de confirmación no coincide' })

        const user = jwt.verify(recovery_token, process.env.ACCESS_TOKEN_SECRET)

        const passwordHash = await bcrypt.hash(password, 12)

        await User.findOneAndUpdate({ _id: user.id }, {password: passwordHash})

        res.json({ msg: "Contraseña cambiada exitosamente." })

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}