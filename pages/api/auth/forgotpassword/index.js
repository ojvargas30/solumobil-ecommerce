// NODE
import connectDB from '../../../../utils/connectDB'
import { createAccessToken } from '../../../../utils/createTokens'
import User from '../../../../models/userModel'
import sendMail from '../../../../utils/sendMail'
import jwt from 'jsonwebtoken'

connectDB() // Conexión base de datos MongoDB o Proximamente MySql

export default async (req, res) => { // Plantilla POST
    switch (req.method) { // Método POST
        case "POST":
            await recoveryEmail(req, res);
            break;
    }
}

const recoveryEmail = async (req, res) => {
    try {
        const { email } = req.body;

        const user = await User.findOne({ email })

        if (!user) return res.status(400).json({ msg: "El correo que ingresaste no existe." })

        const recovery_token = createAccessToken({ id: user._id })

        const recovery_url = `${process.env.CLIENT_URL}/forgotpassword/${recovery_token}`;

        await sendMail({
            to: email,
            subject: 'Recuperar cuenta Solumobil',
            html: `<div></div><h1 style="width="100%">Recupera tu cuenta y disfruta de calidad en tus productos</h1><div></div><a href="${recovery_url}" style="margin-bottom: 1rem;cursor: pointer;text-decoration:none;padding:1rem;background: #DC3545;color:white;">Recuperar contraseña</a><p></p><div></div><p><h4>Si por alguna razón no funciono lo anterior, haz clic en el siguiente enlace:</h4></p><a target="_blank" href="${recovery_url}">${recovery_url}</a><p>Este tiquete vence en 5 minutos</p>`,
        });

        res.json({ msg: "Hemos enviado a tu correo (bandeja principal o spam) un enlace para recuperar tu contraseña" })

    } catch (err) {
        // if(err.message == 'jwt expired')
        //     return res.status(500).json({ err: 'Verificación vencida' })

        return res.status(500).json({ err: err.message })
    }
}