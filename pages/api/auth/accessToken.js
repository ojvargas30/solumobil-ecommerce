// NODE
// import { User } from '../../../db/models/index'
import connectDB from '../../../utils/connectDB'
import User from '../../../models/userModel'
import Cookie from 'js-cookie'

connectDB()
import jwt from 'jsonwebtoken'

// Para la autenticación
import { createAccessToken } from '../../../utils/createTokens'

export default async (req, res) => { // Plantilla POST
    try {
        const rf_token = req.cookies.refreshtoken;
        if (!rf_token) return res.status(400).json({ err: "Por favor inicia sesión" })

        const result = jwt.verify(rf_token, process.env.REFRESH_TOKEN_SECRET)

        if (!result) return res.status(400).json({ err: "Su tiquete es incorrecto o ha expirado" })

        // const user = await User.findByPk(result.id)
        const user = await User.findById(result.id)
        if (!user) return res.status(400).json({ err: "El usuario no existe" })

        // const access_token = createAccessToken({ id: user.id })
        const access_token = createAccessToken({ id: user._id })
        res.json({
            access_token,
            user: {
                name: user.name,
                email: user.email,
                role: user.role,
                avatar: user.avatar,
                root: user.root
            }
        })
    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}