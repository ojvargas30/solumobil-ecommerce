// NODE
// import { User } from '../../../db/models/index'
import connectDB from '../../../utils/connectDB'
import User from '../../../models/userModel'

const { google } = require('googleapis')
const { OAuth2 } = google.auth
const client = new OAuth2(process.env.OAUTH_CLIENTID)
connectDB()

import bcrypt from 'bcryptjs'

// Para la autenticación
import { createAccessToken, createRefreshToken } from '../../../utils/createTokens'

export default async (req, res) => { // Plantilla POST
    switch (req.method) { // Método POST
        case "POST":
            await googleLogin(req, res);
            break;
    }
}

const googleLogin = async (req, res) => {
    try {

        // console.log(process.env.OAUTH_CLIENTID)

        const { tokenId } = req.body
        const verify = await client.verifyIdToken({ idToken: tokenId, audience: process.env.OAUTH_CLIENTID })
        // console.log(verify)
        const { email_verified, email, name, picture } = verify.payload

        const password = email + process.env.GOOGLE_SECRET
        const passwordHash = await bcrypt.hash(password, 12)

        if (!email_verified)
            return res.status(400).json({ err: "Verificación de correo fallida." })

        if (email_verified) {
            const user = await User.findOne({ email })

            if (user) {
                const isMatch = await bcrypt.compare(password, user.password) // Primero la que llega por post
                if (!isMatch) return res.status(400).json({ err: "La contraseña es incorrecta" })

                if (!user.isActive)
                    return res.status(400).json({ err: 'Cuenta Inactiva.' })

                const access_token = createAccessToken({ id: user._id }),
                    refresh_token = createRefreshToken({ id: user._id });

                res.json({
                    msg: "Inicio de sesión exitoso",
                    refresh_token,
                    access_token,
                    user: {
                        name: user.name,
                        email: user.email,
                        role: user.role,
                        avatar: user.avatar,
                        root: user.root
                    }
                })
            } else {
                // REGISTRO DE USUARIOS
                const newUser = new User({
                    name, email, password: passwordHash, avatar: picture
                })

                await newUser.save()

                const access_token = createAccessToken({ id: newUser._id }),
                    refresh_token = createRefreshToken({ id: newUser._id });

                res.json({
                    msg: "Inicio de sesión exitoso",
                    refresh_token,
                    access_token,
                    user: {
                        name: newUser.name,
                        email: newUser.email,
                        role: newUser.role,
                        avatar: newUser.avatar,
                        root: newUser.root
                    }
                })
            }
        }

    } catch (err) {
        return res.status(500).json({ err: err.message })
    }
}