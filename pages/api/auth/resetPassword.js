// NODE
import connectDB from '../../../utils/connectDB'
import User from '../../../models/userModel'
import jwt from 'jsonwebtoken'

connectDB() // Conexión base de datos MongoDB o Proximamente MySql

export default async (req, res) => { // Plantilla POST
    switch (req.method) { // Método POST
        case "POST":
            await activeaccount(req, res);
            break;
    }
}

const activeaccount = async (req, res) => {
    try {
        const { activation_token } = req.body;

        const user = jwt.verify(activation_token, process.env.ACTIVATE_ACCOUNT_TOKEN_SECRET)

        // if (!user)
        //     return res.status(400).json({ err: 'No se encontro tu cuenta' })

        // await User.findByIdAndUpdate({ _id: user.id }, { isActive: true })

        const { name, email, password } = user

        const check = await User.findOne({ email })
        if (check) return res.status(400).json({ msg: "El correo ya existe." })

        const newUser = new User({
            name, email, password
        })

        await newUser.save()

        res.json({ msg: "Cuenta verificada, ya puedes ingresar" })

    } catch (err) {
        // if(err.message == 'jwt expired')
        //     return res.status(500).json({ err: 'Verificación vencida' })

        return res.status(500).json({ err: err.message })
    }
}