import Head from 'next/head'
import Link from 'next/link'
import valid from '../utils/valid'
import Btn from '../components/Btn'
import { useState, useContext, useEffect } from 'react'
import { DataContext } from '../store/GlobalState'
import { postData } from '../utils/fetchData'
import { useRouter } from 'next/router'
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import Cookie from 'js-cookie'

const signup = () => {

    // Config pickup of data signup
    const initState = { name: '', email: '', password: '', cf_password: '' },
        [userData, setUserData] = useState(initState),
        { name, email, password, cf_password } = userData, // variables definition
        { state, dispatch } = useContext(DataContext),
        { auth } = state,
        router = useRouter(),

        handleChangeInput = e => {
            const { name, value } = e.target // pick up name and value of input
            setUserData({ ...userData, [name]: value }) // fill userData with names and values of inputs
            dispatch({ type: 'NOTIFY', payload: {} })
        },

        handleSubmit = async e => {
            e.preventDefault()

            const errMsg = valid(name, email, password, cf_password) // Validar campos

            if (errMsg) return dispatch({ type: 'NOTIFY', payload: { error: errMsg } }) // type(...muchos) and payload

            dispatch({ type: 'NOTIFY', payload: { loading: true } })

            const res = await postData('auth/register', userData)

            if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back


            dispatch({ type: 'NOTIFY', payload: { success: res.msg } })
        }

    useEffect(() => {
        if (Object.keys(auth).length !== 0) router.push('/')
    }, [auth])

    useEffect(() => {
        window.scrollTo(0, window.innerHeight - (window.innerHeight * 0.59));
    }, []);

    const responseGoogle = async googleResponse => {
        // console.log(googleResponse)
        try {
            dispatch({ type: 'NOTIFY', payload: { loading: true } })

            const res = await postData('auth/googleLogin', { tokenId: googleResponse.tokenId })

            if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

            dispatch({ type: 'NOTIFY', payload: { success: res.msg } })

            dispatch({ // Recepcionamos datos de respuesta
                type: 'AUTH', payload: {
                    token: res.access_token,
                    user: res.user,
                }
            })

            Cookie.set('refreshtoken', res.refresh_token, {
                path: 'api/auth/accessToken',
                expires: 7
            })

            localStorage.setItem('firstLogin', true)

        } catch (err) {
            dispatch({ type: 'NOTIFY', payload: { error: err } })
        }
    }

    const responseFacebook = async fbResp => {
        // console.log(fbResp);

        try {
            dispatch({ type: 'NOTIFY', payload: { loading: true } })

            const { accessToken, userID } = fbResp

            const res = await postData('auth/facebookLogin', { accessToken, userID })

            if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

            dispatch({ type: 'NOTIFY', payload: { success: res.msg } })

            dispatch({ // Recepcionamos datos de respuesta
                type: 'AUTH', payload: {
                    token: res.access_token,
                    user: res.user,
                }
            })

            Cookie.set('refreshtoken', res.refresh_token, {
                path: 'api/auth/accessToken',
                expires: 7
            })

            localStorage.setItem('firstLogin', true)

        } catch (err) {
            dispatch({ type: 'NOTIFY', payload: { error: err } })
        }
    }

    return (
        <div>
            <Head>
                <title>Registrarse</title>
				<meta name="Description" lang="es" content={`Registrate en Solumobil y realiza tus compras en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia. Ahorra mas y lleva tu movil en línea por precios exorbitantes. Brindamos soluciones al alcance de tu bolsillo`} />

            </Head>

            <main id="main" className="main-site left-sidebar">

                <div className="container">

                    <div className="wrap-breadcrumb">
                        <ul>
                            <li className="item-link">
                                <Link href="/">
                                    <a className="link">inicio</a>
                                </Link>
                            </li>
                            <li className="item-link">
                                <span>registrarse</span>
                            </li>
                        </ul>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-md-7">
                            <div className=" main-content-area">
                                <div className="wrap-login-item ">
                                    <div className="register-form form-item ">
                                        <form className="form-stl login_form" name="frm-login" onSubmit={handleSubmit}>

                                            <fieldset className="wrap-title">
                                                <h3 className="form-title">Crear una cuenta</h3>
                                                <h4 className="form-subtitle">Información personal</h4>
                                            </fieldset>

                                            <fieldset className="wrap-input">
                                                <label htmlFor="frm-name">Nombre</label>
                                                <input
                                                    type="text"
                                                    placeholder="Nombre"
                                                    autoComplete="name"
                                                    id="frm-name"
                                                    name="name"
                                                    value={name}
                                                    onChange={handleChangeInput}
                                                />
                                            </fieldset>

                                            <fieldset className="wrap-input">
                                                <label htmlFor="frm-email">Correo</label>
                                                <input
                                                    type="email"
                                                    id="frm-email"
                                                    name="reg-email"
                                                    placeholder="Correo"
                                                    autoComplete="email"
                                                    name="email"
                                                    value={email}
                                                    onChange={handleChangeInput}
                                                />
                                            </fieldset>

                                            {/* <fieldset className="wrap-functions ">
                                                <label className="remember-field">
                                                    <input
                                                        name="newletter"
                                                        id="new-letter"
                                                        type="checkbox"
                                                        name="name"
                                                        value={name}
                                                        onChange={handleChangeInput}
                                                    />
                                                     <span>Suscríbete al boletín</span>
                                                </label>
                                            </fieldset> */}

                                            <fieldset className="wrap-title mt-3">
                                                <h3 className="form-title">Información de ingreso</h3>
                                            </fieldset>

                                            <fieldset className="wrap-input item-width-in-half left-item ">
                                                <label htmlFor="frm-password">Contraseña</label>
                                                <input
                                                    type="password"
                                                    id="frm-password"
                                                    placeholder="Contraseña"
                                                    name="password"
                                                    minLength="8"
                                                    autoComplete="new-password"
                                                    value={password}
                                                    onChange={handleChangeInput}
                                                />
                                            </fieldset>

                                            <fieldset className="wrap-input item-width-in-half ">
                                                <label htmlFor="frm-cf_password">Confirmar Contraseña</label>
                                                <input
                                                    type="password"
                                                    id="frm-cf_password"
                                                    placeholder="Confirmar Contraseña"
                                                    name="cf_password"
                                                    minLength="8"
                                                    autoComplete="new-password"
                                                    value={cf_password}
                                                    onChange={handleChangeInput}
                                                />
                                            </fieldset>

                                            <Btn text="Registrarse" classN="text-white mt-3 bg-success" />

                                            <div className="hr">O registrarse con</div>

                                            <div className="social">
                                                <GoogleLogin
                                                    clientId={process.env.OAUTH_CLIENTID}
                                                    // clientId="Your google client id"
                                                    buttonText="Registrarse con Google"
                                                    onSuccess={responseGoogle}
                                                    // onFailure={responseGoogle}
                                                    cookiePolicy={'single_host_origin'}
                                                />

                                                <FacebookLogin
                                                    appId="525382375275458"
                                                    autoLoad={false} // true
                                                    fields="name,email,picture"
                                                    textButton="Registrarse con Facebook"
                                                    callback={responseFacebook}
                                                    className="text-center"
                                                // onClick={componentClicked}
                                                />
                                            </div>

                                            <div className="centerBtn align-items-center text-center pt-2">

                                                <span className="d-inline">¿Ya tienes cuenta? </span>

                                                <Link href="/signin"><a className="text-blue">Ingresar</a></Link>

                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    );
}

export default signup;
