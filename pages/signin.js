import Head from 'next/head'
import Link from 'next/link'
import Btn from '../components/Btn'
import Cookie from 'js-cookie'
import { useState, useContext, useEffect } from 'react'
import { DataContext } from '../store/GlobalState'
import { postData } from '../utils/fetchData'
import { useRouter } from 'next/router'
// import { useForm } from 'react-hook-form'
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login';

const signin = () => {

    // FORM EASIER
    // const { register, handleSubmit, errors, reset } = useForm
    // END FORM EASIER

    // Config pickup of data signup
    const initState = { email: '', password: '' },
        [userData, setUserData] = useState(initState),
        { email, password } = userData, // variables definition
        { state, dispatch } = useContext(DataContext),
        { auth } = state,
        router = useRouter(),

        handleChangeInput = e => {
            const { name, value } = e.target // pick up name and value of input
            setUserData({ ...userData, [name]: value }) // fill userData with names and values of inputs
            dispatch({ type: 'NOTIFY', payload: {} })
        },

        handleLogin = async e => {
            e.preventDefault()

            dispatch({ type: 'NOTIFY', payload: { loading: true } })

            const res = await postData('auth/login', userData) // datos de respuesta

            if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

            // console.log(res);
            dispatch({ type: 'NOTIFY', payload: { success: res.msg } })

            dispatch({ // Recepcionamos datos de respuesta
                type: 'AUTH', payload: {
                    token: res.access_token,
                    user: res.user,
                }
            })

            Cookie.set('refreshtoken', res.refresh_token, {
                path: 'api/auth/accessToken',
                expires: 7
            })

            localStorage.setItem('firstLogin', true)
        }

    // console.log(auth);
    useEffect(() => {
        if (Object.keys(auth).length !== 0) router.push('/')
    }, [auth])

    useEffect(() => {
        window.scrollTo(0, window.innerHeight - (window.innerHeight * 0.59));
    }, []);

    const responseGoogle = async googleResponse => {
        // console.log(googleResponse)
        try {
            dispatch({ type: 'NOTIFY', payload: { loading: true } })

            const res = await postData('auth/googleLogin', { tokenId: googleResponse.tokenId })

            if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

            dispatch({ type: 'NOTIFY', payload: { success: res.msg } })

            dispatch({ // Recepcionamos datos de respuesta
                type: 'AUTH', payload: {
                    token: res.access_token,
                    user: res.user,
                }
            })

            Cookie.set('refreshtoken', res.refresh_token, {
                path: 'api/auth/accessToken',
                expires: 7
            })

            localStorage.setItem('firstLogin', true)

        } catch (err) {
            dispatch({ type: 'NOTIFY', payload: { error: err } })
        }
    }

    const responseFacebook = async fbResp => {
        // console.log(fbResp);

        try {
            dispatch({ type: 'NOTIFY', payload: { loading: true } })

            const { accessToken, userID } = fbResp

            const res = await postData('auth/facebookLogin', { accessToken, userID })

            if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

            dispatch({ type: 'NOTIFY', payload: { success: res.msg } })

            dispatch({ // Recepcionamos datos de respuesta
                type: 'AUTH', payload: {
                    token: res.access_token,
                    user: res.user,
                }
            })

            Cookie.set('refreshtoken', res.refresh_token, {
                path: 'api/auth/accessToken',
                expires: 7
            })

            localStorage.setItem('firstLogin', true)

        } catch (err) {
            dispatch({ type: 'NOTIFY', payload: { error: err } })
        }
    }

    return (
        <div>
            <Head>
				<meta name="Description" lang="es" content={`Inicia sesión en Solumobil y realiza tus compras en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia. Ahorra mas y lleva tu movil en línea por precios exorbitantes. Brindamos soluciones al alcance de tu bolsillo`} />
                <title>Iniciar Sesión</title>
            </Head>

            {/* <!--main area--> */}
            <main id="main" className="main-site left-sidebar">

                <div className="container">

                    {/* START BREADCRUMB */}
                    <div className="wrap-breadcrumb">
                        <ul>
                            <li className="item-link">
                                <Link href="/">
                                    <a className="link">Inicio</a>
                                </Link>
                            </li>
                            <li className="item-link">
                                <span>Iniciar Sesión</span>
                            </li>
                        </ul>
                    </div>
                    {/* END BREADCRUMB */}


                    <div className="row justify-content-center">
                        <div className="col-lg-6 col-sm-6 col-md-6 col-xs-12 col-md-offset-3">
                            <div className=" main-content-area">
                                <div className="wrap-login-item ">
                                    <div className="login-form form-item form-stl">

                                        <form name="frm-login" className="login_form" onSubmit={handleLogin}>
                                            <fieldset className="wrap-title">
                                                <h3 className="form-title">INGRESE A SU CUENTA</h3>
                                            </fieldset>
                                            <fieldset className="wrap-input">
                                                <label htmlFor="frm-email">Correo:</label>
                                                <input
                                                    type="text"
                                                    autoComplete="email"
                                                    placeholder="Correo"
                                                    id="frm-email"
                                                    name="email"
                                                    value={email}
                                                    onChange={handleChangeInput}
                                                />
                                            </fieldset>

                                            <fieldset className="wrap-input">
                                                <label htmlFor="frm-password">Contraseña:</label>
                                                <input
                                                    type="password"
                                                    autoComplete="current-password"
                                                    placeholder="Contraseña"
                                                    id="frm-password"
                                                    name="password"
                                                    value={password}
                                                    onChange={handleChangeInput}
                                                />
                                            </fieldset>

                                            <fieldset className="wrap-input">
                                                {/* <label className="remember-field d-inline">
                                                    <input className="frm-input" name="rememberme"
                                                        id="rememberme" type="checkbox" />
                                                    <span>Recordarme</span>
                                                </label> */}
                                                <Link href="/forgotpassword/forgotpassword">
                                                    <a className="link-function left-position" title="¿Olvido su contraseña?">
                                                        ¿Olvidaste tu contraseña?
                                                    </a>
                                                </Link>
                                            </fieldset>

                                            <Btn text="Iniciar Sesión" classN="text-white mt-3 bg-success" />

                                            <div className="hr">O ingresar con</div>

                                            <div className="social">
                                                <GoogleLogin
                                                    clientId={process.env.OAUTH_CLIENTID}
                                                    // clientId="Your google client id"
                                                    buttonText="Ingresar con Google"
                                                    onSuccess={responseGoogle}
                                                    // onFailure={responseGoogle}
                                                    cookiePolicy={'single_host_origin'}
                                                />

                                                <FacebookLogin
                                                    appId="525382375275458"
                                                    autoLoad={false} // true
                                                    fields="name,email,picture"
                                                    textButton="Ingresar con Facebook"
                                                    callback={responseFacebook}
                                                    className="text-center"
                                                // onClick={componentClicked}
                                                />
                                            </div>

                                            <div className="centerBtn align-items-center text-center pt-2">

                                                <span className="d-inline">¿Sin cuenta? </span>

                                                <Link href="/signup">
                                                    <a className="text-blue">Registrarse</a>
                                                </Link>

                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            {/* <!--end main products area--> */}
                        </div>
                    </div>
                    {/* <!--end row--> */}

                </div>
                {/* <!--end container--> */}

            </main>
        </div>
    );
}

export default signin;
