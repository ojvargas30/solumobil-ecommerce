import Head from 'next/head';
import { useState, useContext } from 'react'
import { DataContext } from '../../store/GlobalState'
import { getData } from '../../utils/fetchData'
import { addToCart } from '../../store/Actions'
import Link from 'next/link';
import { currencyFormat, keywordsExtract } from '../../utils/funcs';
import { NextSeo } from 'next-seo';
import ShareModal from '../../components/ShareModal'

const DetailProduct = (props) => {

	const [product] = useState(props.product),
		[products] = useState(props.products),
		initState = { quantity: '1' },
		[tab, setTab] = useState(0),

		{ state, dispatch } = useContext(DataContext),
		{ cart, selectedCurrency, currencies } = state,
		[buyData, setBuyData] = useState(initState),

		handleChangeInput = e => {
			const { name, value } = e.target // pick up name and value of input
			setBuyData({ ...buyData, [name]: value }) // fill buyData with names and values of inputs
		},
		isActive = (index) => {
			if (tab === index) return " active";
			return ""
		};

	// console.log(product._id)

	const DEFAULT_SEO = {
		title: 'Solumobil | Tienda en línea celular Engativeña',
		description: 'Compra tu nuevo celular ejecutivo en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia',
		openGraph: {
			type: 'website',
			locale: 'es_CO',
			url: 'https://www.solumobil.com',
			title: 'Compra tu nuevo celular ejecutivo en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia',
			description: 'Compra tu nuevo celular ejecutivo en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia',
			image:
				'https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg',
			site_name: 'Solumobil Tu Movil',
			imageWidth: 1200,
			imageHeight: 1200
		},
		twitter: {
			handle: '@OscarVa96229360',
			cardType: 'Ahorra mas y lleva tu movil en línea por precios exorbitantes. Brindamos soluciones al alcance de tu bolsillo'
		}
	};

	// console.log(`${process.env.CLIENT_URL}/product/${product._id}`)

	return (
		<div>
			<NextSeo
				config={DEFAULT_SEO}
			/>

			<Head>
				<title>Detalles {product.title}</title>
				<meta name="Keywords" lang="es" content={keywordsExtract(product.title, 3)} />
				<meta name="Description" lang="es" content={`Compra tu nuevo ${product.title} en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia. Ahorra mas y lleva tu movil en línea por precios exorbitantes. Brindamos soluciones al alcance de tu bolsillo`} />
			</Head>

			<main id="main" className="main-site">

				<div className="container">

					<div className="wrap-breadcrumb">
						<ul>
							<li className="item-link">
								<Link href="/">
									<a className="link">inicio</a>
								</Link>
							</li>
							<li className="item-link"><span>Detalles {product.title}</span></li>
						</ul>
					</div>
					<div className="row">

						<div className="col-lg-9 col-md-8 col-sm-8 col-xs-12 main-content-area">
							<div className="wrap-product-detail">

								<div className="detail-media">
									<img
										src={product.images[tab].url}
										alt={product.images[tab].url}
										className="d-block img-thumbnail rounded mt-4 w-100"
										style={{ height: '350px' }} />

									<div className="row mx-0" style={{ cursor: 'pointer' }}>

										{
											product.images.map((img, index) => (
												<img key={index} src={img.url} alt={img.url}
													className={`img-thumbnail rounded ${isActive(index)}`}
													style={{ height: '80px', width: '20%' }}
													onClick={() => setTab(index)} />
											))
										}

									</div>
								</div>

								<div className="detail-info">
									<div className="product-rating">
										<i className="fa fa-star" aria-hidden="true"></i>
										<i className="fa fa-star" aria-hidden="true"></i>
										<i className="fa fa-star" aria-hidden="true"></i>
										<i className="fa fa-star" aria-hidden="true"></i>
										<i className="fa fa-star" aria-hidden="true"></i>
										<a href="#" className="count-review">05 revisiones</a>
									</div>
									<h2 className="product-name">{product.title}</h2>
									{/* <div className="short-desc">
										<ul>
											<li>7,9-inch LED-backlit, 130Gb</li>
											<li>Dual-core A7 with quad-core graphics</li>
											<li>FaceTime HD Camera 7.0 MP Photos</li>
										</ul>
									</div> */}
									<div className="wrap-social">
										<a className="link-socail" href="#">
											<img src="assets/images/social-list.png" alt="" />

										</a>
									</div>
									<div className="wrap-price">
										<span className="product-price">
											{
												selectedCurrency.selected == 'cop'
													? currencyFormat(product.price)
													: selectedCurrency.selected == 'usd'
														? currencyFormat((product.price / currencies.usd).toFixed(2), 'en-US', "USD")
														: ''
											}
										</span>
									</div>
									<div className="stock-info in-stock">
										<p className="availability pb-0 mb-0">Disponibilidad:
											{
												product.inStock > 0
													? <b> En Stock</b>
													: <b> Fuera de Stock</b>
											}
										</p>
										<p className="availability p-0 m-0">Vendidos:&nbsp;
											<b>
												{product.sold}
											</b>
										</p>
									</div>
									{/* <div className="quantity">
										<span>Cantidad:</span>
										<div className="quantity-input">
											<input type="text" name="quantity"
												onChange={handleChangeInput}
												value={quantity}
												data-max="120"
												max="120"
												pattern="[0-9]*" />

											<a className="btn btn-reduce"
												href="#"
											></a>
											<a className="btn btn-increase"
												href="#"
											></a>
										</div>
									</div> */}

									{
										process.env.CLIENT_URL != "http://localhost:3000"
											? <ShareModal url={`${process.env.CLIENT_URL}/product/${product._id}`} />
											: "No se puede compartir en redes en localhost"
									}

									<div className="wrap-butons">
										<button type="button"
											onClick={() => dispatch(addToCart(product, cart, 'warning'))}
											className="btn add-to-cart shadow">
											Añadir al carrito
										</button>
										{/* <div className="wrap-btn">
											<a href="#"
												className="btn btn-compare">
												<i className="fas fa-exchange-alt"></i>
												&nbsp;Añadir a lista de comparación
											</a>
											<a href="#" className="btn btn-wishlist float-none"
												style={{ textAlign: 'left !important' }}>
												<i className="fas fa-heart"></i>
												&nbsp;Añadir a lista de deseos
											</a>
										</div> */}
									</div>
								</div>
								<div className="advance-info">
									<div className="tab-control normal">
										<a href="#description" className="tab-control-item active">Descripción</a>
										{/* <a href="#add_infomation" className="tab-control-item">Información adicional</a>
										<a href="#review" className="tab-control-item">Opiniones</a> */}
									</div>
									<div className="tab-contents">
										<div className="tab-content-item active" id="description">
											{product.desc}
										</div>
										<div className="tab-content-item " id="add_infomation">
											<table className="shop_attributes">
												<tbody>
													<tr>
														<th>Peso</th><td className="product_weight">1 kg</td>
													</tr>
													<tr>
														<th>Dimensiones</th><td className="product_dimensions">12 x 15 x 23 cm</td>
													</tr>
													<tr>
														<th>Color</th><td><p>Black, Blue, Grey, Violet, Yellow</p></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div className="tab-content-item " id="review">

											<div className="wrap-review-form">

												<div id="comments">
													<h2 className="woocommerce-Reviews-title">01 review for
														<span>Radiant-360 R6 Chainsaw Omnidirectional [Orage]</span>
													</h2>
													<ol className="commentlist">
														<li className="comment byuser comment-author-admin bypostauthor even thread-even depth-1" id="li-comment-20">
															<div id="comment-20" className="comment_container">
																<img alt="" src="assets/images/author-avata.jpg" height="80" width="80" />
																<div className="comment-text">
																	<div className="star-rating">
																		<span className="width-80-percent">Rated <strong className="rating">5</strong> out of 5</span>
																	</div>
																	<p className="meta">
																		<strong className="woocommerce-review__author">admin</strong>
																		<span className="woocommerce-review__dash">–</span>
																		<time className="woocommerce-review__published-date" dateTime="2008-02-14 20:00" >Tue, Aug 15,  2017</time>
																	</p>
																	<div className="description">
																		<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
																	</div>
																</div>
															</div>
														</li>
													</ol>
												</div>

												<div id="review_form_wrapper">
													<div id="review_form">
														<div id="respond" className="comment-respond">

															<form action="#" method="post" id="commentform" className="comment-form" noValidate="">
																<p className="comment-notes">
																	<span id="email-notes">Your email address will not be published.</span> Required fields are marked <span className="required">*</span>
																</p>
																<div className="comment-form-rating">
																	<span>Your rating</span>
																	<p className="stars">

																		<label htmlFor="rated-1"></label>
																		<input type="radio" id="rated-1" name="rating"
																		// value="1"
																		/>
																		<label htmlFor="rated-2"></label>
																		<input type="radio" id="rated-2" name="rating"
																		// value="2"
																		/>
																		<label htmlFor="rated-3"></label>
																		<input type="radio" id="rated-3" name="rating"
																		// value="3"
																		/>
																		<label htmlFor="rated-4"></label>
																		<input type="radio" id="rated-4" name="rating"
																		// value="4"
																		/>
																		<label htmlFor="rated-5"></label>
																		<input type="radio" id="rated-5" name="rating"
																		// value="5"
																		// checked="checked"
																		/>
																	</p>
																</div>
																<p className="comment-form-author">
																	<label htmlFor="author">Name <span className="required">*</span></label>
																	<input id="author" name="author" type="text"
																	// value=""
																	/>
																</p>
																<p className="comment-form-email">
																	<label htmlFor="email">Email <span className="required">*</span></label>
																	<input id="email" name="email" type="email"
																	// value=""
																	/>
																</p>
																<p className="comment-form-comment">
																	<label htmlFor="comment">Your review <span className="required">*</span>
																	</label>
																	<textarea id="comment" name="comment" cols="45" rows="8"></textarea>
																</p>
																<p className="form-submit">
																	<input name="submit" type="submit" id="submit" className="submit"
																	// value="Submit"
																	/>
																</p>
															</form>

														</div>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div className="col-lg-3 col-md-4 col-sm-4 col-xs-12 sitebar">
							<div className="widget widget-our-services ">
								<div className="widget-content">
									<ul className="our-services">

										<li className="service">
											<a className="link-to-service" href="#">
												<i className="fas fa-truck" aria-hidden="true"></i>
												<div className="right-content">
													<b className="title">Envío gratis</b>
													<span className="subtitle">Solo Bogotá - Colombia</span>
													<p className="desc">Por compras mayores a 200.000 COP</p>
												</div>
											</a>
										</li>

										{/* {
											product.price >= 50000
												? <li className="service">
													<a className="link-to-service" href="#">
														<i className="fas fa-gift" aria-hidden="true"></i>
														<div className="right-content">
															<b className="title">Oferta Especial</b>
															<span className="subtitle">¡Obten unos audifonos gratis!</span>

														</div>
													</a>
												</li>
												: ''
										} */}

										<li className="service">
											<a className="link-to-service" href="#">
												<i className="fas fa-gift" aria-hidden="true"></i>
												<div className="right-content">
													<b className="title">Oferta Especial</b>
													<span className="subtitle">¡Obten unos audifonos gratis!</span>
													<p className="desc">Por compras mayores a 60 mil</p>
												</div>
											</a>
										</li>


										<li className="service">
											<a className="link-to-service" href="#">
												<i className="fas fa-reply" aria-hidden="true"></i>
												<div className="right-content">
													<b className="title">Devolución de pedido</b>
													<span className="subtitle">Por defectos de fabrica</span>
												</div>
											</a>
										</li>
									</ul>
								</div>
							</div>

							{/* <div className="widget mercado-widget widget-product">
								<h2 className="widget-title">Productos Populares</h2>
								<div className="widget-content">
									<ul className="products">
										<li className="product-item">
											<div className="product product-widget-style">
												<div className="thumbnnail">
													<a href="detail.html" title={product.title}>
														<figure>
															<img src="assets/images/products/digital_20.jpg" alt="" />
														</figure>
													</a>
												</div>
												<div className="product-info">
													<a href="#" className="product-name"><span>{product.title}</span></a>
													<div className="wrap-price">
														<span className="product-price">
															{currencyFormat(product.price)}
														</span>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div> */}

						</div>

						<div className="single-advance-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div className="wrap-show-advance-info-box style-1 box-in-site">
								<h3 className="title-box">Productos relacionados</h3>
								<div className="wrap-products mt-0 pt-0">
									<div className="products slide-carousel style-nav-1 d-flex" >

										{
											products.map(item => {
												return product.category === item.category
													? <div className="product product-style-2 d-inline" key={item._id}
														style={{ height: "405px", width: "240px" }}>
														<div className="product-thumnail" >
															<Link href={`/product/${item._id}`}>
																<a title={item.title.replace(/\b\w/g, l => l.toUpperCase())}>
																	<figure>
																		<div className="img-box">
																			<img className="img-resp"
																				style={{ height: "210px", width: "240px" }}
																				src={item.images[0].url}

																				alt={item.title.replace(/\b\w/g, l => l.toUpperCase())} />

																		</div>
																	</figure>
																</a>
															</Link>
															<div className="group-flash">
																<span className="flash-item new-label">Nuevo</span>
															</div>
															<div className="wrap-btn">
																<Link href={`/product/${item._id}`}>
																	<a className="function-link">Vista Rápida</a>
																</Link>
															</div>
														</div>
														<div className="product-info">
															<Link href={`/product/${item._id}`}>
																<a className="product-name">
																	<span>{item.title.replace(/\b\w/g, l => l.toUpperCase())}</span>
																</a>
															</Link>

															<div className="wrap-price">
																<span className="product-price">
																	{
																		selectedCurrency.selected == 'cop'
																			? currencyFormat(item.price)
																			: selectedCurrency.selected == 'usd'
																				? currencyFormat((item.price / currencies.usd).toFixed(2), 'en-US', "USD")
																				: ''
																	}
																</span>
															</div>
														</div>
													</div> : ''
											})
										}
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>

			</main>
		</div>
	);
}

export async function getServerSideProps({ params: { id }, query }) { // Server side rendering -> no console.log()

	const { product } = await getData(`product/${id}`)
	// const { products } = await getData(`product`)
	// console.log(res);
	const page = query.page || 1
	const category = query.category || 'all'
	const sort = query.sort || ''
	const search = query.search || 'all'

	const { products } = await getData(
		`product?limit=${page * 6}&category=${category}&sort=${sort}&title=${search}`
	)

	// console.log(`products`);
	// console.log(products)

	return {
		props: {
			product,
			products
			// result: res.result
		}, // will be passed to the page component as props
	}
}

export default DetailProduct;
