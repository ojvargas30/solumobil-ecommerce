import '../styles/globals.css'
// import '../styles/style.css'
import Layout from '../components/Layout';
import { DataProvider } from '../store/GlobalState'
// Import Swiper styles
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import { useRouter } from 'next/router'
import { useEffect } from 'react';
import * as gtag from '../lib/gtag'
import Head from 'next/head'

function MyApp({ Component, pageProps }) {

  const router = useRouter()

  useEffect(() => {
    const handleRouteChange = url => gtag.pageview(url)

    router.events.on('routeChangeComplete', handleRouteChange)

    return () => router.events.off('routeChangeComplete', handleRouteChange)

  }, [router.events])

  return (
    <DataProvider>
      <Layout>
        <Head>
          <meta name="viewport" lang="es" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        </Head>
        <Component {...pageProps} />
      </Layout>
    </DataProvider>
  );
}

export default MyApp
