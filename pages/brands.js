import Head from 'next/head'
import Link from 'next/link'
import { useContext, useState, useEffect } from 'react'
import { DataContext } from '../store/GlobalState'
import MTable from '../components/MTable'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { postData, putData } from '../utils/fetchData'
import { updateItem } from '../store/Actions'
import ConfirmMsg from '../components/ConfirmMsg'
import { getEsDate } from '../utils/funcs'

const Brands = () => {
    // const [name, setName] = useState('')

    const initState = {
        name: '',
        category: '',
    }

    const [brand, setBrand] = useState(initState)

    const { name, category } = brand // content

    const [cancelUpdate, setCancelUpdate] = useState('d-none')

    const { state, dispatch } = useContext(DataContext)

    const { brands, categories, auth } = state

    // Cancelar o Terminar  Actualziación
    const cancelUpdated = () => {
        // setName('')
        setId('')
        setCancelUpdate('d-none')
    }

    // Crear o editar Marca
    const createBrand = async e => {
        e.preventDefault()

        if (auth.user.role !== 'admin')
            return dispatch({ type: 'NOTIFY', payload: { error: 'Autenticación Invalida' } })

        if (!name)
            return dispatch({ type: 'NOTIFY', payload: { error: 'El nombre no puede quedar vacío' } })

        dispatch({ type: 'NOTIFY', payload: { loading: true } })

        let res;

        if (id) {
            res = await putData(`brand/${id}`, { ...brand }, auth.token)
            if (res.err) {

                if (res.err == 'jwt expired')
                    return dispatch({ type: 'NOTIFY', payload: { error: "La sesión expiro" } })

                return dispatch({ type: 'NOTIFY', payload: { error: res.err } })
            }
            dispatch(updateItem(brands, id, res.brand, 'ADD_BRANDS'))

        } else {
            res = await postData('brand', { ...brand }, auth.token)
            if (res.err) {

                if (res.err == 'jwt expired')
                    return dispatch({ type: 'NOTIFY', payload: { error: "La sesión expiro" } })

                return dispatch({ type: 'NOTIFY', payload: { error: res.err } })
            }
            dispatch({ type: 'ADD_BRANDS', payload: [...brands, res.newBrand] })
        }

        cancelUpdated()

        return dispatch({ type: 'NOTIFY', payload: { success: res.msg } })
    }

    const [id, setId] = useState('')

    // Editar Marca
    const handleEditBrand = brand => {
        setId(brand._id)
        // console.log(brand.category)
        // setName(brand.name)
        setBrand({ ...brand, ["name"]: brand.name })
        setBrand({ ...brand, ["category"]: brand.category })
        setCancelUpdate('d-inline')
    }

    // Confirmación de acción
    const [isOpen, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleDialogClose = () => {
        setOpen(false);
    };

    const handleChangeInput = e => {
        const { name, value } = e.target
        setBrand({ ...brand, [name]: value })
        dispatch({ type: 'NOTIFY', payload: {} })
    }

    useEffect(() => {
        window.scrollTo(0, window.innerHeight - (window.innerHeight * 0.59));
    }, []);


    return (
        <>
            <Head>
                <title>Solumobil | Marcas</title>
            </Head>

            <div className="container my-5">
                <div className="row justify-content-center">
                    <div className="order-summary col-md-4">
                        <div className="wrap-login-item">

                            <div className="login-form form-item form-stl">
                                <form onSubmit={createBrand}>
                                    <fieldset className="wrap-title text-center">
                                        <h1 className="form-title text-lg py-1 text-center text-uppercase">
                                            Marca
                                        </h1>
                                    </fieldset>

                                    <fieldset className="wrap-input">
                                        <label htmlFor="name">Nombre</label>

                                        <input
                                            type="text"
                                            id="name"
                                            name="name"
                                            placeholder="Nombre"
                                            className="form-control"
                                            value={name}
                                            onChange={handleChangeInput}
                                        />
                                    </fieldset>

                                    <fieldset className="wrap-input">
                                        <label htmlFor="category">Categoría</label>

                                        <select name="category" id="category"
                                            value={category}
                                            onChange={handleChangeInput}
                                            className="custom-select text-capitalize w-100 p-2 form-control">
                                            <option value="all">Seleccione...</option>
                                            {
                                                categories.map(category => (
                                                    <option key={category._id} value={category._id}>
                                                        {category.name}
                                                    </option>
                                                ))
                                            }
                                        </select>
                                    </fieldset>


                                    <div className="text-center">

                                        <button
                                            className="btn mt-3 d-inline"
                                        >
                                            {id ? 'Actualizar' : 'Guardar'}
                                        </button>

                                        <button type="button"
                                            className={`btn mt-3 mx-2 ${cancelUpdate}`}
                                            onClick={cancelUpdated}
                                        >
                                            Cancelar
                                        </button>

                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-8">
                        <MTable
                            title={<h3 className="text-uppercase">Marcas</h3>}
                            data={brands}
                            columns={[
                                {
                                    title: '#', field: '_id', defaultSort: 'desc'
                                },

                                { title: 'NOMBRE', field: 'name' },

                                {
                                    title: 'CATEGORÍA', field: 'category', render: rowData => <>

                                        {
                                            categories.map(allcategory => (
                                                <span>
                                                    {
                                                        rowData.category == allcategory._id
                                                            ? allcategory.name
                                                            : ""
                                                    }
                                                </span>
                                            ))
                                        }
                                    </>
                                },

                                // Acciones
                                {
                                    title: '', field: 'name', render: rowData => <>

                                        <EditIcon className="cursor-pointer" onClick={() => handleEditBrand(rowData)} title="Editar Marca" color="primary" />

                                        <DeleteIcon
                                            className="cursor-pointer"
                                            title="Eliminar Marca"
                                            color="secondary"
                                            onClick={() => {
                                                handleClickOpen()
                                                dispatch({
                                                    type: 'ADD_CONFIRM',
                                                    payload: [{
                                                        data: brands,
                                                        id: rowData._id,
                                                        title: rowData.name,
                                                        type: 'ADD_BRANDS'
                                                    }]
                                                })
                                            }}
                                        />

                                    </>
                                }
                            ]}

                            grouping={false}
                            detailPanel={rowData => <>
                                <table className="table table-striped table-responsive text-center">
                                    <thead>
                                        <tr>
                                            <th scope="col">Fecha creación</th>
                                            <th scope="col">Fecha actualización</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>{getEsDate(rowData.createdAt)}</th>
                                            <th>{getEsDate(rowData.updatedAt)}</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </>}
                        />

                    </div>
                </div>
            </div>

            <ConfirmMsg
                isOpen={isOpen}
                handleClose={handleDialogClose}
                subtitle='¿Quieres borrar la marca?'
            />
        </>
    );
}

export default Brands;
