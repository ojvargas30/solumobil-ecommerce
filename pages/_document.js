import Document, { Html, Head, Main, NextScript } from 'next/document';
import Footer from '../components/footer/Footer'
// import { useContext } from 'react'
// import { DataContext } from '../store/GlobalState'
import { GA_TRACKING_ID } from '../lib/gtag'

class MyDocument extends Document {

    render() {
        const currentYear = new Date().getFullYear()

        // const { dispatch } = useContext(DataContext)

        return (
            <Html lang="es">
                <Head>

                    <meta charSet="utf-8" />
                    <meta name="Author" lang="es" content="Óscar Javier Vargas Diaz, oscarjaviervargas@hotmail.com" />
                    <meta name="DC.identifier" lang="es" content="1000620103" />
                    <meta httpEquiv="Expires" lang="es" content="0" />
                    {/* <meta name="Keywords" lang="es" content="Compra tu nuevo celular ejecutivo en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia"/> */}
                    <meta httpEquiv="PICS-Label" content='(PICS-1.1 "http://www.gcf.org/v2.5"labels on "1994.11.05T08:15-0500"until "1995.12.31T23:59-0000" for "http://w3.org/PICS/Overview.html"ratings (suds 0.5 density 0 color/hue 1))'/>
                    <meta name="copyright" content={`&copy; Solumobil 2019 - ${currentYear} Todos los derechos reservados.`} />
                    {/* <meta name="Description" lang="es" content="Compra tu nuevo celular ejecutivo en línea, disfruta de las nuevas tendencias en tabletas para tus hijos o para usar tus sistemas POS de tu empresa. ¿Que esperas? Engativá - Bogotá - Colombia. Ahorra mas y lleva tu movil en línea por precios exorbitantes. Brindamos soluciones al alcance de tu bolsillo"/> */}
                    <meta name="date" content="19:05:09, sábado 29, febrero 2021 -07" />
                    <meta name="generator" content="HTML-KIT 2.9" />
                    <meta name="language" content="es" />
                    <meta name="revisit-after" content="1 month" />
                    <meta name="robots" content="index, follow" />
                    <meta name="application-name" lang="es" content="Solumobil Venta de celulares, tabletas, parlantes y tecnología." />
                    <meta name="encoding" charSet="utf-8" />
                    <meta httpEquiv="»X-UA-Compatible»." />
                    <meta name="apple-mobile-web-app-capable" content="yes" />
                    <meta httpEquiv="X-UA-Compatible" content="IE=edge, chrome=1" />
                    <meta name="organization" content="Solumobil para tu móvil, Ventas al por mayor" />
                    <meta name="revisit" content="7" />
                    <noscript>
                        <meta httpEquiv="refresh" content="30; url=https://www.youtube.com/watch?v=XyW1XiNBsaQ" />
                    </noscript>


                    {/* Bootstrap */}
                    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
                        rel="stylesheet" />

                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
                    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"></script>
                    {/* END BOOTSTRAP 5 */}

                    <link rel="stylesheet" type="text/css" href="/css/animate.css" />
                    <link rel="stylesheet" type="text/css" href="/css/owl.carousel.css" />
                    <link rel="stylesheet" type="text/css" href="/css/color-01.css" />

                    {/* GOOGLE FONT */}
                    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />

                    {/* MATERIAL REACT ICONS */}
                    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

                    <link rel="shortcut icon" type="image/x-icon" href="/media/favicon/paletaUsb.ico" />

                    <script src="/js/jquery-1.12.4.minb8ff.js?ver=1.12.4"></script>
                    <script src="/js/jquery-ui-1.12.4.minb8ff.js?ver=1.12.4"></script>
                    <script src="/js/jquery.sticky.js"></script>
                    <script src="/js/functions.js"></script>
                    <script src="/js/carousel.js"></script>

                    {/* PRODUCT CARD */}
                    <script src="/js/modernProductCard.js"></script>
                    {/* PRODUCT CARD */}

                    {/* FONT AWESOME 5 */}
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
                    {/* END FONT AWESOME 5 */}

                    {/* PAYPAL Required. Replace YOUR_CLIENT_ID with your sandbox client ID.*/}
                    <script
                        src={`https://www.paypal.com/sdk/js?client-id=${process.env.PAYPAL_CLIENT_ID}`}>
                    </script>
                    {/* END PAYPAL */}

                    {/* ADSENSE */}
                    <script data-ad-client="ca-pub-4415779182320006"
                        async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js">
                    </script>
                    {/* END ADSENSE */}

                    {/* GOOGLE ANALYTICS */}
                    {/* Global Site Tag (gtag.js) - Google Analytics */}
                    <script
                        async
                        src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}
                    />

                    <script
                        dangerouslySetInnerHTML={{
                            __html: `
                                window.dataLayer = window.dataLayer || [];
                                    function gtag(){
                                        dataLayer.push(arguments);}
                                        gtag('js', new Date());
                                        gtag('config', '${GA_TRACKING_ID}', {
                                        page_path: window.location.pathname,
                                });
                            `,
                        }}
                    />
                    {/* END GOOGLE ANALYTICS */}

                    {/* FB ADMIN */}
                    <meta name="facebook-domain-verification" content="5pvmp5rpjlsrxjpo3nizhhak9d2ein" />
                </Head>
                <body className="home-page home-01 ">

                    <Main />
                    <NextScript />

                    <Footer
                    // dispatch={dispatch}
                    />
                </body>
            </Html>
        );
    }
}

export default MyDocument