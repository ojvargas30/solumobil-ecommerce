import { useEffect } from 'react'

const hbdsofiard = () => {

    useEffect(() => {
        let wrapper = document.getElementById('wrapper');
        let button = wrapper.querySelector('button');
        let loader = document.getElementById('loader');
        let warning = document.getElementById('warning');
        let cards = wrapper.getElementsByClassName('cardsofiahbd');
        let i = 0;

        function foward() {
            if (i > cards.length - 2) return;

            cards[++i].style.transform = "translateY(-" + 390 * i + "px)";
            wrapper.style.transform = "translateY(-" + 10 * i + "px)";
            cards[i].style.opacity = 1;
            loader.style.width = 100 * i / (cards.length - 1) + "%";
        }

        function backwards() {
            if (i <= 0) return;

            cards[i].style.opacity = 1;
            cards[i].style.transform = "translateY(" + 430 * i-- / cards.length + "vh)";
            cards[i].style.opacity = 1;
            wrapper.style.transform = "translateY(-" + 2 * i + "vh)";
            loader.style.width = 100 * i / (cards.length - 1) + "%";
        }

        button.onclick = function () {
            warning.style.opacity = 0;
            warning.style.transform = "translateY(-200px)";

            window.onkeydown = function (e) {
                if (e.keyCode == 32 || e.keyCode == 40 || e.keyCode == 13 || e.keyCode == 83)
                    foward();
                if (e.keyCode == 38 || e.keyCode == 87)
                    backwards()
            }

            window.onclick = foward;
        }

        $('#start').click(function () {
            $('.candle').toggleClass('done');
            $('#dialog').fadeOut(200);
            $('#will').fadeIn(400);
        });
    })

    return (
        <>
            <div className="bodysofia">
                <div id="all">
                    <div id="loader"></div>

                    <div id='warning'>
                        <p style={{marginLeft: "1.5rem"}}>Haga clic en 'iniciar' y use las teclas de flecha para navegar (arriba y abajo)</p>
                    </div>

                    <div id='wrapper' style={{overflowX: "scroll", justifyContent: "center", alignItems: "center"}}>
                        <header className='cardsofiahbd zero'>
                            <h1>UN REGALO PARA TI</h1>
                            <button id='start' style={{color: "white"}}>INICIAR</button><br />
                        </header>

                        <div className="cardsofiahbd one" >
                            <div className='side-info'>
                                <p className="description-sofiahbd">~Felices 15 años~</p>

                                <ol>
                                    <li className="lisofiahbd">Nos festejamos con usted al celebrar su cumpleaños hoy.</li>
                                    <li className="lisofiahbd">Le deseamos más grandes logros en los próximos años.</li>
                                    <li className="lisofiahbd">Le deseamos buena salud y felicidad en la vida. </li>
                                </ol>
                            </div>

                            <p style={{color: "white", paddingTop: "3rem"}}>“Otro año lleno de aventuras te espera”</p>
                            <p style={{color: "white"}}>¡Felíz cumpleaños! Sofía</p>
                            <img src="https://media.giphy.com/media/Be9nOh8wsow8Kvt8Jx/giphy.gif"
                                height="200" width="200" />
                        </div>

                        <div className="cardsofiahbd two">
                            <div className='star' style={{ left: "20%", top: "50%" }}></div>
                            <div className='star' style={{ left: "75%", top: "20%" }}></div>
                            <div className='star' style={{ left: "85%", top: "35%" }}></div>
                            <div className='star' style={{ left: "35%", top: "18%" }}></div>
                            <div className='star' style={{ left: "70%", top: "60%" }}></div>


                            <div className='sun'>
                                <div className='face'></div>
                                <div className='shadow'></div>
                            </div>
                            <div className='textBlock bottom'>
                                <p>"Te deseo un cumpleaños muy feliz y lleno de diversión." por Óscar V. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>

    )
}

export default hbdsofiard
