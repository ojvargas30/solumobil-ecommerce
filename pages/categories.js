import Head from 'next/head'
import Link from 'next/link'
import { useContext, useState, useEffect } from 'react'
import { DataContext } from '../store/GlobalState'
import MTable from '../components/MTable'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { postData, putData } from '../utils/fetchData'
import { updateItem } from '../store/Actions'
import ConfirmMsg from '../components/ConfirmMsg'
import { getEsDate } from '../utils/funcs'

const Categories = () => {
    const [name, setName] = useState('')

    const [cancelUpdate, setCancelUpdate] = useState('d-none')

    const { state, dispatch } = useContext(DataContext)

    const { categories, auth } = state

    const [categoryStr, setCategoryStr] = useState('category')

    // Cancelar o Terminar  Actualziación
    const cancelUpdated = () => {
        setName('')
        setId('')
        setCancelUpdate('d-none')
    }

    // Crear o editar Categoría
    const createCategory = async () => {
        if (auth.user.role !== 'admin')
            return dispatch({ type: 'NOTIFY', payload: { error: 'Autenticación Invalida' } })

        if (!name)
            return dispatch({ type: 'NOTIFY', payload: { error: 'El nombre no puede quedar vacío' } })

        dispatch({ type: 'NOTIFY', payload: { loading: true } })

        let res;

        if (id) {
            res = await putData(`category/${id}`, { name }, auth.token)
            if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back
            dispatch(updateItem(categories, id, res.category, 'ADD_CATEGORIES'))

        } else {
            res = await postData('category', { name }, auth.token)
            if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back
            dispatch({ type: 'ADD_CATEGORIES', payload: [...categories, res.newCategory] })
        }

        cancelUpdated()

        return dispatch({ type: 'NOTIFY', payload: { success: res.msg } })
    }

    const [id, setId] = useState('')

    // Editar Categoría
    const handleEditCategory = (category) => {
        setId(category._id)
        setName(category.name)
        setCancelUpdate('d-inline')
    }

    // Confirmación de acción
    const [isOpen, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleDialogClose = () => {
        setOpen(false);
    };

    useEffect(() => {
        window.scrollTo(0, window.innerHeight - (window.innerHeight * 0.59));
    }, []);


    return (
        <>
            <Head>
                <title>Categorías</title>
            </Head>

            <div className="container my-5">
                <div className="row justify-content-center">
                    <div className="order-summary col-md-4">
                        <div className="wrap-login-item">

                            <div className="login-form form-item form-stl">

                                <fieldset className="wrap-title text-center">
                                    <h1 className="form-title text-lg py-1 text-center text-uppercase">
                                        Categoría
                                    </h1>
                                </fieldset>

                                <fieldset className="wrap-input">
                                    <label htmlFor="name">Nombre</label>

                                    <input
                                        type="text"
                                        id="name"
                                        placeholder="Nombre"
                                        className="form-control"
                                        value={name}
                                        onChange={e => setName(e.target.value)}
                                    />
                                </fieldset>

                                <div className="text-center">

                                    <button type="button"
                                        className="btn mt-3 d-inline"
                                        onClick={createCategory}
                                    >
                                        {id ? 'Actualizar' : 'Guardar'}
                                    </button>

                                    <button type="button"
                                        className={`btn mt-3 mx-2 ${cancelUpdate}`}
                                        onClick={cancelUpdated}
                                    >
                                        Cancelar
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-8">
                        <MTable
                            title={<h3 className="text-uppercase">Categorías</h3>}
                            data={categories}
                            columns={[
                                {
                                    title: '#', field: '_id', defaultSort: 'desc'
                                    // , render: rowData => (
                                    //     <Link href={`/${categoryStr.toString().toLowerCase()}/${rowData._id}`}>
                                    //         <a>{rowData._id}</a>
                                    //     </Link>
                                    // )
                                },

                                { title: 'NOMBRE', field: 'name' },
                                // Acciones
                                {
                                    title: '', field: 'name', render: rowData => <>

                                        <EditIcon className="cursor-pointer" onClick={() => handleEditCategory(rowData)} title="Editar Categoría" color="primary" />

                                        <DeleteIcon
                                            className="cursor-pointer"
                                            title="Eliminar Categoría"
                                            color="secondary"
                                            onClick={() => {
                                                handleClickOpen()
                                                dispatch({
                                                    type: 'ADD_CONFIRM',
                                                    payload: [{
                                                        data: categories,
                                                        id: rowData._id,
                                                        title: rowData.name,
                                                        type: 'ADD_CATEGORIES'
                                                    }]
                                                })
                                            }}
                                        />

                                    </>
                                }
                            ]}

                            grouping={false}
                            detailPanel={rowData => <>
                                <table class="table table-striped table-responsive text-center">
                                    <thead>
                                        <tr>
                                            <th scope="col">Fecha creación</th>
                                            <th scope="col">Fecha actualización</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>{getEsDate(rowData.createdAt)}</th>
                                            <th>{getEsDate(rowData.updatedAt)}</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </>}
                        />

                    </div>
                </div>
            </div>

            <ConfirmMsg
                isOpen={isOpen}
                handleClose={handleDialogClose}
                subtitle='¿Quieres borrar la categoría?'
            />
        </>
    );
}

export default Categories;
