import Head from 'next/head'
import { useState, useContext, useEffect } from 'react';
import { DataContext } from '../../store/GlobalState'
import { getData, postData, putData } from '../../utils/fetchData';
import { imageUpload } from '../../utils/imageUpload'
import { useRouter } from 'next/router'
import imageCompression from 'browser-image-compression';
// import Select from 'react-select';
// import InputLabel from '@material-ui/core/InputLabel';
// import MenuItem from '@material-ui/core/MenuItem';
// import FormHelperText from '@material-ui/core/FormHelperText';
// import FormControl from '@material-ui/core/FormControl';
// import Select from '@material-ui/core/Select';

const ProductManager = () => {

    // INICIALIZACIÓN DE ESTADOs Y CONSTANTES DE PRODUCTO
    const initState = {
        title: '',
        price: 0,
        inStock: 0,
        desc: '',
        // content: '',
        category: '',
        brand: '',
    }

    const [product, setProduct] = useState(initState)
    const [sendMailCheck, setSendMailCheck] = useState(true);
    const [shrinkImage, setShrinkImage] = useState(true);

    const { title, price, inStock, desc, category, brand } = product // content
    // FIN INICIALIZACIÓN DE ESTADO Y CONSTANTES DE PRODUCTO

    const { state, dispatch } = useContext(DataContext)

    const { categories, auth, brands } = state

    // TO EDIT PRODUCT
    const router = useRouter();
    // console.log(router)
    const { id } = router.query;
    const [onEdit, setOnEdit] = useState(false)

    useEffect(() => {
        // EDITAR SI LLEGA ID
        if (id) {
            setOnEdit(true)

            getData(`product/${id}`).then(res => {
                setProduct(res.product)
                setImages(res.product.images) // Dentro del producto
            })
        } else {
            setOnEdit(false)
            setProduct(initState)
            setImages([])
        }
    }, [id])

    const handleChangeInput = e => {
        const { name, value } = e.target
        setProduct({ ...product, [name]: value })
        dispatch({ type: 'NOTIFY', payload: {} })
    }

    // IMAGENES
    const [images, setImages] = useState([])

    const handleUploadInput = async e => {

        try {

            dispatch({ type: 'NOTIFY', payload: {} })

            let newImages = [], maxImgs = 0, err = '';

            const productImgs = [...e.target.files]

            if (productImgs.length === 0)
                return dispatch({ type: 'NOTIFY', payload: { error: 'Los archivos no existen.' } })

            productImgs.forEach(img => {
                if (img.size > (1024 * 1024) * 3)
                    return dispatch({ type: 'NOTIFY', payload: { error: 'El tamaño maximo es 3mb.' } })

                if (img.type !== 'image/jpeg' && img.type !== 'image/png')
                    return dispatch({ type: 'NOTIFY', payload: { error: 'Formato de imagen incorrecto.' } })

                maxImgs += 1

                if (maxImgs <= 5) newImages.push(img)

                return newImages
            })

            if (err) dispatch({ type: 'NOTIFY', payload: { error: err } })

            const imgCount = images.length

            if (imgCount + newImages.length > 5)
                return dispatch({ type: 'NOTIFY', payload: { error: 'Seleccione hasta 5 imágenes' } })

            if (shrinkImage) {
                // IMG COMPRESOR
                let options = {
                    maxSizeMB: 2, // 1
                    maxWidthOrHeight: 800,
                    useWebWorker: true,

                    // onProgress: Function,       // optional, a function takes one progress argument (percentage from 0 to 100)

                    // // following options are for advanced users
                    // maxIteration: number,       // optional, max number of iteration to compress the image (default: 10)
                    // exifOrientation: number,    // optional, see https://stackoverflow.com/a/32490603/10395024
                    // fileType: string,           // optional, fileType override
                    // initialQuality: number      // optional, initial quality value between 0 and 1 (default: 1)
                }


                let compressedImages = []

                // newImages.forEach(async (img, index) => {
                //     compressedImages[index] = await imageCompression(img, options)
                //     compressedImages.push(compressedImages[index])
                // })

                if (newImages[0]) {
                    let compressedImage = await imageCompression(newImages[0], options);
                    compressedImages.push(compressedImage)
                }


                if (newImages[1]) {
                    let compressedImage2 = await imageCompression(newImages[1], options);
                    compressedImages.push(compressedImage2)
                }


                if (newImages[2]) {
                    let compressedImage3 = await imageCompression(newImages[2], options);
                    compressedImages.push(compressedImage3)
                }


                if (newImages[3]) {
                    let compressedImage4 = await imageCompression(newImages[3], options);
                    compressedImages.push(compressedImage4)
                }


                if (newImages[4]) {
                    let compressedImage5 = await imageCompression(newImages[4], options);
                    compressedImages.push(compressedImage5)
                }

                if (compressedImages.length === 0)
                    return dispatch({ type: 'NOTIFY', payload: { error: 'Error de compresión. Intentalo mas tarde' } })

                setImages([...images, ...compressedImages])
                // END IMG COMPRESOR
            } else {
                setImages([...images, ...newImages])
            }

        } catch (err) {
            console.log(err);
        }
    }

    const deleteImg = index => {
        const newArr = [...images]

        // Quitamos
        newArr.splice(index, 1)

        // Actaulizamos estado de imagesnes
        setImages(newArr)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()

        // Valida que sea admin para que pued crear o editar el product
        if (!auth.user)
            return dispatch({ type: 'NOTIFY', payload: { error: 'Autenticación Invalida' } })

        if (auth.user.role !== 'admin')
            return dispatch({ type: 'NOTIFY', payload: { error: 'Autenticación Invalida' } })

        // Valida que lso datos no lleguen vacios
        if (!title || !price || !inStock || !desc || category === 'all' ||
            !category || images.length === 0)
            return dispatch({ type: 'NOTIFY', payload: { error: 'Por favor llena todos los campos.' } })

        // Cargando loader
        dispatch({ type: 'NOTIFY', payload: { loading: true } })

        let media = []
        const imgNewURL = images.filter(img => !img.url)
        const imgOldURL = images.filter(img => img.url)

        if (imgNewURL.length > 0)
            media = await imageUpload(imgNewURL) // Imagenes ya en cloudinary

        let res;

        // EDITAR
        if (onEdit) {
            res = await putData(`product/${id}`, { ...product, images: [...imgOldURL, ...media] }, auth.token)
            if (res.err) {

                if (res.err == 'jwt expired')
                    return dispatch({ type: 'NOTIFY', payload: { error: "La sesión expiro" } })

                return dispatch({ type: 'NOTIFY', payload: { error: res.err } })
            }
        } else {
            // NUEVO PRODUCTO
            res = await postData('product', { ...product, images: [...imgOldURL, ...media], sendEmails: sendMailCheck }, auth.token)
            if (res.err) {

                if (res.err == 'jwt expired')
                    return dispatch({ type: 'NOTIFY', payload: { error: "La sesión expiro" } })

                return dispatch({ type: 'NOTIFY', payload: { error: res.err } })
            }
        }

        // if(res.msg == 'cannot read property _id of undefined')
        //     return dispatch({ type: 'NOTIFY', payload: { success: 'Por favor inicia sesión de nuevo' } })

        return dispatch({ type: 'NOTIFY', payload: { success: res.msg } })

    }
    // END IMAGENES

    // const [reactSelectCategories, setReactSelectCategories] = useState()

    useEffect(() => {
        // let selectCategories = []

        // categories to react-select

        // console.log(categories)

        // categories.map(category => {
        //     selectCategories.push({ label: category.name, value: category._id, avatar: category.icon })
        // })

        // console.log(selectCategories)

        // setReactSelectCategories(selectCategories)

        // console.log(reactSelectCategories)

        window.scrollTo(0, window.innerHeight - (window.innerHeight * 0.59));
    }, []);

    return (
        <div className="products_manager p-3 m-4">
            <Head>
                <title>
                    Gestionar Producto
                </title>
            </Head>

            <form action="" className="row" onSubmit={handleSubmit}>
                <div className="col-md-6">

                    <input
                        type="text"
                        name="title"
                        value={title}
                        maxLength="300"
                        placeholder="Título"
                        className="d-block my-4 w-100 p-2  form-control"
                        onChange={handleChangeInput}
                        onInput={e => e.target.value = e.target.value.toUpperCase()}
                    />

                    <div className="row">
                        <div className="col-sm-6">
                            <label htmlFor="price">Precio (COP)</label>
                            <input
                                type="number"
                                name="price"
                                value={price}
                                placeholder="Precio"
                                className="p-2 w-100  form-control"
                                onChange={handleChangeInput}
                            />
                        </div>
                        <div className="col-sm-6">
                            <label htmlFor="inStock">En Stock</label>
                            <input
                                type="number"
                                name="inStock"
                                value={inStock}
                                placeholder="Stock"
                                className="p-2 w-100  form-control"
                                onChange={handleChangeInput}
                            />
                        </div>
                    </div>

                    <textarea
                        name="desc"
                        id="desc"
                        value={desc}
                        cols="30"
                        rows="4"
                        placeholder="Descripción"
                        onChange={handleChangeInput}
                        className="d-block my-4 w-100 p-2 form-control"
                    />

                    <div className="input-group-prepend px-0 mb-2 d-block w-100 p-2">

                        <select name="category" id="category"
                            value={category}
                            onChange={handleChangeInput}
                            className="custom-select text-capitalize w-100 p-2 mb-3 form-control">
                            <option value="all">Seleccione Categoría...</option>
                            {
                                categories.map(category => (
                                    <option key={category._id} value={category._id}>
                                        {category.name}
                                    </option>
                                ))
                            }
                        </select>

                        <select name="brand" id="brand"
                            value={brand}
                            onChange={handleChangeInput}
                            className="custom-select text-capitalize w-100 p-2 form-control">
                            <option value="all">Seleccione Marca...</option>
                            {
                                brands.map(brand => (
                                    <option key={brand._id} value={brand._id}>
                                        {brand.name}
                                    </option>
                                ))
                            }

                        </select>

                        {
                            !onEdit
                                ? <div className="form-check">
                                    <input className="form-check-input "
                                        type="checkbox"
                                        id="sendEmails"
                                        checked={sendMailCheck}
                                        defaultValue={sendMailCheck}
                                        onChange={() => setSendMailCheck(!sendMailCheck)}
                                    />
                                    <label className="form-check-label" htmlFor="sendEmails">
                                        Enviar correos
                                    </label>
                                </div>
                                : ""
                        }


                        <div className="form-check mb-4">
                            <input className="form-check-input "
                                type="checkbox"
                                id="shrinkImage"
                                checked={shrinkImage}
                                defaultValue={shrinkImage}
                                onChange={() => setShrinkImage(!shrinkImage)}
                            />
                            <label className="form-check-label" htmlFor="shrinkImage">
                                Comprimir imágenes
                            </label>
                        </div>

                        <button className="btn btn-primary my-2 px-4">
                            {
                                onEdit
                                    ? <><i className="fas fa-edit"></i> Editar</>
                                    : <><i className="fas fa-plus"></i> Guardar</>
                            }
                        </button>


                    </div>
                </div>

                <div className="col-md-6 my-4">

                    <div className="mb-3">
                        {/* <label htmlFor="productImgs" className="form-label">Cargar Imágenes</label> */}
                        <input className="form-control" type="file" id="productImgs"
                            onChange={handleUploadInput} multiple accept="image/*" />
                        <span className="text-secondary text-sm">Preferiblemente sube .png con fondo transparente</span>
                    </div>

                    <div className="row img-up mx-0">
                        {
                            images.map((img, index) => (
                                <div key={index} className="file_img my-1">
                                    <img src={img.url ? img.url : URL.createObjectURL(img)}
                                        alt={img.url ? img.url : URL.createObjectURL(img)}
                                        className="img-thumbnail rounded" />

                                    <span onClick={() => deleteImg(index)}>X</span>
                                </div>
                            ))
                        }
                    </div>

                </div>

            </form>
        </div>
    );
}

export default ProductManager;
