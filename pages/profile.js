import { useContext, useState, useEffect } from 'react'
import { DataContext } from '../store/GlobalState'
import Head from 'next/head'
// import Btn from '../components/Btn'
import valid from '../utils/valid'
import { patchData } from '../utils/fetchData'
import { imageUpload } from '../utils/imageUpload'
// import CollapseTable from '../components/CollapseTable'
// import TableTrap from '../components/TableTrap'
import MTable from '../components/MTable'
import Link from 'next/link'
// import ArrowForwardIcon from '@material-ui/icons/ArrowForward'
// import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward'
// import Fab from '@material-ui/core/Fab'
import { getEsDate, currencyFormat } from '../utils/funcs.js'
import imageCompression from 'browser-image-compression';

const profile = () => {

    // Estado inicial campos controlados
    const initialState = {
        avatar: '',
        name: '',
        password: '',
        cf_password: ''
    }

    const [data, setData] = useState(initialState);
    const { avatar, name, password, cf_password } = data;

    // Requerimos el estado global para tomar información del usuario en sesión
    const { state, dispatch } = useContext(DataContext);
    const { auth, notify, orders } = state;

    // Guardamos la info de lso campos si hay usuario logueado
    useEffect(() => {
        console.log(orders)

        if (auth.user) setData({ ...data, name: auth.user.name }) // Guardar el nombre del usuario en el estado con el nombre que tiene el usuario en sesión
    }, [auth.user])

    // Tener inputs controlados
    const handleChange = e => {
        const { name, value } = e.target
        setData({ ...data, [name]: value }) // Cada vez que se modifica el valor del input le coge el name y el value y lo pone en donde corresponde en el estado

        dispatch({ type: 'NOTIFY', payload: {} })
    }

    // Actualizar perfil
    const handleProfileUpdate = e => {
        e.preventDefault()

        if (password) {
            const errMsg = valid(name, auth.user.email, password, cf_password)

            if (errMsg) return dispatch({ type: 'NOTIFY', payload: { error: errMsg } })

            updatePassword() // Uso
        }

        if (auth.user.name != name || avatar) updateInfor()
    }

    // Método para Actualizar contraseña de usuario
    const updatePassword = () => {
        dispatch({ type: 'NOTIFY', payload: { loading: true } })

        // Se le pasa el token que tiene el usuario en sesión auth.token
        patchData('user/resetPassword', { password }, auth.token)
            .then(res => {
                if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } })

                return dispatch({ type: 'NOTIFY', payload: { success: res.msg } })
            })
    }

    // Cambiar foto del avatar con validaciones
    const changeAvatar = async e => {
        const userPhoto = e.target.files[0]

        if (!userPhoto)
            return dispatch({ type: 'NOTIFY', payload: { error: 'La imagen no existe' } })

        if (userPhoto.type !== 'image/jpeg' && userPhoto.type !== 'image/png')
            return dispatch({ type: 'NOTIFY', payload: { error: 'El formato de imagen puede ser .jpeg o .png' } })

        if (userPhoto.size > (1024 * 1024) * 2) // 2mb
            return dispatch({ type: 'NOTIFY', payload: { error: 'El tamaño maximo aceptado para la imagen es de 2mb' } })

        // IMG COMPRESOR
        let options = {
            maxSizeMB: 2, // 1
            maxWidthOrHeight: 800,
            useWebWorker: true
        }

        let compressedImage;

        if (userPhoto)
            compressedImage = await imageCompression(userPhoto, options);

        if (!compressedImage)
            return dispatch({ type: 'NOTIFY', payload: { error: 'Error de compresión. Intentalo mas tarde' } })

        setData({ ...data, avatar: compressedImage })
        // END IMG COMPRESOR
        // Guardamos la foto en el estado avatar
    }

    const updateInfor = async () => {
        dispatch({ type: 'NOTIFY', payload: { loading: true } })

        let media

        if (avatar) media = await imageUpload([avatar])

        // Actualizar información imagen de avatar o nombre
        patchData('user', {
            avatar: avatar ? media[0].url : auth.user.avatar,
            name
        }, auth.token).then(res => {
            if (res.err)
                return dispatch({ type: 'NOTIFY', payload: { error: err } })

            // Volvemos a loguear al usuario
            dispatch({
                type: 'AUTH', payload: {
                    token: auth.token, // el mismo token
                    user: res.user // El usuario actualizado
                }
            })

            return dispatch({ type: 'NOTIFY', payload: { success: res.msg } })
        })
    }

    const [orderStr, setOrderStr] = useState('order')

    useEffect(() => {
        window.scrollTo(0, window.innerHeight - (window.innerHeight * 0.59));
    }, []);

    // Si no hay usuario en sesión no muestra nada
    if (!auth.user) return null;

    return (
        <div className="profile_page">
            <Head>
                <title>Perfil</title>
            </Head>

            <section className="row text-secondary mb-3 mt-5 p-3">

                {/* USER PROFILE */}
                <div className="col-md-4">
                    {/* <h3 className="text-center text-uppercase">
                        {
                            auth.user.role === 'user'
                                ? 'Perfil del usuario'
                                : 'Perfil administrador'
                        }
                    </h3> */}

                    <div className="order-summary">
                        <div className="wrap-login-item">

                            <div className="login-form form-item form-stl">

                                <fieldset className="wrap-title text-center">
                                    <h1 className="form-title text-lg py-1 text-center text-uppercase">
                                        {
                                            auth.user.role === 'user'
                                                ? 'Perfil del usuario'
                                                : 'Perfil administrador'
                                        }
                                    </h1>
                                </fieldset>

                                {/* USER PHOTO */}
                                <div className="avatar">
                                    <img src={avatar ? URL.createObjectURL(avatar) : auth.user.avatar}
                                        alt="Solumobil Avatar"
                                        title={avatar ? URL.createObjectURL(avatar) : auth.user.avatar} />

                                    <span>
                                        <i className="fas fa-camera fa-lg "></i>
                                        <p className="">Cambiar</p>
                                        <input
                                            type="file"
                                            name="profilePicture"
                                            id="profilePicture"
                                            onChange={changeAvatar}
                                            accept="image/*" />
                                    </span>
                                </div>
                                {/* END USER PHOTO */}

                                <fieldset className="wrap-input">
                                    <label htmlFor="name">Nombre</label>

                                    <input type="text" name="name" id="name" value={name}
                                        placeholder="Nombre"
                                        onChange={handleChange}
                                        className="form-control" />
                                </fieldset>

                                <fieldset className="wrap-input">
                                    <label htmlFor="email">Correo</label>

                                    <input type="email" name="email" id="email" defaultValue={auth.user.email}
                                        placeholder="Correo" className="form-control" disabled={true} />
                                </fieldset>

                                <fieldset className="wrap-input">
                                    <label htmlFor="password">Contraseña</label>

                                    <input type="password"
                                        // autoComplete="new-password"
                                        name="password" id="password" value={password}
                                        placeholder="Contraseña"
                                        onChange={handleChange}
                                        className="form-control" />
                                </fieldset>

                                <fieldset className="wrap-input">
                                    <label htmlFor="cf_password">Confirmar contraseña</label>

                                    <input type="password"
                                        // autoComplete="new-password"
                                        name="cf_password" id="cf_password" value={cf_password}
                                        placeholder="Confirmar contraseña"
                                        onChange={handleChange}
                                        className="form-control" />
                                </fieldset>

                                <div className="text-center">

                                    <button type="button"
                                        className="btn mt-3"
                                        disabled={notify.loading}
                                        onClick={handleProfileUpdate}>
                                        Actualizar
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                {/* END USER PROFILE */}


                {/* USER ORDERS */}
                <div className="col-md-8">

                    <MTable
                        title={<h3 className="text-uppercase">Ordenes</h3>}
                        data={orders}
                        columns={[
                            {
                                title: '#', field: '_id', render: rowData => (
                                    // Como se trata de un enlace externo, no es necesario usar Link
                                    <Link href={`/${orderStr.toString().toLowerCase()}/${rowData._id}`}>
                                        <a>{rowData._id.slice(0, -10)}</a>
                                    </Link>
                                )
                            },

                            {
                                title: 'FECHA',
                                field: 'createdAt',
                                type: "date",
                                render: rowData => getEsDate(rowData.createdAt),
                                // defaultGroupSort: 'desc',
                                defaultSort: 'desc',
                                export: true

                                // dateSetting: {locale: "es-ES", format: 'yyyy-mm-dd' }
                            },

                            {
                                title: 'TOTAL', field: 'total', render: rowData => (<>
                                    {/* <i className="fas fa-dollar-sign"></i> */}
                                    {currencyFormat(rowData.total)}</>)
                            },

                            {
                                title: 'ENTREGADO', field: 'delivered',
                                lookup: {
                                    false: <i className="fas fa-times fa-lg d-flex justify-content-center text-danger"></i>,
                                    true: <i className="fas fa-check fa-lg d-flex justify-content-center text-success"></i>
                                },
                                removable: true,
                                resizable: true
                            },

                            {
                                title: 'PAGADO', field: 'paid',
                                lookup: {
                                    false: <i className="fas fa-times fa-lg d-flex justify-content-center text-danger"></i>,
                                    true: <i className="fas fa-check fa-lg d-flex justify-content-center text-success"></i>
                                }
                            },

                            // BTN DETALLE CON USE STATE
                            // {
                            //     title: '', field: 'total', filtering: false,
                            //     render: rowData => (
                            //         <Link href={`/${orderStr.toString().toLowerCase()}/${rowData._id}`}>
                            //             <a
                            //                 onMouseOver={() => handleIconChange(rowData._id)}
                            //                 onMouseOut={() => handleIconChange('0')}
                            //             >
                            //                 <Fab size="small" color="primary" aria-label="Detalle de orden Solumobil">
                            //                     {
                            //                         activeId === rowData._id
                            //                             ? <ArrowDownwardIcon />
                            //                             : <ArrowForwardIcon />
                            //                     }
                            //                 </Fab>

                            //             </a>
                            //         </Link>

                            //     )
                            // }
                        ]}
                    />
                </div>
                {/* END USER ORDERS */}
            </section>
        </div>
    );
}

export default profile;
