import Head from 'next/head';
import { useState, useContext, useEffect } from 'react'
import { DataContext } from '../../store/GlobalState'
import { useRouter } from 'next/router'
import { postData } from '../../utils/fetchData';
import valid from '../../utils/valid'
import Link from 'next/link'
import Btn from '../../components/Btn'

const Forgotpassword = () => {

    const initState = { password: '', cf_password: '' },
        [userData, setUserData] = useState(initState),
        { password, cf_password } = userData,
        handleChangeInput = e => {
            const { name, value } = e.target // pick up name and value of input
            setUserData({ ...userData, [name]: value }) // fill userData with names and values of inputs
            dispatch({ type: 'NOTIFY', payload: {} })
        };
    const { state, dispatch } = useContext(DataContext)
    const { auth } = state
    const router = useRouter()
    const { id } = router.query

    console.log(id);

    const handleResetPassword = async e => {
        e.preventDefault()

        // validar passwrod y passwords match
        if (password.length < 8)
            return dispatch({ type: 'NOTIFY', payload: { error: 'La contraseña debe tener al menos 8 caracteres' } })

        if (password !== cf_password)
            return dispatch({
                type: 'NOTIFY', payload: {
                    error: 'La contraseña de confirmación no coincide'
                }
            })

        dispatch({ type: 'NOTIFY', payload: { loading: true } })

        const res = await postData('auth/forgotpassword/reset', { password, cf_password, recovery_token: id }, id) // datos de respuesta

        if (res.err) {
            if (res.err == 'jwt expired')
                return dispatch({ type: 'NOTIFY', payload: { error: 'Tiquete vencido, recupera tu contraseña' } }) // erro del back

            return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back
        }

        dispatch({ type: 'NOTIFY', payload: { success: res.msg } })

        router.push('/signin')
    }


    useEffect(() => {
        if (Object.keys(auth).length !== 0) router.push('/')
    }, [auth])

    useEffect(() => {
        window.scrollTo(0, window.innerHeight - (window.innerHeight * 0.59));
    }, []);

    return (
        <>
            <Head>
                <title>Solumobil | Recuperación de cuenta</title>
            </Head>

            <main id="main" className="main-site left-sidebar">

                <div className="container">

                    {/* START BREADCRUMB */}
                    <div className="wrap-breadcrumb">
                        <ul>
                            <li className="item-link">
                                <Link href="/">
                                    <a className="link">Inicio</a>
                                </Link>
                            </li>
                            <li className="item-link">
                                <span>Recuperar contraseña</span>
                            </li>
                        </ul>
                    </div>
                    {/* END BREADCRUMB */}


                    <div className="row justify-content-center">
                        <div className="col-lg-6 col-sm-6 col-md-6 col-xs-12 col-md-offset-3">
                            <div className=" main-content-area">
                                <div className="wrap-login-item ">
                                    <div className="login-form form-item form-stl">

                                        <form name="frm-login" onSubmit={handleResetPassword}>
                                            <fieldset className="wrap-title">
                                                <h3 className="form-title">NUEVA CONTRASEÑA</h3>
                                            </fieldset>

                                            <fieldset className="wrap-input item-width-in-half left-item ">
                                                <label htmlFor="frm-password">Contraseña</label>
                                                <input
                                                    type="password"
                                                    id="frm-password"
                                                    placeholder="Contraseña"
                                                    name="password"
                                                    minLength="8"
                                                    autoComplete="new-password"
                                                    value={password}
                                                    onChange={handleChangeInput}
                                                />
                                            </fieldset>

                                            <fieldset className="wrap-input item-width-in-half ">
                                                <label htmlFor="frm-cf_password">Confirmar Contraseña</label>
                                                <input
                                                    type="password"
                                                    id="frm-cf_password"
                                                    placeholder="Confirmar Contraseña"
                                                    name="cf_password"
                                                    minLength="8"
                                                    autoComplete="new-password"
                                                    value={cf_password}
                                                    onChange={handleChangeInput}
                                                />
                                            </fieldset>

                                            <Btn text="Guardar" classN="text-white mt-3 bg-success" />

                                            <div className="centerBtn align-items-center text-center pt-2">

                                                {/* <span className="d-inline">¿Ya recuerdas? </span> */}

                                                <Link href="/">
                                                    <a className="text-blue">Menú Principal</a>
                                                </Link>

                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            {/* <!--end main products area--> */}
                        </div>
                    </div>
                    {/* <!--end row--> */}

                </div>
                {/* <!--end container--> */}

            </main>
        </>
    );
}

export default Forgotpassword;
