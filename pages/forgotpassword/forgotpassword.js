import Head from 'next/head'
import Link from 'next/link'
import Btn from '../../components/Btn'
import { useState, useContext, useEffect } from 'react'
import { DataContext } from '../../store/GlobalState'
import { postData } from '../../utils/fetchData'
import { useRouter } from 'next/router'

const forgotpassword = () => {

    // Config pickup of data signup
    const [email, setEmail] = useState(''),
        { state, dispatch } = useContext(DataContext),
        { auth } = state,
        router = useRouter(),

        handleChangeEmail = e => {
            // console.log(e.target.value);
            setEmail(e.target.value)
            dispatch({ type: 'NOTIFY', payload: {} })
        },

        handleRecovery = async e => {
            e.preventDefault()

            dispatch({ type: 'NOTIFY', payload: { loading: true } })

            console.log(email)

            const res = await postData('auth/forgotpassword', { email }) // datos de respuesta

            if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

            dispatch({ type: 'NOTIFY', payload: { success: res.msg } })
        }

    // console.log(auth);
    useEffect(() => {
        if (Object.keys(auth).length !== 0) router.push('/')
    }, [auth])

    useEffect(() => {
        window.scrollTo(0, window.innerHeight - (window.innerHeight * 0.67));
    }, []);

    return (
        <div>
            <Head>
                <title>Recuperar contraseña</title>
            </Head>

            {/* <!--main area--> */}
            <main id="main" className="main-site left-sidebar">

                <div className="container">

                    {/* START BREADCRUMB */}
                    <div className="wrap-breadcrumb">
                        <ul>
                            <li className="item-link">
                                <Link href="/">
                                    <a className="link">Inicio</a>
                                </Link>
                            </li>
                            <li className="item-link">
                                <span>Recuperar contraseña</span>
                            </li>
                        </ul>
                    </div>
                    {/* END BREADCRUMB */}


                    <div className="row justify-content-center">
                        <div className="col-lg-6 col-sm-6 col-md-6 col-xs-12 col-md-offset-3">
                            <div className=" main-content-area">
                                <div className="wrap-login-item ">
                                    <div className="login-form form-item form-stl">

                                        <form name="frm-login" onSubmit={handleRecovery}>
                                            <fieldset className="wrap-title">
                                                <h3 className="form-title">RECUPERE SU CUENTA</h3>
                                            </fieldset>
                                            <fieldset className="wrap-input">
                                                <label htmlFor="frm-email">Correo:</label>
                                                <input
                                                    type="text"
                                                    autoComplete="email"
                                                    placeholder="Correo electrónico"
                                                    id="frm-email"
                                                    name="email"
                                                    value={email}
                                                    onChange={handleChangeEmail}
                                                />
                                            </fieldset>

                                            <Btn text="Recuperar" classN="text-white mt-3 bg-success" />

                                            <div className="centerBtn align-items-center text-center pt-2">

                                                <span className="d-inline">¿Ya recuerdas? </span>

                                                <Link href="/signin">
                                                    <a className="text-blue">Ingresar</a>
                                                </Link>

                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            {/* <!--end main products area--> */}
                        </div>
                    </div>
                    {/* <!--end row--> */}

                </div>
                {/* <!--end container--> */}

            </main>
        </div>
    );
}

export default forgotpassword;
