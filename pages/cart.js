import Head from 'next/head';
import { useContext, useState, useEffect } from 'react'
import { DataContext } from '../store/GlobalState'
import TooltipMsg from '../components/TooltipMsg'
import CartItem from '../components/CartItem'
import Link from 'next/link'
import { getData, postData } from '../utils/fetchData'
// import PaypalBtn from '../components/paypalBtn'
import { useRouter } from 'next/router'
import { currencyFormat } from '../utils/funcs'

const cart = () => {

    const { state, dispatch } = useContext(DataContext),
        { cart, auth, orders, selectedCurrency, currencies } = state,

        [address, setAddress] = useState(''),
        [mobile, setMobile] = useState(''),

        // PAYPAL
        // [payment, setPayment] = useState(false),
        [callback, setCallback] = useState(false),
        // END PAYPAL

        // Total del carrito
        [total, setTotal] = useState(0),

        emptyCart = () => {
            const img = <img className="img-responsive m-auto w-50 p-2 d-flex"
                src="/media/img/home/empty_cart.svg"
                alt="Carrito vacio" />;

            return (
                <TooltipMsg text="Carrito vacio"
                    placement="bottom"
                    element={img}
                    style={{ marginTop: '5rem !important' }} />
            );
        };

    const router = useRouter()

    // Obtener el reducido total
    useEffect(() => {

        const getTotal = () => {
            const res = cart.reduce((prev, item) => {
                return prev + (item.price * item.quantity)
            }, 0)

            setTotal(res)
        }

        getTotal()

    }, [cart])

    // Validar el carrito del localStorage con el inventario de la BD
    // Constantemente cambia el inventario por eso validamos y actualizamos el carrito constantemente
    useEffect(() => {
        const cartLocal = JSON.parse(localStorage.getItem('__next__cart01__vargas'))

        if (cartLocal && cartLocal.length > 0) {
            let newArr = []

            const updateCart = async () => {
                for (const item of cartLocal) {
                    const res = await getData(`product/${item._id}`) // Obtenemos el detalle del producto
                    console.log(res.product);

                    if (res.product) {
                        const { _id, title, images, price, inStock, sold } = res.product

                        if (inStock > 0) {
                            newArr.push({
                                _id, title, images, price, inStock, sold,
                                quantity: item.quantity > inStock ? 1 : item.quantity
                            })
                        }
                    }
                }

                dispatch({ type: 'ADD_CART', payload: newArr })
            }

            updateCart()
        }
    }, [callback])

    // PAYPAL PART TWO
    const handlePayment = async () => {

        if (!auth.user) return dispatch({
            type: 'NOTIFY',
            payload: { success: 'Por favor inicia sesión para continuar' }
        })

        if (!address || !mobile)
            return dispatch({
                type: 'NOTIFY',
                payload: { error: 'Por favor ingresa tu dirección y télefono' }
            })

        // setPayment(true)

        // Validar de acuerdo a la cantidad solicitada en el carrito payment checkout que si haya STOCK
        let newCart = []

        for (const item of cart) {
            const res = await getData(`product/${item._id}`)

            if (res.product.inStock - item.quantity >= 0) {
                newCart.push(item) // Llenamos solo si hay en stock
            }
        }

        if (newCart.length < cart.length) {

            setCallback(!callback)

            return dispatch({
                type: 'NOTIFY',
                payload: { error: 'El producto esta fuera de Stock o la cantidad es insuficiente' }
            })
        }

        dispatch({ type: 'NOTIFY', payload: { loading: true } })

        // Llenamos la orden
        postData('order', { address, mobile, cart, total }, auth.token)
            .then(res => {
                if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

                // Vaciar el carrito
                dispatch({ type: 'ADD_CART', payload: [] })

                const newOrder = {
                    ...res.newOrder,
                    user: auth.user
                }

                // Añadir orden en perfil
                dispatch({ type: 'ADD_ORDERS', payload: [...orders, newOrder] })
                dispatch({ type: 'NOTIFY', payload: { success: res.msg } })

                return router.push(`/order/${res, newOrder._id}`)
            })
    }

    useEffect(() => {
        window.scrollTo(0, window.innerHeight - (window.innerHeight * 0.59));
    }, []);

    return (
        <>
            <Head>
                <title>Carrito</title>
            </Head>

            {
                cart.length === 0
                    ? emptyCart()
                    : (
                        <main id="main" className="main-site">

                            <div className="container">

                                <div className="wrap-breadcrumb">
                                    <ul>
                                        <li className="item-link">
                                            <Link href="/">
                                                <a className="link">Inicio</a>
                                            </Link>
                                        </li>
                                        <li className="item-link">
                                            <span>Carrito</span>
                                        </li>
                                    </ul>
                                </div>
                                <div className="main-content-area">
                                    <div className="wrap-iten-in-cart">
                                        <h3 className="box-title">Productos</h3>

                                        {/* Carrito */}
                                        <ul className="products-cart">
                                            {
                                                cart.map(item => (
                                                    <CartItem key={item._id} item={item}
                                                        dispatch={dispatch} cart={cart} state={state} />
                                                ))
                                            }
                                        </ul>
                                    </div>

                                    <div className="summary">

                                        <div className="row">
                                            <div className="col-md-8">
                                                <div className="order-summary">
                                                    <h4 className="title-box">Resumen del pedido</h4>
                                                    <p className="summary-info">
                                                        <span className="title">Subtotal</span>
                                                        <b className="index">
                                                            {
                                                                selectedCurrency.selected == 'cop'
                                                                    ? currencyFormat(total)
                                                                    : selectedCurrency.selected == 'usd'
                                                                        ? currencyFormat((total / currencies.usd).toFixed(2), 'en-US', "USD")
                                                                        : ''
                                                            }
                                                        </b>
                                                    </p>
                                                    <p className="summary-info">
                                                        <span className="title">Envío</span>
                                                        <b className="index">Envío gratis</b></p>
                                                    <p className="summary-info total-info ">
                                                        <span className="title">Total</span>
                                                        <b className="index">
                                                            {
                                                                selectedCurrency.selected == 'cop'
                                                                    ? currencyFormat(total)
                                                                    : selectedCurrency.selected == 'usd'
                                                                        ? currencyFormat((total / currencies.usd).toFixed(2), 'en-US', "USD")
                                                                        : ''
                                                            }
                                                        </b>

                                                    </p>
                                                </div>
                                                <div className="checkout-info" >
                                                    {/* <label className="checkbox-field">
                                                <input className="frm-input " name="have-code"
                                                    id="have-code" value="" type="checkbox" />
                                                <span>I have promo code</span>
                                            </label> */}

                                                    {/* Si esta autenticado bien sino lo manda a que se autentique */}
                                                    {/* {
                                                        payment
                                                            ? <PaypalBtn
                                                                total={total}
                                                                address={address}
                                                                mobile={mobile}
                                                                state={state}
                                                                dispatch={dispatch} />
                                                            : */}
                                                    <Link href={auth.user ? '#' : '/signin'}>
                                                        <a onClick={handlePayment} className="btn btn-checkout">
                                                            Proceder con el pago
                                                        </a>
                                                    </Link>
                                                    {/* } */}

                                                    <Link href="/">
                                                        <a className="link-to-shop">Continuar comprando <i className="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                                    </Link>
                                                </div>
                                                {/* <div className="update-clear">
                                                    <a className="btn btn-clear" href="#">Limpiar carrito de compra</a>
                                                    <a className="btn btn-update" href="#">Actualizar carrito de compra</a>
                                                </div> */}
                                            </div>

                                            <div className="col-md-4">
                                                <div className="order-summary">
                                                    {/* <h4 className="title-box">Envío</h4> */}
                                                    <div className="wrap-login-item">

                                                        <div className="login-form form-item form-stl">

                                                            <fieldset className="wrap-title">
                                                                <h3 className="form-title">Envío</h3>
                                                            </fieldset>

                                                            <fieldset className="wrap-input">
                                                                <label htmlFor="frm-address">Dirección:</label>
                                                                <input
                                                                    type="text"
                                                                    autoComplete="address"
                                                                    placeholder="Dirección"
                                                                    id="frm-address"
                                                                    name="address"
                                                                    value={address}
                                                                    onChange={e => setAddress(e.target.value)}
                                                                />
                                                            </fieldset>

                                                            <fieldset className="wrap-input">
                                                                <label htmlFor="frm-mobile">Télefono:</label>
                                                                <input
                                                                    type="text"
                                                                    autoComplete="mobile"
                                                                    placeholder="Télefono"
                                                                    id="frm-mobile"
                                                                    name="mobile"
                                                                    value={mobile}
                                                                    onChange={e => setMobile(e.target.value)}
                                                                />
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </main>
                    )
            }

        </>
    );
}

export default cart;