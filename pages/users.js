import Head from 'next/head'
import Link from 'next/link'
import { useContext, useState, useEffect } from 'react'
import { DataContext } from '../store/GlobalState'
import MTable from '../components/MTable'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import ConfirmMsg from '../components/ConfirmMsg'

const Users = () => {

    const { state, dispatch } = useContext(DataContext)

    // Obtenemos los usuarios
    const { users, auth } = state

    const [userStr] = useState('user')

    // Confirmación de acción
    const [isOpen, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleDialogClose = () => {
        setOpen(false);
    };

    useEffect(() => {
        window.scrollTo(0, window.innerHeight - (window.innerHeight * 0.59));
    }, []);

    if (!auth.user) return null

    return (
        <>
            <Head>Usuarios</Head>

            {/* MTable */}
            <div className="container my-5 pt-3 position-relative">
                <MTable
                    title={<h3 className="text-uppercase">Usuarios</h3>}
                    data={users}
                    columns={[
                        {
                            title: '#', field: '_id', render: rowData => rowData._id
                        },

                        // por alinear imagen
                        {
                            title: 'AVATAR',
                            field: 'avatar',
                            render: rowData => (
                                <img
                                    src={rowData.avatar}
                                    alt={rowData.name}
                                    width="30"
                                    height="30"
                                    className="overflow-hidden obj-fit-cover rounded-circle"
                                />
                            )
                        },

                        { title: 'NOMBRE', field: 'name' },

                        {
                            title: 'CORREO', field: 'email', render: rowData => (
                                <Link href={`mailto:${rowData.email}?Subject=Solumobil20Celulares`}>
                                    <a>{rowData.email}</a>
                                </Link>
                            )
                        },

                        {
                            title: 'ADMIN', field: 'role', render: rowData => rowData.role === 'admin'
                                ? rowData.root
                                    ? <i className="fas fa-check text-success"> Raíz</i>
                                    : <i className="fas fa-check text-success"></i>
                                : <i className="fas fa-times text-danger"></i>

                        },

                        // Acciones
                        {
                            title: '', field: 'role', render: rowData => <>
                                <Link href={auth.user.root && auth.user.email !== rowData.email
                                    ? `/edit_user/${rowData._id}` : '#!'}>
                                    <a title="Editar Usuario">
                                        <EditIcon
                                            color="primary"
                                        // className="text-info"
                                        />
                                    </a>
                                </Link>

                                {
                                    auth.user.root && auth.user.email !== rowData.email
                                        ? <DeleteIcon color="secondary" title="Remover Usuario"
                                            onClick={() => {
                                                handleClickOpen()
                                                dispatch({
                                                    type: 'ADD_CONFIRM',
                                                    payload: [{
                                                        data: users,
                                                        id: rowData._id,
                                                        title: rowData.name,
                                                        type: 'ADD_USERS'
                                                    }]
                                                })
                                            }} className="cursor-pointer" />
                                        : ''
                                }

                            </>
                        }
                    ]}

                    actionsColIndex="-1"
                    grouping={false}
                />

                <ConfirmMsg
                    isOpen={isOpen}
                    handleClose={handleDialogClose}
                    subtitle='¿Quieres remover este usuario?'
                />
            </div>
        </>
    );
}

export default Users;