import Head from 'next/head'
import { useState, useContext, useEffect } from 'react'
import { DataContext } from '../../store/GlobalState'
import { useRouter } from 'next/router'
import { patchData } from '../../utils/fetchData'
import { updateItem } from '../../store/Actions'

const EditUser = () => {

    const router = useRouter()
    const { id } = router.query

    const { state, dispatch } = useContext(DataContext)

    const { auth, users } = state

    const [editUser, setEditUser] = useState([])
    const [checkAdmin, setCheckAdmin] = useState(false)
    const [num, setNum] = useState(0)

    useEffect(() => {

        users.forEach(user => {
            if (user._id === id) {
                setEditUser(user)
                setCheckAdmin(user.role === 'admin' ? true : false)
            }
        });

    }, [users])

    const handleCheck = () => {
        setCheckAdmin(!checkAdmin)
        setNum(num + 1)
    }

    const handleSubmit = () => {

        let role = checkAdmin ? 'admin' : 'user'

        // if (num % 2 !== 0) { // SOLO UNA VEZ
            dispatch({ type: 'NOTIFY', payload: { loading: true } })

            patchData(`user/${editUser._id}`, { role }, auth.token).then(res => {
                if (res.err)
                    return dispatch({ type: 'NOTIFY', payload: { error: res.err } })

                // Actualizar listado de usuarios
                dispatch(updateItem(users, editUser._id, { ...editUser, role }, 'ADD_USERS'))

                return dispatch({ type: 'NOTIFY', payload: { success: res.msg } })
            })
        // }
    }

    useEffect(() => {
        window.scrollTo(0, window.innerHeight - (window.innerHeight * 0.59));
    }, []);

    return (
        <>
            <Head>
                <title>Editar Usuario</title>
            </Head>

            <div className="d-block">
                <button className="btn btn-dark" onClick={() => router.back()}>
                    <i className="fas fa-long-arrow-alt-left" aria-hidden> Volver</i>
                </button>
            </div>

            <div className="profile_page edit_user my-3 d-flex justify-content-center">

                <div className="col-md-5 max-auto my-4">

                    <div className="order-summary">
                        <div className="wrap-login-item">

                            <div className="login-form form-item form-stl">

                                <fieldset className="wrap-title text-center">
                                    <h1 className="form-title text-lg py-1 text-center text-uppercase">
                                        Editar Usuario
                                    </h1>
                                </fieldset>

                                <fieldset className="wrap-input">
                                    <label htmlFor="name">Nombre</label>

                                    <input type="text" id="name" defaultValue={editUser.name}
                                        placeholder="Nombre" className="form-control" disabled={true} />
                                </fieldset>

                                <fieldset className="wrap-input">
                                    <label htmlFor="email">Correo</label>

                                    <input type="email" id="email" defaultValue={editUser.email}
                                        placeholder="Correo" className="form-control" disabled={true} />
                                </fieldset>

                                <fieldset className="wrap-input">

                                    <label className="remember-field">
                                        <input
                                            type="checkbox" id="isAdmin"
                                            placeholder="Correo"
                                            className="form-control frm-input"
                                            checked={checkAdmin}
                                            onChange={handleCheck}
                                        />
                                        <span>Es Administrador</span>
                                    </label>

                                </fieldset>

                                <div className="text-center">

                                    <button type="button"
                                        className="btn mt-3"
                                        onClick={handleSubmit}
                                    >
                                        Actualizar
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default EditUser;