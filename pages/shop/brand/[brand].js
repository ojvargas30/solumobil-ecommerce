import { getData } from '../../../utils/fetchData';
import { useState, useContext, useEffect } from 'react'
import { DataContext } from '../../../store/GlobalState'
import Head from 'next/head'
import filterSearch from '../../../utils/filterSearch'
import { useRouter } from 'next/router'
import Shop from '../../../components/shop/Shop'

const shop = props => {

    const [products, setProducts] = useState(props.products)

    // CONSTANTES FILTROS
    const [brand, setBrand] = useState('')
    const [category, setCategory] = useState('')
    const [search, setSearch] = useState('')
    const [sort, setSort] = useState('')
    // END CONSTANTES FILTROS

    const { state, dispatch } = useContext(DataContext)

    useEffect(() => {
        setProducts(props.products)
    }, [props.products])

    const router = useRouter()

    useEffect(() => {

        if (router.query.brand) setBrand(router.query.brand)
        filterSearch({ router, brand: router.query.brand })

    }, [])

    const handleCategory = e => {
        console.log(e.target.dataset.category)
        setCategory(e.target.dataset.category)

        // FILTRO
        filterSearch({ router, category: e.target.dataset.category })
    }

    const handleBrand = e => {
        // console.log(e.target.dataset.brand)
        setBrand(e.target.dataset.brand)

        // FILTRO
        filterSearch({ router, brand: e.target.dataset.brand })
    }

    const handleSort = e => {
        setSort(e.target.value)
        filterSearch({ router, sort: e.target.value })
    }

    useEffect(() => {
        console.log(search);
        // if (search) getData(`product`)
        filterSearch({ router, search: search ? search : 'all' })

    }, [search])

    return (
        <>
            <Head>
                <title>Solumobil | Tienda</title>
            </Head>

            <Shop
                handleCategory={handleCategory}
                handleBrand={handleBrand}
                handleSort={handleSort}
                products={products}
                state={state}
                dispatch={dispatch}
                brand={brand}
            />
        </>
    );
}

export async function getServerSideProps({ query }) { // Server side rendering -> no console.log()

    const category = query.category || 'all'
    const brand = query.brand || 'all'
    const sort = query.sort || ''
    const search = query.search || 'all'

    let url = `product?category=${category}&brand=${brand}&sort=${sort}&title=${search}`;

    if (sort == 'orderall')
        url = `product?category=${category}&brand=${brand}&title=${search}`


    const res = await getData(url)

    // server side rendering
    return {
        props: {
            products: res.products,
            result: res.result
        }, // will be passed to the page component as props
    }
}

export default shop;
