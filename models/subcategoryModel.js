import mongoose from 'mongoose'

const subcategorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    isActive: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
})

let Dataset = mongoose.models.subcategory || mongoose.model('subcategory', subcategorySchema)
export default Dataset