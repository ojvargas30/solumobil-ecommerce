import mongoose from 'mongoose'

const usernewsletterSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        trim: true
    },
    isActive: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
})

let Dataset = mongoose.models.usernewsletter || mongoose.model('usernewsletter', usernewsletterSchema)
export default Dataset