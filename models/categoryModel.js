import mongoose from 'mongoose'

const categorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    subcategory: {
        type: mongoose.Types.ObjectId,
        ref: 'subcategory'
    },
    
    isActive: {
        type: Boolean,
        default: true
    },
    icon: {
        type: String
        // default: 'https://res.cloudinary.com/linda-leblanc/image/upload/v1612634594/test/male_ava_klfutu.svg'
    }
}, {
    timestamps: true
})

let Dataset = mongoose.models.category || mongoose.model('category', categorySchema)
export default Dataset