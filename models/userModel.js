import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Por favor ingresa tu nombre"],
        trim: true,
    },
    email: {
        type: String,
        required: [true, "Por favor ingresa tu correo"],
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: [true, "Por favor ingresa tu contraseña"],
    },
    role: {
        type: String,
        default: 'user'
    },
    root: {
        type: Boolean,
        default: false
    },
    isActive: {
        type: Boolean,
        default: true
    },
    avatar: {
        type: String,
        default: 'https://res.cloudinary.com/linda-leblanc/image/upload/v1612634594/test/male_ava_klfutu.svg'
    }
}, {
    timestamps: true
});

let Dataset = mongoose.models.user || mongoose.model('user', userSchema)
export default Dataset
