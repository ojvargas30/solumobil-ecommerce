import mongoose from 'mongoose';

const productSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        trim: true,
        uppercase: true
    },
    price: {
        type: Number,
        required: true
    },
    desc: {
        type: String,
        required: true,
        trim: true
    },
    content: {
        type: String,
        required: false
    },
    images: {
        type: Array,
        required: true
    },
    colors: {
        type: Array,
        required: false
    },
    features: {
        type: Array,
        required: false
    },
    // colors: {
    //     type: Array,
    //     required: false
    // },
    category: {
        type: String,
        required: true
    },
    brand: {
        type: String,
        required: false
    },
    weight: {
        type: String,
        required: false
    },
    dimensions: {
        type: String,
        required: false
    },
    checked: {
        type: Boolean,
        default: false
    },
    inStock: {
        type: Number,
        default: 0
    },
    isActive: {
        type: Boolean,
        default: true
    },
    sold: {
        type: Number,
        default: 0
    },
}, {
    timestamps: true
});

let Dataset = mongoose.models.product || mongoose.model('product', productSchema)
export default Dataset