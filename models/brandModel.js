import mongoose from 'mongoose'

const brandSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    category: {
        type: mongoose.Types.ObjectId,
        ref: 'category'
    },
    image: {
        type: String,
        // default: 'https://res.cloudinary.com/linda-leblanc/image/upload/v1612634594/test/male_ava_klfutu.svg'
    },
    isActive: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
})

let Dataset = mongoose.models.brand || mongoose.model('brand', brandSchema)
export default Dataset