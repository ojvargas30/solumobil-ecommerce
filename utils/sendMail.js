import User from '../models/userModel'
import sgMail from '@sendgrid/mail'

sgMail.setApiKey(process.env.SENDGRID_APIKEY)

const sendMail = async mailOptions => {

    if (!mailOptions.hasOwnProperty('from'))
        mailOptions.from = `Solumobil <${process.env.MAIL_USERNAME}>`;

    // SEND EMAIL TO EVERYBODY
    if (process.env.BASE_URL == "http://localhost:3000") {
        mailOptions.to = ['adtestjavis@gmail.com', 'adventurejavis@gmail.com']
    } else {
        if (mailOptions.hasOwnProperty('to')) {

            if (mailOptions.to == 'all' || mailOptions.to == 'todos') {
                const users = await User.find({}, { email: 1, _id: 0 })

                console.log(users)

                let everybody = [];
                await users.forEach(user => everybody.push(user.email))

                mailOptions.to = everybody

            } else if (mailOptions.to == 'admin') {
                const users = await User.find({ role: 'admin' }, { email: 1, _id: 0 })

                let everybody = [];
                await users.forEach(user => everybody.push(user.email))

                mailOptions.to = everybody
            }
        }
    }

    await sgMail.sendMultiple(mailOptions).then(() => {
    }, error => {
        console.error(error, 'inside then');
        if (error.response)
            console.error(error.response.body, 'inside then if condition')

    })
        .catch((err) => {
            console.log(err, 'inside catch');
        });
}

export default sendMail;
