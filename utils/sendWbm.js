// requiere auth con qr una vez
const wbm = require('wbm')

wbm.start().then(async () => {
    const phones = ['573133043714', '573208557457', '573118330760', '573203149746'];
    const message = 'Mensaje Whatsapp enviado desde www.solumobil.com';
    await wbm.send(phones, message);
    await wbm.end();
}).catch(err => console.log(err));