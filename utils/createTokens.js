import jwt from 'jsonwebtoken'

export const createActivateAccountToken = (payload) => {
    return jwt.sign(payload, process.env.ACTIVATE_ACCOUNT_TOKEN_SECRET, { expiresIn: '5m' }) // 5m recommended
}

export const createAccessToken = (payload) => {
    return jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '90m' })
}

export const createRefreshToken = (payload) => {
    return jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, { expiresIn: '7d' })
}
