const Vonage = require('@vonage/server-sdk')

const vonage = new Vonage({
    apiKey: process.env.VONAGE_KEY,
    apiSecret: process.env.VONAGE_SECRET
})

// console.log(process.env.VONAGE_KEY)
// console.log(process.env.VONAGE_SECRET)

// const from = "Vonage APIs"
// const to = "573133043714"
// const text = 'A text message sent using the Vonage SMS API'

// Se demora unos minutos pero lo envía
const sendSms = smsOptions => {

    const { to, text } = smsOptions

    // FROM, TO, TEXT
    vonage.message.sendSms("Solumobil", to, text
    // , {
    //     type: "unicode"
    // }
    , (err, responseData) => {

        if (err) console.log(err)

        else {
            if (responseData.messages[0]['status'] === "0")
                console.log("Message sent successfully.")
            else
                console.log(`Message failed with error: ${responseData.messages[0]['error-text']}`)

        }
    })
}

export default sendSms;
