const filterSearch = ({ router, page, category, sort, search, brand }) => {

    const path = router.pathname

    if (
        path != "/signin" &&
        path != "/activeaccount/[id]" &&
        path != "/activeaccount" &&
        path != "/forgotpassword/[id]" &&
        path != "/order/[id]?search=all" &&
        path != "/order/[id]" &&
        path != "/create/[id]" &&
        path != "/create" &&
        path != "/profile" &&
        path != "/cart" &&
        path != "/signup"
    ) {
        const query = router.query
        console.log('PATH')
        console.log(path)
        console.log('END PATH')

        console.log('QUERY')
        console.log(query)
        console.log('END QUERY')


        if (category) query.category = category;
        if (page) query.page = page;
        if (search) query.search = search;
        if (sort) query.sort = sort;
        if (brand) query.brand = brand;

        router.push({
            pathname: path,
            query
        }, undefined, { scroll: false })

    }

    console.log('ROUTER')
    console.log(router)
    console.log('END ROUTER')
}

export default filterSearch;
