function blobToFile(theBlob, fileName) {
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    theBlob.lastModifiedDate = new Date();
    theBlob.name = fileName;
    return theBlob;
}

function makeID(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

async function convertCurrency(amount, from_currency, to_currency) {
    let restUrl = "https://api.exchangerate-api.com/v4/latest/" + from_currency + "",
        options = {
            method: 'GET',
            headers: {
                "accept": "application/json;odata=verbose"
            }
        };

    const response = await fetch(restUrl, options)
        .then(res => res.json())
        .then(data => {
            // console.log(data)

            if (data) {
                let rate = data.rates[to_currency];
                let valueConverted = (amount * rate).toFixed(2);

                return valueConverted
            }
        })
        .catch(err => console.error(err))

    return response

}

function currencyFormat(value, idiome = "es-CO", currency = "COP") {
    // idiome = "en-US", currency = "USD"
    // if (currency == 'USD')
    //     value = await convertCurrency(value, 'COP', 'USD')

    // value = await convertCurrency(value, 'COP', 'USD')
    return value
        ? new Intl.NumberFormat(idiome, { style: "currency", currency }).format(value)
        : "";
}

function getEsDate(date, ret = "fulldate") {
    date = new Date(date)

    // let day = date.getDate().length === 1 ? '0' + date.getDate() : date.getDate();
    let day = date.getDate()
    let month = date.getMonth()
        , year = date.getFullYear()

        , options = { year: 'numeric', month: 'long', day: 'numeric' }

        , splitDate = new Date(year, month, day)

        , esDate = splitDate.toLocaleDateString("es-ES", options);

    // console.log(esDate)

    esDate = esDate.replaceAll(' de ', ' - ');

    let esTime = date.toLocaleTimeString('es-CO');

    if (ret == "onlydate") date = esDate;
    else date = esDate + ' ' + esTime;

    date = date.toString()

    if (ret == "onlydate") {
        if (date.indexOf("p.") > -1) date = date.slice(0, -9) + ' PM'
        if (date.indexOf('a.') > -1) date = date.slice(0, -9) + ' AM'
    }

    // console.log(date)

    return date
}

function getFullEsDate(date) {
    date = new Date(date)

    let day = date.getDate()
        , month = date.getMonth()
        , year = date.getFullYear()

        , options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }

        , splitDate = new Date(year, month, day)

        , esDate = splitDate.toLocaleDateString("es-ES", options)
        // let esDate = date.toLocaleDateString('es-CO').split('/').join('-');
        // date.toLocaleDateString('es-CO'),

        , esTime = date.toLocaleTimeString('es-CO');

    date = esDate + ' ' + esTime;

    date = date.toString()

    if (date.indexOf("p.") > -1) date = date.slice(0, -9) + ' PM'
    if (date.indexOf('a.') > -1) date = date.slice(0, -9) + ' AM'

    // console.log(date)

    return date
}

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function genBasicProductHTML(tmplOptions) {

    let date = new Date()
    let esDate = date.getFullYear()

    let { product, relatedProducts } = tmplOptions

    let basicMAKProductHTML = `<!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <meta charset="utf-8"> <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
        <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
        <meta name="format-detection" content="telephone=no,address=no,email=no,date=no,url=no"> <!-- Tell iOS not to automatically link certain text strings. -->
        <meta name="color-scheme" content="light">
        <meta name="supported-color-schemes" content="light">
        <title>Solumobil</title> <!--   The title tag shows in email notifications, like Android 4.4. -->

        <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
        <!--[if gte mso 9]>
        <xml>
            <o:OfficeDocumentSettings>
                <o:AllowPNG/>
                <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->

        <!-- Web Font / @font-face : BEGIN -->
        <!-- NOTE: If web fonts are not required, lines 23 - 41 can be safely removed. -->

        <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
        <!--[if mso]>
            <style>
                * {
                    font-family: sans-serif !important;
                }
            </style>
        <![endif]-->

        <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
        <!--[if !mso]><!-->
        <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
        <!--<![endif]-->

        <!-- Web Font / @font-face : END -->

        <!-- CSS Reset : BEGIN -->
        <style>

            /* What it does: Tells the email client that only light styles are provided but the client can transform them to dark. A duplicate of meta color-scheme meta tag above. */
            :root {
              color-scheme: light;
              supported-color-schemes: light;
            }

            /* What it does: Remove spaces around the email design added by some email clients. */
            /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
            html,
            body {
                margin: 0 auto !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
            }

            /* What it does: Stops email clients resizing small text. */
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }

            /* What it does: Centers email on Android 4.4 */
            div[style*="margin: 16px 0"] {
                margin: 0 !important;
            }

            /* What it does: forces Samsung Android mail clients to use the entire viewport */
            #MessageViewBody, #MessageWebViewDiv{
                width: 100% !important;
            }

            /* What it does: Stops Outlook from adding extra spacing to tables. */
            table,
            td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }

            /* What it does: Fixes webkit padding issue. */
            table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                margin: 0 auto !important;
            }

            /* What it does: Uses a better rendering method when resizing images in IE. */
            img {
                -ms-interpolation-mode:bicubic;
            }

            /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
            a {
                text-decoration: none;
            }

            /* What it does: A work-around for email clients meddling in triggered links. */
            a[x-apple-data-detectors],  /* iOS */
            .unstyle-auto-detected-links a,
            .aBn {
                border-bottom: 0 !important;
                cursor: default !important;
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }

            /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
            .a6S {
                display: none !important;
                opacity: 0.01 !important;
            }

            /* What it does: Prevents Gmail from changing the text color in conversation threads. */
            .im {
                color: inherit !important;
            }

            /* If the above doesn't work, add a .g-img class to any image in question. */
            img.g-img + div {
                display: none !important;
            }

            /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
            /* Create one of these media queries for each additional viewport size you'd like to fix */

            /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
            @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
                u ~ div .email-container {
                    min-width: 320px !important;
                }
            }
            /* iPhone 6, 6S, 7, 8, and X */
            @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
                u ~ div .email-container {
                    min-width: 375px !important;
                }
            }
            /* iPhone 6+, 7+, and 8+ */
            @media only screen and (min-device-width: 414px) {
                u ~ div .email-container {
                    min-width: 414px !important;
                }
            }

        </style>
        <!-- CSS Reset : END -->

        <!-- Progressive Enhancements : BEGIN -->
        <style>

            /* What it does: Hover styles for buttons */
            .button-td,
            .button-a {
                transition: all 100ms ease-in;
            }
            .button-td-primary:hover,
            .button-a-primary:hover {
                background: #555555 !important;
                border-color: #555555 !important;
            }

            /* Media Queries */
            @media screen and (max-width: 600px) {

                /* What it does: Adjust typography on small screens to improve readability */
                .email-container p {
                    font-size: 17px !important;
                }

            }

        </style>
        <!-- Progressive Enhancements : END -->

    </head>
    <!--
        The email background color (#222222) is defined in three places:
        1. body tag: for most email clients
        2. center tag: for Gmail and Inbox mobile apps and web versions of Gmail, GSuite, Inbox, Yahoo, AOL, Libero, Comcast, freenet, Mail.ru, Orange.fr
        3. mso conditional: For Windows 10 Mail
    -->
    <body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #FFFFFF;">
        <center role="article" aria-roledescription="email" lang="en" style="width: 100%; background-color: #FFFFFF;">
        <!--[if mso | IE]>
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #222222;">
        <tr>
        <td>
        <![endif]-->

            <!-- Visually Hidden Preheader Text : BEGIN -->
            <div style="max-height:0; overflow:hidden; mso-hide:all;" aria-hidden="true">
                (Optional) This text will appear in the inbox preview, but not the email body. It can be used to supplement the email subject line or even summarize the email's contents. Extended text preheaders (~490 characters) seems like a better UX for anyone using a screenreader or voice-command apps like Siri to dictate the contents of an email. If this text is not included, email clients will automatically populate it using the text (including image alt text) at the start of the email's body.
            </div>
            <!-- Visually Hidden Preheader Text : END -->

            <!-- Create white space after the desired preview text so email clients don’t pull other distracting text into the inbox preview. Extend as necessary. -->
            <!-- Preview Text Spacing Hack : BEGIN -->
            <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
                &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
            </div>
            <!-- Preview Text Spacing Hack : END -->

            <!--
                Set the email width. Defined in two places:
                1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
                2. MSO tags for Desktop Windows Outlook enforce a 600px width.
            -->
            <div style="max-width: 600px; margin: 0 auto;" class="email-container">
                <!--[if mso]>
                <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="600">
                <tr>
                <td>
                <![endif]-->

                <!-- Email Body : BEGIN -->
                <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
                    <!-- Email Header : BEGIN -->
                    <tr>
                        <td style="padding: 20px 0; text-align: center">
                            <img src="https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg"
                            width="200"
                            height="50"
                            alt="Solumobil_Logo"
                            border="0"
                            style="height: auto; background: #dddddd;
                                    font-family: sans-serif; font-size: 15px;
                                     line-height: 15px; color: #555555;">
                        </td>
                    </tr>
                    <!-- Email Header : END -->

                    <!-- Hero Image, Flush : BEGIN -->
                    <tr>
                        <td style="background-color: #ffffff;">
                            <img
                                src="${product.images[0] ? product.images[0].url : 'https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg'}"
                                width="600"
                                height=""
                                alt="${product.title}"
                                border="0"
                                style="width: 100%; max-width: 600px; height: auto; background: #dddddd;
                                        font-family: sans-serif; font-size: 15px;
                                        line-height: 15px; color: #555555;
                                        margin: auto; display: block;"
                                        class="g-img"
                                     >
                        </td>
                    </tr>
                    <!-- Hero Image, Flush : END -->

                    <!-- 1 Column Text + Button : BEGIN -->
                    <tr>
                        <td style="background-color: #ffffff;">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0"
                            width="100%">
                                <tr>
                                    <td style="padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                                        <h1 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 25px; line-height: 30px; color: #333333; font-weight: normal;">${product.title}</h1>
                                        <p style="margin: 0;">
                                            ${product.desc}
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 20px;">
                                        <!-- Button : BEGIN -->
                                        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" style="margin: auto;">
                                            <tr>
                                                <td class="button-td button-td-primary" style="border-radius: 4px; background: #222222;">
                                                     <a class="button-a button-a-primary"
                                                     href="${process.env.CLIENT_URL}/product/${product._id}"
                                                     style="background: #222222;
                                                     border: 1px solid #000000;
                                                     font-family: sans-serif;
                                                     font-size: 15px; line-height: 15px; text-decoration: none; padding: 13px 17px;
                                                     color: #ffffff; display: block; border-radius: 4px; padding-bottom: 1rem;">
                                                        Adquirir Producto
                                                     </a>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- Button : END -->
                                    </td>
                                </tr>
                                <!--<tr>
                                    <td style="padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                                        <h2 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 18px; line-height: 22px; color: #333333; font-weight: bold;">Contenido</h2>
                                        <ul style="padding: 0; margin: 0 0 10px 0; list-style-type: disc;">
                                            <li style="margin:0 0 10px 30px;" class="list-item-first">A list item.</li>
                                            <li style="margin:0 0 10px 30px;">Another list item here.</li>
                                            <li style="margin: 0 0 0 30px;" class="list-item-last">Everyone gets a list item, list items for everyone!</li>
                                        </ul>
                                        <p style="margin: 0;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent laoreet malesuada cursus. Maecenas scelerisque congue eros eu posuere. Praesent in felis ut velit pretium lobortis rhoncus ut&nbsp;erat.</p>
                                    </td>
                                </tr>-->
                            </table>
                        </td>
                    </tr>
                    <!-- 1 Column Text + Button : END -->

                    <!-- 2 Even Columns : BEGIN -->
                    <!--<tr>
                        <td style="padding: 0 10px 40px 10px; background-color: #ffffff;">
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <img src="https://via.placeholder.com/200" width="200" height="" alt="alt_text" border="0" style="width: 100%; max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; padding: 10px 10px 0;">
                                                   <p style="margin: 0;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" width="50%">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td style="text-align: center; padding: 0 10px;">
                                                    <img src="https://via.placeholder.com/200" width="200" height="" alt="alt_text" border="0" style="width: 100%; max-width: 200px; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; padding: 10px 10px 0;">
                                                    <p style="margin: 0;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>-->
                    <!-- 2 Even Columns : END -->

                    <!-- Clear Spacer : BEGIN -->
                    <tr>
                        <td aria-hidden="true" height="40" style="font-size: 0px; line-height: 0px;">
                            &nbsp;
                        </td>
                    </tr>
                    <!-- Clear Spacer : END -->

                    <!-- 1 Column Text : BEGIN -->
                    <!--
                    <tr>
                        <td style="background-color: #ffffff;">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                                        <p style="margin: 0;">Maecenas sed ante pellentesque, posuere leo id, eleifend dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent laoreet malesuada cursus. Maecenas scelerisque congue eros eu posuere. Praesent in felis ut velit pretium lobortis rhoncus ut&nbsp;erat.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    -->
                    <!-- 1 Column Text : END -->

                </table>
                <!-- Email Body : END -->

                <!-- Email Footer : BEGIN -->
                <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
                    <tr>
                        <td style="padding: 20px; font-family: sans-serif; font-size: 12px; line-height: 15px; text-align: center; color: #ffffff;">
                            <a href="www.solumobil.com">
                            <webversion style="color: #ffffff; text-decoration: underline; font-weight: bold;">Ver como página web</webversion>
                            </a>
                            <br><br>
                            Solumobil<br><span class="unstyle-auto-detected-links">Cra. 116c #66a-16, Bogotá<br>(+57) 3208557457</span>
                            <br><br>
                        </td>
                    </tr>
                </table>
                <!-- Email Footer : END -->

                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </div>

            <!-- Full Bleed Background Section : BEGIN -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="background-color: #4F71FB;">
                <tr>
                    <td>
                        <div align="center" style="max-width: 600px; margin: auto;" class="email-container">
                            <!--[if mso]>
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
                            <tr>
                            <td>
                            <![endif]-->
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td style="padding: 20px; text-align: left; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #ffffff;">
                                        <p style="margin: 0;">
                                        Comprendiendo la necesidad de un producto de calidad ofrecemos una atención especial para que desde la comodidad de tu casa recibas tus aparatos celulares, tabletas o accesorios en su amplia variedad.
                                        </p>
                                    </td>
                                </tr>
                            </table>
                            <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </div>
                    </td>
                </tr>
            </table>
            <!-- Full Bleed Background Section : END -->

        <!--[if mso | IE]>
        </td>
        </tr>
        </table>
        <![endif]-->
        </center>
    </body>
    </html>`;

    return basicMAKProductHTML.toString();
}

function getProductHTMLTmpl(productId = "", title = "", content = "", images, relatedProducts) {
    let date = new Date()
    let esDate = date.getFullYear()

    let htmlProduct = html`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <!--[if gte mso 9]>
            <xml>
                <o:OfficeDocumentSettings>
                  <o:AllowPNG/>
                  <o:PixelsPerInch>96</o:PixelsPerInch>
                </o:OfficeDocumentSettings>
            </xml>
        <![endif]-->
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="format-detection" content="date=no" />
        <meta name="format-detection" content="address=no" />
        <meta name="format-detection" content="telephone=no" />
        <link href="https://fonts.googleapis.com/css?family=Gudea:400,400i,700|Hind+Guntur:400,700" rel="stylesheet" />
        <title>*|MC:SUBJECT|*</title>

    <style type="text/css" media="screen">
    [style*="HG"] {
        font-family: 'Hind Guntur', Arial, sans-serif !important
    }
    [style*="Gudea"] {
        font-family: 'Gudea', Arial, sans-serif !important
    }
    /* Linked Styles */
    body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#ffffff; -webkit-text-size-adjust:none; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px }
    a { color:#ed5258; text-decoration:none }
    p { padding:0 !important; margin:0 !important }
    img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
    table { mso-cellspacing: 0px; mso-padding-alt: 0px 0px 0px 0px; }

    /* Mobile styles */
    @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
        table[class='mobile-shell'] { width: 100% !important; min-width: 100% !important; }
        table[class='center'] { margin: 0 auto !important; }
        table[class='left'] { margin-right: auto !important; }

        td[class='td'] { width: 100% !important; min-width: 100% !important; }
        td[class='auto'] { width: 100% !important; min-width: auto !important; }

        div[class='mobile-br-3'] { height: 4px !important; background: #ffffff !important; display: block !important; }
        div[class='mobile-br-4'] { height: 4px !important; background: #2d2d31 !important; display: block !important; }
        div[class='mobile-br-5'] { height: 5px !important; }
        div[class='mobile-br-10'] { height: 10px !important; }
        div[class='mobile-br-15'] { height: 15px !important; }
        div[class='mobile-br-25'] { height: 25px !important; }

        th[class='m-td'],
        td[class='m-td'],
        div[class='hide-for-mobile'],
        span[class='hide-for-mobile'] { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

        span[class='mobile-block'] { display: block !important; }
        div[class='img-m-center'] { text-align: center !important; }
        div[class='img-m-left'] { text-align: left !important; }

        div[class='fluid-img'] img,
        td[class='fluid-img'] img { width: 100% !important; max-width: 100% !important; height: auto !important; }

        div[class='h1'],
        div[class='slogan'],
        div[class='h1-left'],
        div[class='h1-white'],
        div[class='h1-white-r'],
        div[class='h1-secondary'],
        div[class='text-footer'],
        div[class='text-footer-r'],
        div[class='text-footer-r2'],
        div[class='h2-white-m-center'],
        div[class='h1-white-secondary-r'] { text-align: center !important; }

        th[class='column'],
        th[class='column-top'],
        th[class='column-dir-t'],
        th[class='column-bottom'],
        th[class='column-dir'] { float: left !important; width: 100% !important; display: block !important; }
        div[class='text-top'] { text-align: center !important; }
        th[class='column-top-black'] { float: left !important; width: 100% !important; display: block !important; background: #2d2d31 !important; }

        td[class='item'] { width: 150px !important; }

        td[class='content-spacing'] { width: 15px !important; }
        td[class='content-spacing-pink'] { width: 15px !important; background: #ed5258 !important; }
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#ffffff; -webkit-text-size-adjust:none; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
    <tr>
        <td align="center" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#ed5258">&nbsp;</td>
                                <td align="center" width="650" class="td"
                                    style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell"
                                        bgcolor="#ed5258">
                                        <tr>
                                            <td class="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th class="column"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="25" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <table class="center" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td class="text-top2"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'HG'; font-size:11px; line-height:16px; text-align:left; text-transform:uppercase; font-weight:bold">
                                                                                    <a href="${process.env.CLIENT_URL}/product/${productId}"
                                                                                        target="_blank"
                                                                                        class="link-white-u"
                                                                                        style="color:#ffffff; text-decoration:underline">
                                                                                        <span class="link-white-u"
                                                                                            style="color:#ffffff; font-size: 2rem;
                                                                                text-decoration:underline">
                                                                                            Ver online
                                                                            </span>
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="25" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th class="column"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                            width="240" bgcolor="#f6f6f6">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td align="right">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="25" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <table class="center" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <img width="250" src="https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg" alt="Solumobil" />
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="25" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#f6f6f6">&nbsp;
                    </td>
                            </tr>
                        </table>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#2d2d31">
                            <tr>
                                <td align="center">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer"
                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                        <tr>
                                            <td height="30" class="spacer"
                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                &nbsp;</td>
                                        </tr>
                                    </table>

                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                                        <tr>
                                            <td class="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                    bgcolor="#2d2d31">
                                                    <tr>
                                                        <th class="column"
                                                            style="padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img" style="text-align:left">
                                                                        <div class="img-m-center"
                                                                            >
                                                                            <a
                                                                                href="solumobil.com" target="_blank">
                                                                                Solumobil
                                                                             </a>
                                                                        </div>
                                                                        <div style="font-size:0pt; line-height:0pt;"
                                                                            class="mobile-br-15"></div>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th class="column"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                            width="240">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <table class="left" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td class="text-top2"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'HG'; font-size:11px; line-height:16px; text-align:left; text-transform:uppercase; font-weight:bold">
                                                                                    <a href="${process.env.CLIENT_URL}/?search=all"
                                                                                        target="_blank"
                                                                                        class=""
                                                                                        style="color:#ffffff; >
                                                                                        <span
                                                                                            class="link-white-u" style="color:#ffffff;font-size:1.6rem; left: 0; text-decoration:none;
                                                                                >SOLUMOBIL</span>
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td align="right">
                                                                        <table class="center" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td class="text-top2"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'HG'; font-size:11px; line-height:16px; text-align:left; text-transform:uppercase; font-weight:bold">
                                                                                    <a href="${process.env.CLIENT_URL}/?search=all"
                                                                                        target="_blank"
                                                                                        class=""
                                                                                        style="color:#ffffff; text-decoration:underline">
                                                                                        <span
                                                                                            class="link-white-u" style="color:#ffffff;
                                                                                text-decoration:underline">
                                                                                            VE A LA TIENDA
                                                                            </span>
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer"
                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                        <tr>
                                            <td height="30" class="spacer"
                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                &nbsp;</td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                        bgcolor="#ed5258">&nbsp;</td>
                    <td align="center" width="650" class="td"
                        style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                        <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell"
                            bgcolor="#ed5258">
                            <tr>
                                <td class="td"
                                    style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <th class="column"
                                                style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                         <td class="fluid-img"
                                                            style="font-size:0pt; line-height:0pt; text-align:left">
                                                            <img src="${images[0] ? images[0].url : 'https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg'}" border="0" width="409"
                                                                height="364" alt="" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </th>
                                            <th class="column"
                                                style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                width="240" bgcolor="#f6f6f6">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="content-spacing"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            width="1"></td>
                                                        <td align="right">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0" class="spacer"
                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                <tr>
                                                                    <td height="25" class="spacer"
                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                        &nbsp;</td>
                                                                </tr>
                                                            </table>

                                                            <div class="h1"
                                                                style="color:#2d2d31; font-family:impact, 'AvenirNextCondensed-Bold', Arial,sans-serif; font-size:60px; line-height:64px; text-align:right; text-transform:uppercase">

                                                                ${title}
                                                            </div>
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0" class="spacer"
                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                <tr>
                                                                    <td height="25" class="spacer"
                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                        &nbsp;</td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                        <td class="content-spacing"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            width="1"></td>
                                                    </tr>
                                                </table>
                                            </th>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                        bgcolor="#f6f6f6">&nbsp;</td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#2d2d31">
                <tr>
                    <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="1">
                        &nbsp;</td>
                    <td align="center">
                        <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                            <tr>
                                <td class="td"
                                    style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer"
                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                        <tr>
                                            <td height="42" class="spacer"
                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                &nbsp;</td>
                                        </tr>
                                    </table>

                                    <div class="h2-white-center"
                                        style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:26px; line-height:30px; text-align:center; font-weight:bold">
                                        Gran oferta a partir de ${esDate}</div>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer"
                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                        <tr>
                                            <td height="22" class="spacer"
                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                &nbsp;</td>
                                        </tr>
                                    </table>

                                    <div class="text-light-grey"
                                        style="color:#b6b6b6; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                        ${content}
                                    </div>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer"
                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                        <tr>
                                            <td height="25" class="spacer"
                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                &nbsp;</td>
                                        </tr>
                                    </table>

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center">
                                                <table class="center" border="0" cellspacing="0" cellpadding="0"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td class="img"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            height="34" width="12"></td>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0" class="spacer"
                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                <tr>
                                                                    <td height="6" class="spacer"
                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                        &nbsp;</td>
                                                                </tr>
                                                            </table>

                                                            <div class="button1"
                                                                style="color:#ffffff; font-family:Arial,sans-serif; font-size:13px; line-height:20px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                <a href="${process.env.CLIENT_URL}/product/${productId}" target="_blank" class="link-white"
                                                                    style="color:#ffffff; text-decoration:none">
                                                                    <span
                                                                        class="link-white"
                                                                        style="color:#ffffff; text-decoration:none">
                                                                        Comprar ahora
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0" class="spacer"
                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                <tr>
                                                                    <td height="6" class="spacer"
                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                        &nbsp;</td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                        <td class="img"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            width="12"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer"
                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                        <tr>
                                            <td height="42" class="spacer"
                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                &nbsp;</td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="1">
                        &nbsp;</td>
                </tr>
            </table>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#ed5258">&nbsp;</td>
                                <td align="center" width="650" class="td"
                                    style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell"
                                        bgcolor="#ed5258">
                                        <tr>
                                            <td class="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="content-spacing"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            width="1"></td>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <th class="column"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td class="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0"
                                                                                        class="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="45"
                                                                                                class="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div class="h2-white"
                                                                                        style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:26px; line-height:30px; text-align:left; font-weight:bold">
                                                                                        Productos relacionados
                                                                                    </div>
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0"
                                                                                        class="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="35"
                                                                                                class="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                    <th class="m-td"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        bgcolor="#f6f6f6" width="240">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td class="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="1"></td>
                                                                                <td>
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0"
                                                                                        class="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="25"
                                                                                                class="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0"
                                                                                        class="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="25"
                                                                                                class="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                                <td class="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="30"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="content-spacing-pink"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            bgcolor="#f6f6f6" width="1"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#f6f6f6">&nbsp;
                    </td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#ed5258">&nbsp;</td>
                                <td align="center" width="650" class="td"
                                    style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell"
                                        bgcolor="#ed5258">
                                        <tr>
                                            <td class="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="77" bgcolor="#f6f6f6">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="10"></td>
                                                                    <td>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="10" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <div
                                                                            style="color:#2d2d31; font-family:Arial,sans-serif, 'HG'; font-size:30px; line-height:34px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                            <a href="${process.env.CLIENT_URL}/${relatedProducts[0] ? 'product/' : 'shop'}${relatedProducts[0] ? relatedProducts[0]._id : ''}">
                                                                            <img height="100%" width="100%" src="${!relatedProducts[0] ? 'https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg' :
            relatedProducts[0].images[0] ? relatedProducts[0].images[0].url : 'https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg'}"/>
                                                                        </a>

                                                                        </div>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="10" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="10"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="img"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            width="20" bgcolor="#ed5258">&nbsp;</td>
                                                        <td bgcolor="#f6f6f6">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="20"></td>
                                                                    <td>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="20" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <th class="column"
                                                                                    style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div class="text"
                                                                                                    style="color:#2d2d31; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left; font-weight:normal">
                                                                                                    ${relatedProducts[0] ? relatedProducts[0].content : ''}
                                                                                                    </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                                <th class="column"
                                                                                    style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                                                    width="170">
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <!-- Button -->
                                                                                                <div style="font-size:0pt; line-height:0pt;"
                                                                                                    class="mobile-br-15">
                                                                                                </div>

                                                                                                <table width="100%"
                                                                                                    border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td
                                                                                                            align="right">
                                                                                                            <table
                                                                                                                class="left"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                bgcolor="#2d2d31">
                                                                                                                <tr>
                                                                                                                    <td class="img"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                                        height="34"
                                                                                                                        width="12">
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <table
                                                                                                                            width="100%"
                                                                                                                            border="0"
                                                                                                                            cellspacing="0"
                                                                                                                            cellpadding="0"
                                                                                                                            class="spacer"
                                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                            <tr>
                                                                                                                                <td height="6"
                                                                                                                                    class="spacer"
                                                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                    &nbsp;
                                                                                                                    </td>
                                                                                                                            </tr>
                                                                                                                        </table>

                                                                                                                        <div class="button1"
                                                                                                                            style="color:#ffffff; font-family:Arial,sans-serif; font-size:13px; line-height:20px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                                                            <a href="${relatedProducts[0] ? process.env.CLIENT_URL + '/product/' + relatedProducts[0]._id : 'www.solumobil.com/shop'}"
                                                                                                                                target="_blank"
                                                                                                                                class="link-white"
                                                                                                                                style="color:#ffffff; text-decoration:none">
                                                                                                                                <span
                                                                                                                                    class="link-white"
                                                                                                                                    style="color:#ffffff; text-decoration:none">
                                                                                                                                    Ver mas
                                                                                                                                    </span>
                                                                                                                            </a>
                                                                                                                        </div>
                                                                                                                        <table
                                                                                                                            width="100%"
                                                                                                                            border="0"
                                                                                                                            cellspacing="0"
                                                                                                                            cellpadding="0"
                                                                                                                            class="spacer"
                                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                            <tr>
                                                                                                                                <td height="6"
                                                                                                                                    class="spacer"
                                                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>

                                                                                                                    </td>
                                                                                                                    <td class="img"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                                        width="12">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <!-- END Button -->
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="20" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                    <td class="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#f6f6f6">&nbsp;
                    </td>
                            </tr>
                        </table>
                        <!--
            <!-- END EVENT Row -->
                        <!--
            <!-- Separator -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#ed5258">&nbsp;</td>
                                <td align="center" width="650" class="td"
                                    style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell"
                                        bgcolor="#ed5258">
                                        <tr>
                                            <td class="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th class="column"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="20" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th class="m-td"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            bgcolor="#f6f6f6" width="240">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                    <td>
                                                                        <div class="hide-for-mobile">
                                                                            <table width="100%" border="0"
                                                                                cellspacing="0" cellpadding="0"
                                                                                class="spacer"
                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                <tr>
                                                                                    <td height="20" class="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                    <td class="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="30"></td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#f6f6f6">&nbsp;
                    </td>
                            </tr>
                        </table>
                        <!--
            <!-- END Separator -->

                        <!--
            <!-- EVENT Row  -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#ed5258">&nbsp;</td>
                                <td align="center" width="650" class="td"
                                    style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell"
                                        bgcolor="#ed5258">
                                        <tr>
                                            <td class="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="77" bgcolor="#f6f6f6">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="10"></td>
                                                                    <td>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="10" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <div
                                                                            style="color:#2d2d31; font-family:Arial,sans-serif, 'HG'; font-size:30px; line-height:34px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                            <a href="${relatedProducts[1] ? process.env.CLIENT_URL + '/product/' + relatedProducts[1]._id : 'www.solumobil.com/shop'}">
                                                                            <img height="100%" width="100%" src="${!relatedProducts[1] ? 'https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg' :
            relatedProducts[1].images[0] ? relatedProducts[1].images[0].url : 'https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg'}"/>
                                                                            </a>

                                                                        </div>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="10" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="10"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="img"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            width="20" bgcolor="#ed5258">&nbsp;</td>
                                                        <td bgcolor="#f6f6f6">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="20"></td>
                                                                    <td>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="20" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <th class="column"
                                                                                    style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div class="text"
                                                                                                    style="color:#2d2d31; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left; font-weight:normal">
                                                                                                    ${relatedProducts[1] ? relatedProducts[1].content : ''}</div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                                <th class="column"
                                                                                    style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                                                    width="170">
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <!-- Button -->
                                                                                                <div style="font-size:0pt; line-height:0pt;"
                                                                                                    class="mobile-br-15">
                                                                                                </div>

                                                                                                <table width="100%"
                                                                                                    border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td
                                                                                                            align="right">
                                                                                                            <table
                                                                                                                class="left"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                bgcolor="#2d2d31">
                                                                                                                <tr>
                                                                                                                    <td class="img"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                                        height="34"
                                                                                                                        width="12">
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <table
                                                                                                                            width="100%"
                                                                                                                            border="0"
                                                                                                                            cellspacing="0"
                                                                                                                            cellpadding="0"
                                                                                                                            class="spacer"
                                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                            <tr>
                                                                                                                                <td height="6"
                                                                                                                                    class="spacer"
                                                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                    &nbsp;
                                                                                                                    </td>
                                                                                                                            </tr>
                                                                                                                        </table>

                                                                                                                        <div class="button1"
                                                                                                                            style="color:#ffffff; font-family:Arial,sans-serif; font-size:13px; line-height:20px; text-align:center; text-transform:uppercase; font-weight:bold">

                                                                                                                            <a href="${relatedProducts[1] ? process.env.CLIENT_URL + '/product/' + relatedProducts[1]._id : 'www.solumobil.com/shop'}"

                                                                                                                                target="_blank"
                                                                                                                                class="link-white"
                                                                                                                                style="color:#ffffff; text-decoration:none">
                                                                                                                                <span
                                                                                                                                    class="link-white"
                                                                                                                                    style="color:#ffffff; text-decoration:none">Ver mas
                                                                                                                                    </span>
                                                                                                                                    </a>
                                                                                                                        </div>
                                                                                                                        <table
                                                                                                                            width="100%"
                                                                                                                            border="0"
                                                                                                                            cellspacing="0"
                                                                                                                            cellpadding="0"
                                                                                                                            class="spacer"
                                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                            <tr>
                                                                                                                                <td height="6"
                                                                                                                                    class="spacer"
                                                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                    &nbsp;
                                                                                                                    </td>
                                                                                                                            </tr>
                                                                                                                        </table>

                                                                                                                    </td>
                                                                                                                    <td class="img"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                                        width="12">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <!-- END Button -->
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="20" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                    <td class="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#f6f6f6">&nbsp;
                    </td>
                            </tr>
                        </table>
                        <!--
            <!-- END EVENT Row -->
                        <!--
            <!-- Separator -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#ed5258">&nbsp;</td>
                                <td align="center" width="650" class="td"
                                    style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell"
                                        bgcolor="#ed5258">
                                        <tr>
                                            <td class="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th class="column"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="20" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th class="m-td"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            bgcolor="#f6f6f6" width="240">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                    <td>
                                                                        <div class="hide-for-mobile">
                                                                            <table width="100%" border="0"
                                                                                cellspacing="0" cellpadding="0"
                                                                                class="spacer"
                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                <tr>
                                                                                    <td height="20" class="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                    <td class="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="30"></td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#f6f6f6">&nbsp;
                    </td>
                            </tr>
                        </table>
                        <!--
            <!-- END Separator -->

                        <!--
            <!-- EVENT Row  -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#ed5258">&nbsp;</td>
                                <td align="center" width="650" class="td"
                                    style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell"
                                        bgcolor="#ed5258">
                                        <tr>
                                            <td class="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="77" bgcolor="#f6f6f6">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="10"></td>
                                                                    <td>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="10" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <div
                                                                        style="color:#2d2d31; font-family:Arial,sans-serif, 'HG'; font-size:30px; line-height:34px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                        <a href="${relatedProducts[2] ? process.env.CLIENT_URL + '/product/' + relatedProducts[2]._id : 'www.solumobil.com/shop'}">
                                                                            <img height="100%" width="100%" src="${!relatedProducts[2] ? 'https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg' :
            relatedProducts[2].images[0] ? relatedProducts[2].images[0].url : 'https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg'}"/>
                                                                            </a>

                                                                    </div>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="10" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="10"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="img"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            width="20" bgcolor="#ed5258">&nbsp;</td>
                                                        <td bgcolor="#f6f6f6">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="20"></td>
                                                                    <td>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="20" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <th class="column"
                                                                                    style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div class="text"
                                                                                                    style="color:#2d2d31; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left; font-weight:normal">
                                                                                                    ${relatedProducts[2] ? relatedProducts[2].content : ''}</div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                                <th class="column"
                                                                                    style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                                                    width="170">
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <!-- Button -->
                                                                                                <div style="font-size:0pt; line-height:0pt;"
                                                                                                    class="mobile-br-15">
                                                                                                </div>

                                                                                                <table width="100%"
                                                                                                    border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td
                                                                                                            align="right">
                                                                                                            <table
                                                                                                                class="left"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                bgcolor="#2d2d31">
                                                                                                                <tr>
                                                                                                                    <td class="img"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                                        height="34"
                                                                                                                        width="12">
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <table
                                                                                                                            width="100%"
                                                                                                                            border="0"
                                                                                                                            cellspacing="0"
                                                                                                                            cellpadding="0"
                                                                                                                            class="spacer"
                                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                            <tr>
                                                                                                                                <td height="6"
                                                                                                                                    class="spacer"
                                                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                    &nbsp;
                                                                                                                    </td>
                                                                                                                            </tr>
                                                                                                                        </table>

                                                                                                                        <div class="button1"
                                                                                                                            style="color:#ffffff; font-family:Arial,sans-serif; font-size:13px; line-height:20px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                                                            <a href="${relatedProducts[2] ? process.env.CLIENT_URL + '/product/' + relatedProducts[2]._id : 'www.solumobil.com/shop'}"

                                                                                                                                target="_blank"
                                                                                                                                class="link-white"
                                                                                                                                style="color:#ffffff; text-decoration:none">
                                                                                                                                <span
                                                                                                                                    class="link-white"
                                                                                                                                    style="color:#ffffff; text-decoration:none">Ver mas
                                                                                                                                    </span>
                                                                                                                                    </a>
                                                                                                                        </div>
                                                                                                                        <table
                                                                                                                            width="100%"
                                                                                                                            border="0"
                                                                                                                            cellspacing="0"
                                                                                                                            cellpadding="0"
                                                                                                                            class="spacer"
                                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                            <tr>
                                                                                                                                <td height="6"
                                                                                                                                    class="spacer"
                                                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                    &nbsp;
                                                                                                                    </td>
                                                                                                                            </tr>
                                                                                                                        </table>

                                                                                                                    </td>
                                                                                                                    <td class="img"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                                        width="12">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <!-- END Button -->
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="20" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                    <td class="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#f6f6f6">&nbsp;
                    </td>
                            </tr>
                        </table>
                        <!--
            <!-- END EVENT Row -->
                        <!--
            <!-- Separator -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#ed5258">&nbsp;</td>
                                <td align="center" width="650" class="td"
                                    style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell"
                                        bgcolor="#ed5258">
                                        <tr>
                                            <td class="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th class="column"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="20" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th class="m-td"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            bgcolor="#f6f6f6" width="240">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                    <td>
                                                                        <div class="hide-for-mobile">
                                                                            <table width="100%" border="0"
                                                                                cellspacing="0" cellpadding="0"
                                                                                class="spacer"
                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                <tr>
                                                                                    <td height="20" class="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                    <td class="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="30"></td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#f6f6f6">&nbsp;
                    </td>
                            </tr>
                        </table>
                        <!--
            <!-- END Separator -->

                        <!--
            <!-- Separator -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#ed5258">&nbsp;</td>
                                <td align="center" width="650" class="td"
                                    style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell"
                                        bgcolor="#ed5258">
                                        <tr>
                                            <td class="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th class="column"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" class="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="40" class="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th class="m-td"
                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                            bgcolor="#f6f6f6" width="240">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                    <td>
                                                                        <div class="hide-for-mobile">
                                                                            <table width="100%" border="0"
                                                                                cellspacing="0" cellpadding="0"
                                                                                class="spacer"
                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                <tr>
                                                                                    <td height="40" class="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                    <td class="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="30"></td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#f6f6f6">&nbsp;
                    </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>`;

    return htmlProduct.toString();
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++)
        await callback(array[index], index, array);
}

async function waitFor(ms) {
    new Promise(r => setTimeout(r, ms));
}

function keywordsExtract(string, length) {

    string = string.replace(/[^a-zA-ZáéóíúÁÉÍÓÚ ]/g, "")
    let arrStrs = string.split(" ")
    let fullStr = "";
    let arrLength = arrStrs.length

    arrStrs.forEach((str, index) => {
        if(arrLength != index){
            if(str.length > length) fullStr += str + ", "
        }else{
            if(str.length > length) fullStr += str
        }
    })
    return fullStr
}

export {
    validateEmail,
    getFullEsDate,
    getEsDate,
    getProductHTMLTmpl,
    genBasicProductHTML,
    currencyFormat,
    convertCurrency,
    asyncForEach,
    waitFor,
    blobToFile,
    makeID,
    keywordsExtract
};
