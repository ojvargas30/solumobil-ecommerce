import { validateEmail } from './funcs';

const valid = (name, email, password, cf_password) => {
    if (!name || !email || !password)
        return 'Por favor llena todos los campos'

    if (!validateEmail(email))
        return 'Correos Inválidos'

    if (password.length < 8)
        return 'La contraseña debe tener al menos 8 caracteres'

    if (password !== cf_password)
        return 'La contraseña de confirmación no coincide'
}

export default valid;