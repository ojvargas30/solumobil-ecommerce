import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { getData } from '../utils/fetchData'
import filterSearch from '../utils/filterSearch'

const Filter = ({ state }) => {

    // CONSTANTES FILTROS
    const [category, setCategory] = useState('')
    const [search, setSearch] = useState('')
    const [sort, setSort] = useState('')
    // END CONSTANTES FILTROS

    const { categories, cart } = state
    const router = useRouter()

    const handleCategory = e => {
        // console.log(e.target.dataset.category)
        setCategory(e.target.dataset.category)

        // FILTRO
        filterSearch({ router, category: e.target.dataset.category })
    }

    const handleSort = e => {
        // console.log(e.target.dataset.sort)
        setSort(e.target.dataset.sort)

        // FILTRO
        filterSearch({ router, sort: e.target.dataset.sort })
    }

    // useEffect(() => {
    //     // console.log(search);
    //     // if (search) getData(`product`)
    //     filterSearch({ router, search: search ? search : 'all' })

    // }, [search])

    const handleKeyPress = e => {
        console.log(e.key)
        if (e.key === 'Enter') router.push(`/shop/${search}`)
    }

    // const scrollDown = e => {
    //     window.scrollTo(0, window.innerHeight - (window.innerHeight * 0.15));
    // }

    return (
        <>
            <div className="container">
                <div className="mid-section main-info-area">

                    <div className="wrap-logo-top left-section">
                        <Link scroll={false} href="/">
                            <a className="link-to-home">
                                {/* IMAGÉNES CARROUSEL? */}
                                <img src="https://res.cloudinary.com/linda-leblanc/image/upload/v1624064324/esolumobil/efjowfqadzxngxejr1aq.jpg" alt="Solumobil" />
                            </a>
                        </Link>
                    </div>
                    <div className="wrap-search center-section">
                        <div className="wrap-search-form">
                            {/* <form autoComplete="off" id="form-search-top" name="form-search-top"> */}

                            <input
                                type="text"
                                name="search"
                                placeholder="Buscar por marca, categoría o especificación"
                                value={search}
                                onChange={e => { e.target.value = e.target.value.toUpperCase(); setSearch(e.target.value) }}
                                onKeyPress={handleKeyPress}
                            // onInput={e => setSearch(e.target.value)}
                            // onKeyUp={scrollDown}
                            />

                            <Link href={`/shop/${search}`}>
                                <a id="searcherA" form="form-search-top" className="bg-solumobil-red d-flex align-items-center text-center justify-content-center">
                                    <i className="fas fa-search" aria-hidden="true"></i>
                                </a>
                            </Link>

                            <div className="wrap-list-cate">
                                <input type="hidden" name="product-cate" id="product-cate" />

                                <Link scroll={false} href="/shop/category/all">
                                    <a className="link-control cursor-pointer">Todas las categorías</a>
                                </Link>

                                <ul className="list-cate">
                                    {/* level 0,1,2 */}
                                    <Link scroll={false} href="/shop/category/all">
                                        <a>
                                            <li className="level-0">Todas las categorías</li>
                                        </a>
                                    </Link>

                                    {
                                        categories.map(item => (
                                            <Link key={item._id} scroll={false} href={`/shop/category/${item._id}`}>
                                                <a>
                                                    <li
                                                        className="level-1">
                                                        {item.name}
                                                    </li>
                                                </a>
                                            </Link>
                                        ))
                                    }
                                    {/* <li className="level-2">Batteries & Chargens</li> */}
                                </ul>
                            </div>
                            {/* </form> */}
                        </div>
                    </div>

                    <div className="wrap-icon right-section">
                        <div className="wrap-icon-section minicart">
                            <Link scroll={false} href="/cart">
                                <a className="link-direction">
                                    <i className="fa fa-shopping-basket" aria-hidden="true"></i>
                                    <div className="left-info d-block">
                                        <span className="index">{cart.length} {(cart.length > 1) ? 'items' : 'item'}</span>
                                        <span className="title">Carrito</span>
                                    </div>
                                </a>
                            </Link>
                        </div>
                        <div className="wrap-icon-section show-up-after-1024 shadow shadow-sm">
                            <a href="false" onClick={e => e.preventDefault()} className="mobile-navigation cursor-pointer">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>

            <div className="nav-section" style={{ width: "100% !important" }}>
                <div className="header-nav-section">
                    <div className="container">
                        {/* <ul className="nav menu-nav clone-main-menu" data-menuname="Registrarse" data-href="/signup" ></ul>
                        <ul className="nav menu-nav clone-main-menu" data-menuname="Iniciar Sesión" data-href="/signin" ></ul> */}
                        <ul className="nav menu-nav clone-main-menu" data-menuname="Menú principal" data-href="/" ></ul>
                        <ul className="nav menu-nav clone-main-menu" data-menuname="Servicio técnico" data-href="https://tec.solumobil.com/" ></ul>
                        <ul className="nav menu-nav clone-main-menu" data-menuname="Carrito" data-href="/cart" ></ul>
                        <ul className="nav menu-nav clone-main-menu" data-menuname="Tienda" data-href="/shop" ></ul>
                        <ul className="nav menu-nav clone-main-menu" data-menuname="Contacto" data-href="https://api.whatsapp.com/send?phone=573208557457&text=%C2%A1Saludos! vengo de la web y quisiera saber mas de sus productos.">
                            {/* <li className="menu-item">
                                <a
                                    // href="javascript:void(0)"
                                    onClick={handleSort, scrollDown}
                                    data-sort="weekPopular"
                                    className="link-term cursor-pointer">Destacados semana</a>
                                <span className="nav-label hot-label">hot</span>
                            </li> */}
                            <li className="menu-item">
                                <Link scroll={false} href="/shop/sort/price">
                                    <a
                                        // onClick={handleSort, scrollDown}
                                        // data-sort="price"
                                        className="link-term cursor-pointer">
                                        En oferta</a>
                                </Link>
                                <span className="nav-label hot-label">hot</span>
                            </li>
                            <li className="menu-item">
                                <Link scroll={false} href="/shop/sort/-createdAt">
                                    <a
                                        // onClick={handleSort, scrollDown}
                                        // data-sort="-createdAt"
                                        className="link-term cursor-pointer">
                                        Novedades</a>
                                </Link>
                                <span className="nav-label hot-label">hot</span>
                            </li>
                            <li className="menu-item">
                                <Link scroll={false} href="/shop/sort/-sold">
                                    <a
                                        // onClick={handleSort, scrollDown}
                                        // data-sort="-sold"
                                        className="link-term cursor-pointer">
                                        Mas vendidos</a>
                                </Link>
                                <span className="nav-label hot-label">hot</span>
                            </li>
                            {/* <li className="menu-item">
                                <a
                                    onClick={handleSort, scrollDown}
                                    data-sort="mostValued"
                                    className="link-term cursor-pointer">Mas valorados</a>
                                <span className="nav-label hot-label">hot</span>
                            </li> */}
                            <li className="menu-item">
                                <Link scroll={false} href="/shop/sort/oldest">
                                    <a
                                        // data-sort="oldest"
                                        className="link-term cursor-pointer">Variados
                                    </a>
                                </Link>
                            </li>
                            <li className="menu-item">
                                <Link scroll={false} href="/shop/sort/-price">
                                    <a
                                        // data-sort="-price"
                                        className="link-term cursor-pointer">Lujosos</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="primary-nav-section mb-1">
                    <div className="container">
                        <ul className="nav bg-white primary border-top justify-content-center text-center" id="mercado_main"
                            data-menuname="Menú principal" data-href="/" >
                            <li className="menu-item home-icon bg-white d-flex p-1 align-items-center border-0">
                                <Link href="/" >
                                    <a className="link-term mercado-item-title">
                                        <i className="fas fa-home fa-sm p-1 text-dark" aria-hidden="true"></i>
                                    </a>
                                </Link>
                            </li>
                            {/* <li className="menu-item">
                                <Link href="/">
                                    <a className="link-term mercado-item-title">Acerca de nosotros</a>
                                </Link>
                            </li> */}
                            <li className="menu-item d-flex align-items-center bg-white border-0">
                                <Link scroll={false} href="/shop">
                                    <a className="link-term mercado-item-title text-dark">Tienda</a>
                                </Link>
                            </li>
                            <li className="menu-item d-flex align-items-center bg-white border-0">
                                <Link scroll={false} href="/cart">
                                    <a className="link-term mercado-item-title text-dark">Carrito</a>
                                </Link>
                            </li>
                            <li className="menu-item d-flex align-items-center bg-white border-0">
                                <Link href="tel:573208557457">
                                    <a className="link-term mercado-item-title text-dark">Contacto</a>
                                </Link>
                            </li>
                            <li className="menu-item d-flex align-items-center bg-white border-0">
                                <Link href="https://solumovil-tigo.negocio.site/?m=true">
                                    <a target="_blank" className="link-term mercado-item-title text-dark">Cotizar</a>
                                </Link>
                            </li>
                            {/* <li className="menu-item d-flex align-items-center">
                                <Link href="https://tec.solumobil.com/">
                                    <a className="link-term mercado-item-title" target="_blank">Servicio técnico</a>
                                </Link>
                            </li> */}
                            {/* <li className="menu-item">
                                <Link href="/" >
                                    <a className="link-term mercado-item-title">Pago</a>
                                </Link>
                            </li> */}
                            {/* <li className="menu-item d-flex align-items-center">
                                <Link href="https://www.whatsapp.com/catalog/573133043714/?app_absent=0">
                                    <a target="_blank" className="link-term mercado-item-title">Catálogo Whatsapp</a>
                                </Link>
                            </li> */}
                        </ul>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Filter;
