import { addToCart } from '../../store/Actions'
import Link from 'next/link'
import { currencyFormat } from '../../utils/funcs'

const Shop = ({ handleCategory, handleBrand, handleSort, products, state, dispatch, sort, category, brand }) => {

    const { categories, brands, cart, selectedCurrency, currencies } = state

    return (
        <>

            <main id="main" className="main-site left-sidebar">
                <div className="container">
                    <div className="wrap-breadcrumb">
                        <ul>
                            <li className="item-link">
                                <Link href="/">
                                    <a className="link">Página principal</a>
                                </Link>
                            </li>
                            <li className="item-link">
                                <span>Digital y electrónica</span>
                            </li>
                        </ul>
                    </div>
                    <div className="row">

                        <div className="col-lg-9 col-md-8 col-sm-8 col-xs-12 main-content-area">

                            <div className="banner-shop" title="NIA AUDIFONOS BLUETOOH">
                                <Link href={process.env.CLIENT_URL == "http://localhost:3000"
                                    ? `/product/60e0d0ff6a0a930f246c6c59`
                                    : `/product/60d7f4e82d2c0a0008ea4543`
                                }>
                                    <a title="NIA AUDIFONOS BLUETOOH" className="banner-link">
                                        <figure title="NIA AUDIFONOS BLUETOOH">
                                            <img src="https://res.cloudinary.com/linda-leblanc/image/upload/v1624637303/esolumobil/bruo2yzyuffttiduc5dp.jpg"
                                                alt="NIA AUDIFONOS BLUETOOH" />
                                        </figure>
                                    </a>
                                </Link>
                            </div>

                            <div className="wrap-shop-control">

                                <h1 className="shop-title">Digital y electrónica</h1>

                                <div className="wrap-right">

                                    <div className="sort-item orderby ">
                                        <select className="use-chosen form-control"
                                            value={sort} onChange={handleSort}>
                                            <option value="orderall">Ordenación por defecto</option>
                                            <option value="-sold">Ordenar por popularidad</option>
                                            {/* <option value="rating">Ordenar por calificación promedio</option> */}
                                            <option value="-createdAt">Ordenar por novedad</option>
                                            <option value="oldest">Ordenar por antiguedad</option>
                                            <option value="price">Ordenar por precio: bajo a alto</option>
                                            <option value="-price">Ordenar por precio: alto a bajo</option>
                                        </select>
                                    </div>

                                    {/* <div className="sort-item product-per-page">
                                        <select name="post-per-page" className="use-chosen" >
                                            <option value="12" selected="selected">12 per page</option>
                                            <option value="16">16 per page</option>
                                            <option value="18">18 per page</option>
                                            <option value="21">21 per page</option>
                                            <option value="24">24 per page</option>
                                            <option value="30">30 per page</option>
                                            <option value="32">32 per page</option>
                                        </select>
                                    </div> */}

                                    {/* <div className="change-display-mode">
                                        <a href="#" className="grid-mode display-mode active"><i className="fa fa-th"></i>Grid</a>
                                        <a href="list.html" className="list-mode display-mode"><i className="fa fa-th-list"></i>List</a>
                                    </div> */}

                                </div>

                            </div>
                            <div className="row">

                                <ul className="product-list grid-products equal-container" style={{ display: "contents" }}>

                                    {
                                        products.length === 0
                                            ? <h2 className="p-3 m-5 text-center">Sin productos</h2>
                                            : products.map(product => <li key={product._id} className="col-lg-4 col-md-6 col-sm-6 col-xs-6 ">
                                                <div className="product product-style-3 equal-elem ">
                                                    <div className="product-thumnail" title={product.title}>
                                                        <Link href={`/product/${product._id}`}>
                                                            <a title={product.title.replace(/\b\w/g, l => l.toUpperCase())}>
                                                                <figure>
                                                                    <img style={{ height: "250px", width: "250px" }}
                                                                        src={product.images[0].url} alt={product.title} />
                                                                </figure>
                                                            </a>
                                                        </Link>
                                                    </div>
                                                    <div className="product-info" title={product.title}>
                                                        <Link href={`/product/${product._id}`}>
                                                            <a className="product-name">
                                                                <span>{product.title.replace(/\b\w/g, l => l.toUpperCase())}</span>
                                                            </a>
                                                        </Link>
                                                        <div className="wrap-price">
                                                            <span className="product-price">
                                                                {
                                                                    selectedCurrency.selected == 'cop'
                                                                        ? currencyFormat(product.price)
                                                                        : selectedCurrency.selected == 'usd'
                                                                            ? currencyFormat((product.price / currencies.usd).toFixed(2), 'en-US', "USD")
                                                                            : ''
                                                                }
                                                            </span>
                                                        </div>

                                                        <button type="button" onClick={() => {
                                                            dispatch(addToCart(product, cart, 'warning'))
                                                            // dispatch({ type: 'NOTIFY', payload: { success: `Se añadio el producto al carrito` } })

                                                        }}
                                                            className="btn add-to-cart shadow">
                                                            Añadir al carrito
                                                        </button>
                                                    </div>
                                                </div>
                                            </li>)
                                    }

                                </ul>

                            </div>

                            {/* <div className="wrap-pagination-info">
                                <ul className="page-numbers">
                                    <li><span className="page-number-item current" >1</span></li>
                                    <li><a className="page-number-item" href="#" >2</a></li>
                                    <li><a className="page-number-item" href="#" >3</a></li>
                                    <li><a className="page-number-item next-link" href="#" >Next</a></li>
                                </ul>
                                <p className="result-count">Showing 1-8 of 12 result</p>
                            </div> */}
                        </div>

                        <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12 sitebar">
                            <div className="widget mercado-widget categories-widget">
                                <h1 className="d-block">Filtrar por:</h1>
                                <h2 className="widget-title">Categoría</h2>
                                <div className="widget-content">
                                    <ul className="list-category">
                                        {/* <li className="category-item has-child-cate">
                                            <a href="#" className="cate-link">Digital & Electronics</a>
                                            <span className="toggle-control">+</span>
                                            <ul className="sub-cate">
                                                <li className="category-item"><a href="#" className="cate-link">Batteries (22)</a></li>
                                                <li className="category-item"><a href="#" className="cate-link">Headsets (16)</a></li>
                                                <li className="category-item"><a href="#" className="cate-link">Screen (28)</a></li>
                                            </ul>
                                        </li> */}

                                        <li className="level-0 cate-link cursor-pointer mb-2 hover-red" onClick={handleCategory} data-category="all">
                                            {/* <Link href="#" scroll={false}> */}
                                            <span onClick={handleCategory}
                                                data-category="all" className="cate-link cursor-pointer">Todas las categorías</span>
                                            {/* </Link> */}
                                        </li>

                                        {
                                            categories.map(item => (
                                                <li className="category-item level-1 cate-link my-2"
                                                    onClick={handleCategory} data-category={item._id} key={item._id}>
                                                    {/* <Link href="#" scroll={false}> */}
                                                    <span onClick={handleCategory}
                                                        data-category={item._id}
                                                        className="cate-link cursor-pointer hover-red">{item.name}
                                                    </span>
                                                    {/* </Link> */}
                                                </li>
                                            ))
                                        }
                                    </ul>
                                </div>
                            </div>

                            <div className="widget mercado-widget filter-widget brand-widget mt-2">
                                <h2 className="widget-title">Marca</h2>
                                <div className="widget-content">
                                    <ul className="list-style vertical-list list-limited">
                                        <li className="list-item my-2" onClick={handleBrand}
                                            data-brand="all">
                                            <span onClick={handleBrand}
                                                data-brand="all" className="filter-link cursor-pointer hover-red">
                                                Todas las marcas
                                            </span>
                                        </li>

                                        {
                                            brands.map(brand =>
                                                <li className="list-item my-2" onClick={handleBrand}
                                                    data-brand={brand._id} key={brand._id}>
                                                    <span className="filter-link cursor-pointer hover-red"
                                                        onClick={handleBrand}
                                                        data-brand={brand._id} key={brand._id}>
                                                        {brand.name}
                                                    </span>
                                                </li>
                                            )
                                        }
                                        {/* <li className="list-item">
                                            <a data-label='Show less<i className="fas fa-angle-up" aria-hidden="true"></i>'
                                                className="btn-control control-show-more" href="#">
                                                Mostrar mas
                                                <i className="fas fa-angle-down" aria-hidden="true">
                                                </i>
                                            </a>
                                        </li> */}
                                    </ul>
                                </div>
                            </div>

                            {/* NO SIRVE TOCA PONER UNO PROPIO!!! */}

                            {/* <div className="widget mercado-widget filter-widget price-filter">
                                <h2 className="widget-title">Precio</h2>
                                <div className="widget-content">
                                    <div id="slider-range"></div>
                                    <p>
                                        <label htmlFor="amount">Precio:</label>
                                        <input type="text" id="amount" readOnly />
                                        <button className="filter-submit">Filtrar</button>
                                    </p>
                                </div>
                            </div> */}

                            {/* <div className="widget mercado-widget filter-widget">
                                <h2 className="widget-title">Color</h2>
                                <div className="widget-content">
                                    <ul className="list-style vertical-list has-count-index">
                                        <li className="list-item">
                                            <a className="filter-link " href="#">Red <span>(217)</span></a></li>
                                        <li className="list-item">
                                            <a className="filter-link " href="#">Yellow <span>(179)</span></a></li>
                                        <li className="list-item">
                                            <a className="filter-link " href="#">Black <span>(79)</span></a></li>
                                        <li className="list-item">
                                            <a className="filter-link " href="#">Blue <span>(283)</span></a></li>
                                        <li className="list-item">
                                            <a className="filter-link " href="#">Grey <span>(116)</span></a></li>
                                        <li className="list-item">
                                            <a className="filter-link " href="#">Pink <span>(29)</span></a></li>
                                    </ul>
                                </div>
                            </div> */}
                            {/* <div className="widget mercado-widget filter-widget">
                                <h2 className="widget-title">Tamaño</h2>
                                <div className="widget-content">
                                    <ul className="list-style inline-round ">
                                        <li className="list-item">
                                            <a className="filter-link active" href="#">s</a></li>
                                        <li className="list-item">
                                            <a className="filter-link " href="#">M</a></li>
                                        <li className="list-item">
                                            <a className="filter-link " href="#">l</a></li>
                                        <li className="list-item">
                                            <a className="filter-link " href="#">xl</a></li>
                                    </ul>
                                    <div className="widget-banner">
                                        <figure><img src="assets/images/size-banner-widget.jpg" width="270" height="331" alt="" /></figure>
                                    </div>
                                </div>
                            </div> */}
                            {/* <div className="widget mercado-widget widget-product">
                                <h2 className="widget-title">Productos populares</h2>
                                <div className="widget-content">
                                    <ul className="products">
                                        <li className="product-item">
                                            <div className="product product-widget-style">
                                                <div className="thumbnnail">
                                                    <a href="detail.html" title="Radiant-360 R6 Wireless Omnidirectional Speaker [White]">
                                                        <figure><img src="assets/images/products/digital_01.jpg" alt="" /></figure>
                                                    </a>
                                                </div>
                                                <div className="product-info">
                                                    <a href="#" className="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker...</span></a>
                                                    <div className="wrap-price"><span className="product-price">$168.00</span></div>
                                                </div>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div> */}

                        </div>
                    </div>

                </div>

            </main>

        </>
    );
}


export async function getServerSideProps({ query }) { // Server side rendering -> no console.log()

    const category = query.category || 'all'
    const brand = query.brand || 'all'
    const sort = query.sort || ''
    const search = query.search || 'all'

    let url = `product?category=${category}&brand=${brand}&sort=${sort}&title=${search}`;

    if (sort == 'orderall')
        url = `product?category=${category}&brand=${brand}&title=${search}`


    const res = await getData(url)

    // server side rendering
    return {
        props: {
            products: res.products,
            result: res.result
        }, // will be passed to the page component as props
    }
}

export default Shop;
