import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useContext } from 'react'
import { DataContext } from '../store/GlobalState'
import { deleteItem } from '../store/Actions'
import { putData } from '../utils/fetchData';
import { useRouter } from 'next/router'

const ConfirmMsg = ({ isOpen, handleClose, title, subtitle = '¿Quieres quitar este articulo del carrito?' }) => {

    const { state, dispatch } = useContext(DataContext)
    const { confirm, auth } = state
    const router = useRouter()

    // console.log(confirm)

    const inactiveOne = (item, exactlyRoute) => {
        dispatch({ type: 'NOTIFY', payload: { loading: true } })
        putData(exactlyRoute, null, auth.token)
            .then(res => {
                if (res.err)
                    return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

                dispatch(deleteItem(item.data, item.id, item.type))
                return dispatch({ type: 'NOTIFY', payload: { success: res.msg } })
            })
    }

    const inactiveProduct = item => {
        dispatch({ type: 'NOTIFY', payload: { loading: true } })
        putData(`product/inactive/${item.id}`, null, auth.token)
            .then(res => {
                if (res.err)
                    return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

                dispatch({ type: 'NOTIFY', payload: { success: res.msg } })

                // GUARDA RUTA EN NEXT/ROUTER
                return router.push('/')
            })
    }

    const handleSubmit = () => {
        handleClose()

        if (confirm.length !== 0) {

            for (const item of confirm) {

                if (item.type === 'ADD_USERS') inactiveOne(item, `user/${item.id}`) // Inactivar Usuario

                else if (item.type === 'ADD_CART') dispatch(deleteItem(item.data, item.id, item.type))

                else if (item.type === 'ADD_CATEGORIES') inactiveOne(item, `category/inactive/${item.id}`) // Inactivar Usuario

                else if (item.type === 'ADD_BRANDS') inactiveOne(item, `brand/inactive/${item.id}`) // Inactivar Usuario

                else if (item.type === 'INACTIVE_PRODUCT') inactiveProduct(item)

                dispatch({ type: 'ADD_CONFIRM', payload: [] })
            }
        }
    };

    return (
        <Dialog
            open={isOpen}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title text-capitalize">{subtitle}</DialogTitle>

            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {/* {confirm[0].title ? confirm[0].title : ''} */}
                    {confirm.length !== 0 && confirm.map(cf => (<span key={cf.id} className="d-block">- {cf.title}</span>))}
                </DialogContentText>
            </DialogContent>

            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Cancelar
                </Button>
                <Button onClick={handleSubmit} color="primary" autoFocus>
                    Aceptar
                </Button>
            </DialogActions>
        </Dialog>
    );
}

export default ConfirmMsg;
