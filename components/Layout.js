import React from 'react';
import NavBar from './NavBar';
import Notify from './Notify'
import Modal from './Modal'
// import ConfirmMsg from './ConfirmMsg'

function Layout({ children }) {
    return (
        <>
            <NavBar />
            <Notify />
            <Modal />
            {/* <ConfirmMsg isOpen={isOpen} handleClose={handleDialogClose}
                title='¿Estas seguro?' /> */}
            {children}
        </>
    );
}

export default Layout;
