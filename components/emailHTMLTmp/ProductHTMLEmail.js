const ProductHTMLEmail = () => {

    // <style type="text/css" media="screen">
    //                     [style*="HG"] {
    //                         font - family: 'Hind Guntur', Arial, sans-serif !important
    // 	}
    // 	[style*="Gudea"] {
    //                         font - family: 'Gudea', Arial, sans-serif !important
    // 	}
    // 	body {padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#ffffff; -webkit-text-size-adjust:none; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px }
    // 	a {color:#ed5258; text-decoration:none }
    // 	p {padding:0 !important; margin:0 !important }
    // 	img {-ms - interpolation - mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
    // 	table {mso - cellspacing: 0px; mso-padding-alt: 0px 0px 0px 0px; }

    // 	@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
    //                         table[className= 'mobile-shell'] {width: 100% !important; min-width: 100% !important; }
    // 		table[className='center'] {margin: 0 auto !important; }
    // 		table[className='left'] {margin - right: auto !important; }

    // 		td[className='td'] {width: 100% !important; min-width: 100% !important; }
    // 		td[className='auto'] {width: 100% !important; min-width: auto !important; }

    // 		div[className='mobile-br-3'] {height: 4px !important; background: #ffffff !important; display: block !important; }
    // 		div[className='mobile-br-4'] {height: 4px !important; background: #2d2d31 !important; display: block !important; }
    // 		div[className='mobile-br-5'] {height: 5px !important; }
    // 		div[className='mobile-br-10'] {height: 10px !important; }
    // 		div[className='mobile-br-15'] {height: 15px !important; }
    // 		div[className='mobile-br-25'] {height: 25px !important; }

    // 		th[className='m-td'],
    // 		td[className='m-td'],
    // 		div[className='hide-for-mobile'],
    // 		span[className='hide-for-mobile'] {display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

    // 		span[className='mobile-block'] {display: block !important; }
    // 		div[className='img-m-center'] {text - align: center !important; }
    // 		div[className='img-m-left'] {text - align: left !important; }

    // 		div[className='fluid-img'] img,
    // 		td[className='fluid-img'] img {width: 100% !important; max-width: 100% !important; height: auto !important; }

    // 		div[className='h1'],
    // 		div[className='slogan'],
    // 		div[className='h1-left'],
    // 		div[className='h1-white'],
    // 		div[className='h1-white-r'],
    // 		div[className='h1-secondary'],
    // 		div[className='text-footer'],
    // 		div[className='text-footer-r'],
    // 		div[className='text-footer-r2'],
    // 		div[className='h2-white-m-center'],
    // 		div[className='h1-white-secondary-r'] {text - align: center !important; }

    // 		th[className='column'],
    // 		th[className='column-top'],
    // 		th[className='column-dir-t'],
    // 		th[className='column-bottom'],
    // 		th[className='column-dir'] {float: left !important; width: 100% !important; display: block !important; }
    // 		div[className='text-top'] {text - align: center !important; }
    // 		th[className='column-top-black'] {float: left !important; width: 100% !important; display: block !important; background: #2d2d31 !important; }

    // 		td[className='item'] {width: 150px !important; }

    // 		td[className='content-spacing'] {width: 15px !important; }
    // 		td[className='content-spacing-pink'] {width: 15px !important; background: #ed5258 !important; }
    // 	}
    // </style>

    return (
        <>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                <tr>
                    <td align="center" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#ed5258">&nbsp;</td>
                                            <td align="center" width="650" className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <th className="column"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="25" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <table className="center" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td className="text-top2"
                                                                                                style="color:#ffffff; font-family:Arial,sans-serif, 'HG'; font-size:11px; line-height:16px; text-align:left; text-transform:uppercase; font-weight:bold">
                                                                                                <a href={`/product/${id}`}
                                                                                                    target="_blank"
                                                                                                    className="link-white-u"
                                                                                                    style="color:#ffffff; text-decoration:underline">
                                                                                                    <span className="link-white-u"
                                                                                                        style="color:#ffffff;
																							text-decoration:underline">
                                                                                                        Ver online
																						</span>
                                                                                                </a>
                                                                                            </td>
                                                                                            <td className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="20"></td>
                                                                                            <td className="text-top2"
                                                                                                style="color:#ffffff; font-family:Arial,sans-serif, 'HG'; font-size:11px; line-height:16px; text-align:left; text-transform:uppercase; font-weight:bold">
                                                                                                {/* <a href="#" target="_blank"
                                                                                                    className="link-white-u"
                                                                                                    style="color:#ffffff; text-decoration:underline">
                                                                                                    <span
                                                                                                        className="link-white-u"
                                                                                                        style="color:#ffffff;
																							text-decoration:underline">
                                                                                                        FORWARD
																						</span>
                                                                                                </a>  */}
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="25" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                    <th className="column"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                                        width="240" bgcolor="#f6f6f6">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="25" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <table className="center" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="40"><a href="#"
                                                                                                    target="_blank"><img
                                                                                                        src="images/ico2_facebook.jpg"
                                                                                                        border="0" width="14"
                                                                                                        height="13" alt="" /></a>
                                                                                            </td>
                                                                                            <td className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="40"><a href="#"
                                                                                                    target="_blank"><img
                                                                                                        src="images/ico2_twitter.jpg"
                                                                                                        border="0" width="14"
                                                                                                        height="13" alt="" /></a>
                                                                                            </td>
                                                                                            <td className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="40"><a href="#"
                                                                                                    target="_blank"><img
                                                                                                        src="images/ico2_gplus.jpg"
                                                                                                        border="0" width="14"
                                                                                                        height="13" alt="" /></a>
                                                                                            </td>
                                                                                            <td className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="40"><a href="#"
                                                                                                    target="_blank"><img
                                                                                                        src="images/ico2_pinterest.jpg"
                                                                                                        border="0" width="14"
                                                                                                        height="13" alt="" /></a>
                                                                                            </td>
                                                                                            <td className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="14"><a href="#"
                                                                                                    target="_blank"><img
                                                                                                        src="images/ico2_instagram.jpg"
                                                                                                        border="0" width="14"
                                                                                                        height="13" alt="" /></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="25" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#f6f6f6">&nbsp;
								</td>
                                        </tr>
                                    </table>

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#2d2d31">
                                        <tr>
                                            <td align="center">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" className="spacer"
                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                    <tr>
                                                        <td height="30" className="spacer"
                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>

                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                                bgcolor="#2d2d31">
                                                                <tr>
                                                                    <th className="column"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                    <div className="img-m-center"
                                                                                        style="font-size:0pt; line-height:0pt"><a
                                                                                            href="#" target="_blank"><img
                                                                                                src="images/logo_free.jpg"
                                                                                                border="0" width="166" height="43"
                                                                                                alt="" /></a></div>
                                                                                    <div style="font-size:0pt; line-height:0pt;"
                                                                                        className="mobile-br-15"></div>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                    <th className="column"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                                        width="240">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <table className="center" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="22"><img
                                                                                                    src="images/ico_link.jpg"
                                                                                                    border="0" width="13"
                                                                                                    height="13" alt="" /></td>
                                                                                            <td className="text-top2"
                                                                                                style="color:#ffffff; font-family:Arial,sans-serif, 'HG'; font-size:11px; line-height:16px; text-align:left; text-transform:uppercase; font-weight:bold">
                                                                                                <a href="https://www.solumobil.com/?search=all"
                                                                                                    target="_blank"
                                                                                                    className="link-white-u"
                                                                                                    style="color:#ffffff; text-decoration:underline"><span
                                                                                                        className="link-white-u" style="color:#ffffff;
																							text-decoration:underline">
                                                                                                        VE A LA TIENDA
																						</span>
                                                                                                </a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" className="spacer"
                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                    <tr>
                                                        <td height="30" className="spacer"
                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#ed5258">&nbsp;</td>
                                <td align="center" width="650" className="td"
                                    style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                        bgcolor="#ed5258">
                                        <tr>
                                            <td className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th className="column"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    {/* <td className="fluid-img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left">
                                                                        <img src={images[0].url} border="0" width="409"
                                                                            height="364" alt="" />
                                                                    </td> */}
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th className="column"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                            width="240" bgcolor="#f6f6f6">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td className="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                    <td align="right">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="25" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <div className="h1"
                                                                            style="color:#2d2d31; font-family:impact, 'AvenirNextCondensed-Bold', Arial,sans-serif; font-size:60px; line-height:64px; text-align:right; text-transform:uppercase">
                                                                            {/* ESPACIAR CADA PALABRA */}
                                                                            {title}
                                                                        </div>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="25" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                    <td className="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                    bgcolor="#f6f6f6">&nbsp;</td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#2d2d31">
                            <tr>
                                <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="1">
                                    &nbsp;</td>
                                <td align="center">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell">
                                        <tr>
                                            <td className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" className="spacer"
                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                    <tr>
                                                        <td height="42" className="spacer"
                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>

                                                <div className="h2-white-center"
                                                    style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:26px; line-height:30px; text-align:center; font-weight:bold">
                                                    Huge Sale Coming on June 24th</div>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" className="spacer"
                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                    <tr>
                                                        <td height="22" className="spacer"
                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>

                                                <div className="text-light-grey"
                                                    style="color:#b6b6b6; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
										fugiat nulla pariatur.</div>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" className="spacer"
                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                    <tr>
                                                        <td height="25" className="spacer"
                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>

                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="center">
                                                            <table className="center" border="0" cellspacing="0" cellpadding="0"
                                                                bgcolor="#ed5258">
                                                                <tr>
                                                                    <td className="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        height="34" width="12"></td>
                                                                    <td>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="6" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <div className="button1"
                                                                            style="color:#ffffff; font-family:Arial,sans-serif; font-size:13px; line-height:20px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                            <a href="#" target="_blank" className="link-white"
                                                                                style="color:#ffffff; text-decoration:none"><span
                                                                                    className="link-white"
                                                                                    style="color:#ffffff; text-decoration:none">FIND
																		OUT MORE</span></a>
                                                                        </div>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="6" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                    <td className="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="12"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" className="spacer"
                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                    <tr>
                                                        <td height="42" className="spacer"
                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="1">
                                    &nbsp;</td>
                            </tr>
                        </table>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#ed5258">&nbsp;</td>
                                            <td align="center" width="650" className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td className="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                    <td>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <th className="column"
                                                                                    style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0"
                                                                                                    className="spacer"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                    <tr>
                                                                                                        <td height="45"
                                                                                                            className="spacer"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                            &nbsp;</td>
                                                                                                    </tr>
                                                                                                </table>

                                                                                                <div className="h2-white"
                                                                                                    style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:26px; line-height:30px; text-align:left; font-weight:bold">
                                                                                                    Upcoming Events</div>
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0"
                                                                                                    className="spacer"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                    <tr>
                                                                                                        <td height="35"
                                                                                                            className="spacer"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                            &nbsp;</td>
                                                                                                    </tr>
                                                                                                </table>

                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                                <th className="m-td"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    bgcolor="#f6f6f6" width="240">
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td className="content-spacing"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="1"></td>
                                                                                            <td>
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0"
                                                                                                    className="spacer"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                    <tr>
                                                                                                        <td height="25"
                                                                                                            className="spacer"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                            &nbsp;</td>
                                                                                                    </tr>
                                                                                                </table>

                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0"
                                                                                                    className="spacer"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                    <tr>
                                                                                                        <td height="25"
                                                                                                            className="spacer"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                            &nbsp;</td>
                                                                                                    </tr>
                                                                                                </table>

                                                                                            </td>
                                                                                            <td className="content-spacing"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="30"></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td className="content-spacing-pink"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        bgcolor="#f6f6f6" width="1"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#f6f6f6">&nbsp;
								</td>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#ed5258">&nbsp;</td>
                                            <td align="center" width="650" className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="77" bgcolor="#f6f6f6">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="10"></td>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="10" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div className="day-date"
                                                                                        style="color:#2d2d31; font-family:Arial,sans-serif, 'HG'; font-size:30px; line-height:34px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                        20</div>
                                                                                    <div className="month-date"
                                                                                        style="color:#2d2d31; font-family:Arial,sans-serif, 'HG'; font-size:14px; line-height:18px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                        aug</div>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="10" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="10"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td className="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="20" bgcolor="#ed5258">&nbsp;</td>
                                                                    <td bgcolor="#f6f6f6">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="20"></td>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <th className="column"
                                                                                                style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <div className="text"
                                                                                                                style="color:#2d2d31; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left; font-weight:normal">
                                                                                                                Lorem ipsum dolor
                                                                                                                sit amet,
                                                                                                                consectetur
                                                                                                                adipiscing elit. Ut
                                                                                                                tincidunt venenatis
																									lacinia.</div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </th>
                                                                                            <th className="column"
                                                                                                style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                                                                width="170">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            {/*
																								<!-- Button --> */}
                                                                                                            <div style="font-size:0pt; line-height:0pt;"
                                                                                                                className="mobile-br-15">
                                                                                                            </div>

                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0">
                                                                                                                <tr>
                                                                                                                    <td
                                                                                                                        align="right">
                                                                                                                        <table
                                                                                                                            className="left"
                                                                                                                            border="0"
                                                                                                                            cellspacing="0"
                                                                                                                            cellpadding="0"
                                                                                                                            bgcolor="#2d2d31">
                                                                                                                            <tr>
                                                                                                                                <td className="img"
                                                                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                                                    height="34"
                                                                                                                                    width="12">
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <table
                                                                                                                                        width="100%"
                                                                                                                                        border="0"
                                                                                                                                        cellspacing="0"
                                                                                                                                        cellpadding="0"
                                                                                                                                        className="spacer"
                                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                        <tr>
                                                                                                                                            <td height="6"
                                                                                                                                                className="spacer"
                                                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                                &nbsp;
																																</td>
                                                                                                                                        </tr>
                                                                                                                                    </table>

                                                                                                                                    <div className="button1"
                                                                                                                                        style="color:#ffffff; font-family:Arial,sans-serif; font-size:13px; line-height:20px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                                                                        <a href="#"
                                                                                                                                            target="_blank"
                                                                                                                                            className="link-white"
                                                                                                                                            style="color:#ffffff; text-decoration:none"><span
                                                                                                                                                className="link-white"
                                                                                                                                                style="color:#ffffff; text-decoration:none">Learn
																																	more</span></a>
                                                                                                                                    </div>
                                                                                                                                    <table
                                                                                                                                        width="100%"
                                                                                                                                        border="0"
                                                                                                                                        cellspacing="0"
                                                                                                                                        cellpadding="0"
                                                                                                                                        className="spacer"
                                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                        <tr>
                                                                                                                                            <td height="6"
                                                                                                                                                className="spacer"
                                                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                                &nbsp;
																																</td>
                                                                                                                                        </tr>
                                                                                                                                    </table>

                                                                                                                                </td>
                                                                                                                                <td className="img"
                                                                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                                                    width="12">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                            {/*
																								<!-- END Button --> */}
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </th>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="1"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#f6f6f6">&nbsp;
								</td>
                                        </tr>
                                    </table>
                                    {/*
						<!-- END EVENT Row --> */}
                                    {/*
						<!-- Separator --> */}
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#ed5258">&nbsp;</td>
                                            <td align="center" width="650" className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <th className="column"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                    <th className="m-td"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        bgcolor="#f6f6f6" width="240">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="1"></td>
                                                                                <td>
                                                                                    <div className="hide-for-mobile">
                                                                                        <table width="100%" border="0"
                                                                                            cellspacing="0" cellpadding="0"
                                                                                            className="spacer"
                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                            <tr>
                                                                                                <td height="20" className="spacer"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                    &nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="30"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#f6f6f6">&nbsp;
								</td>
                                        </tr>
                                    </table>
                                    {/*
						<!-- END Separator --> */}

                                    {/*
						<!-- EVENT Row  --> */}
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#ed5258">&nbsp;</td>
                                            <td align="center" width="650" className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="77" bgcolor="#f6f6f6">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="10"></td>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="10" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div className="day-date"
                                                                                        style="color:#2d2d31; font-family:Arial,sans-serif, 'HG'; font-size:30px; line-height:34px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                        5</div>
                                                                                    <div className="month-date"
                                                                                        style="color:#2d2d31; font-family:Arial,sans-serif, 'HG'; font-size:14px; line-height:18px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                        sept</div>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="10" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="10"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td className="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="20" bgcolor="#ed5258">&nbsp;</td>
                                                                    <td bgcolor="#f6f6f6">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="20"></td>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <th className="column"
                                                                                                style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <div className="text"
                                                                                                                style="color:#2d2d31; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left; font-weight:normal">
                                                                                                                Lorem ipsum dolor
                                                                                                                sit amet,
                                                                                                                consectetur
                                                                                                                adipiscing elit. Ut
                                                                                                                tincidunt venenatis
																									lacinia.</div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </th>
                                                                                            <th className="column"
                                                                                                style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                                                                width="170">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            {/*
																								<!-- Button --> */}
                                                                                                            <div style="font-size:0pt; line-height:0pt;"
                                                                                                                className="mobile-br-15">
                                                                                                            </div>

                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0">
                                                                                                                <tr>
                                                                                                                    <td
                                                                                                                        align="right">
                                                                                                                        <table
                                                                                                                            className="left"
                                                                                                                            border="0"
                                                                                                                            cellspacing="0"
                                                                                                                            cellpadding="0"
                                                                                                                            bgcolor="#2d2d31">
                                                                                                                            <tr>
                                                                                                                                <td className="img"
                                                                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                                                    height="34"
                                                                                                                                    width="12">
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <table
                                                                                                                                        width="100%"
                                                                                                                                        border="0"
                                                                                                                                        cellspacing="0"
                                                                                                                                        cellpadding="0"
                                                                                                                                        className="spacer"
                                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                        <tr>
                                                                                                                                            <td height="6"
                                                                                                                                                className="spacer"
                                                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                                &nbsp;
																																</td>
                                                                                                                                        </tr>
                                                                                                                                    </table>

                                                                                                                                    <div className="button1"
                                                                                                                                        style="color:#ffffff; font-family:Arial,sans-serif; font-size:13px; line-height:20px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                                                                        <a href="#"
                                                                                                                                            target="_blank"
                                                                                                                                            className="link-white"
                                                                                                                                            style="color:#ffffff; text-decoration:none"><span
                                                                                                                                                className="link-white"
                                                                                                                                                style="color:#ffffff; text-decoration:none">Learn
																																	more</span></a>
                                                                                                                                    </div>
                                                                                                                                    <table
                                                                                                                                        width="100%"
                                                                                                                                        border="0"
                                                                                                                                        cellspacing="0"
                                                                                                                                        cellpadding="0"
                                                                                                                                        className="spacer"
                                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                        <tr>
                                                                                                                                            <td height="6"
                                                                                                                                                className="spacer"
                                                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                                &nbsp;
																																</td>
                                                                                                                                        </tr>
                                                                                                                                    </table>

                                                                                                                                </td>
                                                                                                                                <td className="img"
                                                                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                                                    width="12">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                            {/*
																								<!-- END Button --> */}
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </th>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="1"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#f6f6f6">&nbsp;
								</td>
                                        </tr>
                                    </table>
                                    {/*
						<!-- END EVENT Row --> */}
                                    {/*
						<!-- Separator --> */}
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#ed5258">&nbsp;</td>
                                            <td align="center" width="650" className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <th className="column"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                    <th className="m-td"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        bgcolor="#f6f6f6" width="240">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="1"></td>
                                                                                <td>
                                                                                    <div className="hide-for-mobile">
                                                                                        <table width="100%" border="0"
                                                                                            cellspacing="0" cellpadding="0"
                                                                                            className="spacer"
                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                            <tr>
                                                                                                <td height="20" className="spacer"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                    &nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="30"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#f6f6f6">&nbsp;
								</td>
                                        </tr>
                                    </table>
                                    {/*
						<!-- END Separator --> */}

                                    {/*
						<!-- EVENT Row  --> */}
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#ed5258">&nbsp;</td>
                                            <td align="center" width="650" className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="77" bgcolor="#f6f6f6">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="10"></td>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="10" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div className="day-date"
                                                                                        style="color:#2d2d31; font-family:Arial,sans-serif, 'HG'; font-size:30px; line-height:34px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                        22</div>
                                                                                    <div className="month-date"
                                                                                        style="color:#2d2d31; font-family:Arial,sans-serif, 'HG'; font-size:14px; line-height:18px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                        sept</div>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="10" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="10"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td className="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="20" bgcolor="#ed5258">&nbsp;</td>
                                                                    <td bgcolor="#f6f6f6">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="20"></td>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <th className="column"
                                                                                                style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <div className="text"
                                                                                                                style="color:#2d2d31; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left; font-weight:normal">
                                                                                                                Lorem ipsum dolor
                                                                                                                sit amet,
                                                                                                                consectetur
                                                                                                                adipiscing elit. Ut
                                                                                                                tincidunt venenatis
																									lacinia.</div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </th>
                                                                                            <th className="column"
                                                                                                style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                                                                width="170">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            {/*
																								<!-- Button --> */}
                                                                                                            <div style="font-size:0pt; line-height:0pt;"
                                                                                                                className="mobile-br-15">
                                                                                                            </div>

                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0">
                                                                                                                <tr>
                                                                                                                    <td
                                                                                                                        align="right">
                                                                                                                        <table
                                                                                                                            className="left"
                                                                                                                            border="0"
                                                                                                                            cellspacing="0"
                                                                                                                            cellpadding="0"
                                                                                                                            bgcolor="#2d2d31">
                                                                                                                            <tr>
                                                                                                                                <td className="img"
                                                                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                                                    height="34"
                                                                                                                                    width="12">
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <table
                                                                                                                                        width="100%"
                                                                                                                                        border="0"
                                                                                                                                        cellspacing="0"
                                                                                                                                        cellpadding="0"
                                                                                                                                        className="spacer"
                                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                        <tr>
                                                                                                                                            <td height="6"
                                                                                                                                                className="spacer"
                                                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                                &nbsp;
																																</td>
                                                                                                                                        </tr>
                                                                                                                                    </table>

                                                                                                                                    <div className="button1"
                                                                                                                                        style="color:#ffffff; font-family:Arial,sans-serif; font-size:13px; line-height:20px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                                                                        <a href="#"
                                                                                                                                            target="_blank"
                                                                                                                                            className="link-white"
                                                                                                                                            style="color:#ffffff; text-decoration:none"><span
                                                                                                                                                className="link-white"
                                                                                                                                                style="color:#ffffff; text-decoration:none">Learn
																																	more</span></a>
                                                                                                                                    </div>
                                                                                                                                    <table
                                                                                                                                        width="100%"
                                                                                                                                        border="0"
                                                                                                                                        cellspacing="0"
                                                                                                                                        cellpadding="0"
                                                                                                                                        className="spacer"
                                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                        <tr>
                                                                                                                                            <td height="6"
                                                                                                                                                className="spacer"
                                                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                                                &nbsp;
																																</td>
                                                                                                                                        </tr>
                                                                                                                                    </table>

                                                                                                                                </td>
                                                                                                                                <td className="img"
                                                                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                                                    width="12">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                            {/*
																								<!-- END Button --> */}
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </th>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="1"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#f6f6f6">&nbsp;
								</td>
                                        </tr>
                                    </table>
                                    {/*
						<!-- END EVENT Row --> */}
                                    {/*
						<!-- Separator --> */}
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#ed5258">&nbsp;</td>
                                            <td align="center" width="650" className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <th className="column"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                    <th className="m-td"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        bgcolor="#f6f6f6" width="240">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="1"></td>
                                                                                <td>
                                                                                    <div className="hide-for-mobile">
                                                                                        <table width="100%" border="0"
                                                                                            cellspacing="0" cellpadding="0"
                                                                                            className="spacer"
                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                            <tr>
                                                                                                <td height="20" className="spacer"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                    &nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="30"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#f6f6f6">&nbsp;
								</td>
                                        </tr>
                                    </table>
                                    {/*
						<!-- END Separator --> */}

                                    {/*
						<!-- Separator --> */}
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#ed5258">&nbsp;</td>
                                            <td align="center" width="650" className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <th className="column"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="40" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                    <th className="m-td"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        bgcolor="#f6f6f6" width="240">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="1"></td>
                                                                                <td>
                                                                                    <div className="hide-for-mobile">
                                                                                        <table width="100%" border="0"
                                                                                            cellspacing="0" cellpadding="0"
                                                                                            className="spacer"
                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                            <tr>
                                                                                                <td height="40" className="spacer"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                    &nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="30"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#f6f6f6">&nbsp;
								</td>
                                        </tr>
                                    </table>
                                    {/*
						<!-- END Separator --> */}
                                    {/*
						<!-- END Events --> */}
                                </td>
                            </tr>
                        </table>
                        {/*
			<!-- END Section 3 --> */}

                        {/*
			<!-- Section 4 --> */}
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#2d2d31">
                            <tr>
                                <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="1">
                                </td>
                                <td align="center">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell">
                                        <tr>
                                            <td className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" className="spacer"
                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                    <tr>
                                                        <td height="40" className="spacer"
                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>

                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th className="column-top"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <div className="h2-white"
                                                                            style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:26px; line-height:30px; text-align:left; font-weight:bold">
                                                                            Tablets</div>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="22" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        {/*
															<!-- Row --> */}
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="text-white"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left"
                                                                                    width="20">
                                                                                    &bull;
																	</td>
                                                                                <td className="text-white"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                    <div className="text"
                                                                                        style="color:#2d2d31; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left; font-weight:normal">
                                                                                        <a href="#" target="_blank"
                                                                                            className="link-white-u"
                                                                                            style="color:#ffffff; text-decoration:underline"><span
                                                                                                className="link-white-u"
                                                                                                style="color:#ffffff; text-decoration:underline">Lorem
																					ipsum dolor sit amet</span></a>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="6" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        {/*
															<!-- END Row --> */}
                                                                        {/*
															<!-- Row --> */}
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="text-white"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left"
                                                                                    width="20">
                                                                                    &bull;
																	</td>
                                                                                <td className="text-white"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                    <div className="text"
                                                                                        style="color:#2d2d31; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left; font-weight:normal">
                                                                                        <a href="#" target="_blank"
                                                                                            className="link-white-u"
                                                                                            style="color:#ffffff; text-decoration:underline"><span
                                                                                                className="link-white-u"
                                                                                                style="color:#ffffff; text-decoration:underline">Consectetur
																					adipisicing elit</span></a>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="6" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        {/*
															<!-- END Row --> */}
                                                                        {/*
															<!-- Row --> */}
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="text-white"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left"
                                                                                    width="20">
                                                                                    &bull;
																	</td>
                                                                                <td className="text-white"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                    <div className="text"
                                                                                        style="color:#2d2d31; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left; font-weight:normal">
                                                                                        <a href="#" target="_blank"
                                                                                            className="link-white-u"
                                                                                            style="color:#ffffff; text-decoration:underline"><span
                                                                                                className="link-white-u"
                                                                                                style="color:#ffffff; text-decoration:underline">Sed
																					do eiusmod</span></a>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="6" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        {/*
															<!-- END Row --> */}
                                                                        {/*
															<!-- Row --> */}
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="text-white"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left"
                                                                                    width="20">
                                                                                    &bull;
																	</td>
                                                                                <td className="text-white"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                    <div className="text"
                                                                                        style="color:#2d2d31; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left; font-weight:normal">
                                                                                        <a href="#" target="_blank"
                                                                                            className="link-white-u"
                                                                                            style="color:#ffffff; text-decoration:underline"><span
                                                                                                className="link-white-u"
                                                                                                style="color:#ffffff; text-decoration:underline">Tempor
																					incididunt ut labore</span></a>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="6" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        {/*
															<!-- END Row --> */}
                                                                        {/*
															<!-- Row --> */}
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="text-white"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left"
                                                                                    width="20">
                                                                                    &bull;
																	</td>
                                                                                <td className="text-white"
                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                    <div className="text"
                                                                                        style="color:#2d2d31; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left; font-weight:normal">
                                                                                        <a href="#" target="_blank"
                                                                                            className="link-white-u"
                                                                                            style="color:#ffffff; text-decoration:underline"><span
                                                                                                className="link-white-u"
                                                                                                style="color:#ffffff; text-decoration:underline">Et
																					dolore magna aliqua</span></a>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        {/*
															<!-- END Row --> */}
                                                                    </td>
                                                                    <td className="m-td"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="28"></td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th className="column-top"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0"
                                                            width="422">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td className="fluid-img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left">
                                                                        <div style="font-size:0pt; line-height:0pt;"
                                                                            className="mobile-br-15"></div>
                                                                        <img src="images/image5.jpg" border="0" width="422"
                                                                            height="224" alt="" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" className="spacer"
                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                    <tr>
                                                        <td height="40" className="spacer"
                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="1">
                                </td>
                            </tr>
                        </table>
                        {/*
			<!-- END Section 4 --> */}

                        {/*
			<!-- Section 5 --> */}
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    {/*
						<!-- Separator --> */}
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#ed5258">&nbsp;</td>
                                            <td align="center" width="650" className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="70" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td className="m-td"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        bgcolor="#2d2d31" width="240">
                                                                        <div className="hide-for-mobile">
                                                                            <table width="100%" border="0" cellspacing="0"
                                                                                cellpadding="0" className="spacer"
                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                <tr>
                                                                                    <td height="70" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#2d2d31">&nbsp;
								</td>
                                        </tr>
                                    </table>
                                    {/*
						<!-- END Separator --> */}
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#ed5258">&nbsp;</td>
                                            <td align="center" width="650" className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" dir="rtl"
                                                                style="direction: rtl">
                                                                <tr>
                                                                    <th className="column-dir"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr; Margin:0"
                                                                        dir="ltr" bgcolor="#f6f6f6">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                    <div className="fluid-img"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                        <img src="images/image25.jpg" border="0"
                                                                                            width="410" height="370" alt="" />
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                    <th className="column-dir"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr; Margin:0"
                                                                        dir="ltr" bgcolor="#ed5258" width="240">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="1"></td>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div className="h1-white"
                                                                                        style="color:#ffffff; font-family:impact, 'AvenirNextCondensed-Bold', Arial,sans-serif; font-size:60px; line-height:64px; text-align:left; text-transform:uppercase">
                                                                                        New</div>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="30" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div className="h2-white"
                                                                                        style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:26px; line-height:30px; text-align:left; font-weight:bold">
                                                                                        Music player</div>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="12" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div className="text-white"
                                                                                        style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                        Ut pulvinar lorem eu nisi faucibus, a
                                                                                        accumsan nibh tempus. Vivamus sagittis, elit
                                                                                        eget rhoncus maximus, justo ante efficitur
                                                                                        purus, eu bibendum turpis dui eget felis.
                                                                                        Aliquam sit amet eleifend tellus, vel
                                                                                        commodo felis. Aliquam accumsan id felis ac
																			iaculis. </div>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="30"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#f6f6f6">&nbsp;
								</td>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td className="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#ed5258">&nbsp;</td>
                                            <td align="center" width="650" className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell"
                                                    bgcolor="#ed5258">
                                                    <tr>
                                                        <td className="td"
                                                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <th className="column"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="1"></td>
                                                                                <td>
                                                                                    {/*
																		<!-- Table --> */}
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td width="188" bgcolor="#f75c62">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                        <td>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                            <div className="h3-white-center"
                                                                                                                style="color:#f6f6f6; font-family:Arial,sans-serif, 'Gudea'; font-size:20px; line-height:25px; text-align:center">
                                                                                                                <strong>16GB</strong>
                                                                                                            </div>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                        </td>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="4"></td>
                                                                                            <td width="188" bgcolor="#f6646a">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                        <td>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                            <div className="h3-white-center"
                                                                                                                style="color:#f6f6f6; font-family:Arial,sans-serif, 'Gudea'; font-size:20px; line-height:25px; text-align:center">
                                                                                                                $199</div>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                        </td>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="4" className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                                &nbsp;</td>
                                                                                            <td height="4" className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                                &nbsp;</td>
                                                                                            <td height="4" className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="188" bgcolor="#f75c62">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                        <td>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                            <div className="h3-white-center"
                                                                                                                style="color:#f6f6f6; font-family:Arial,sans-serif, 'Gudea'; font-size:20px; line-height:25px; text-align:center">
                                                                                                                <strong>32GB</strong>
                                                                                                            </div>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                        </td>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="4"></td>
                                                                                            <td width="188" bgcolor="#f6646a">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                        <td>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                            <div className="h3-white-center"
                                                                                                                style="color:#f6f6f6; font-family:Arial,sans-serif, 'Gudea'; font-size:20px; line-height:25px; text-align:center">
                                                                                                                $249</div>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                        </td>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="4" className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                                &nbsp;</td>
                                                                                            <td height="4" className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                                &nbsp;</td>
                                                                                            <td height="4" className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="188" bgcolor="#f75c62">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                        <td>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                            <div className="h3-white-center"
                                                                                                                style="color:#f6f6f6; font-family:Arial,sans-serif, 'Gudea'; font-size:20px; line-height:25px; text-align:center">
                                                                                                                <strong>64GB</strong>
                                                                                                            </div>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                        </td>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="4"></td>
                                                                                            <td width="188" bgcolor="#f6646a">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                        <td>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                            <div className="h3-white-center"
                                                                                                                style="color:#f6f6f6; font-family:Arial,sans-serif, 'Gudea'; font-size:20px; line-height:25px; text-align:center">
                                                                                                                $299</div>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                        </td>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="4" className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                                &nbsp;</td>
                                                                                            <td height="4" className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                                &nbsp;</td>
                                                                                            <td height="4" className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="188" bgcolor="#f75c62">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                        <td>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                            <div className="h3-white-center"
                                                                                                                style="color:#f6f6f6; font-family:Arial,sans-serif, 'Gudea'; font-size:20px; line-height:25px; text-align:center">
                                                                                                                <strong>128GB</strong>
                                                                                                            </div>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                        </td>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td className="img"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                width="4"></td>
                                                                                            <td width="188" bgcolor="#f6646a">
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                        <td>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                            <div className="h3-white-center"
                                                                                                                style="color:#f6f6f6; font-family:Arial,sans-serif, 'Gudea'; font-size:20px; line-height:25px; text-align:center">
                                                                                                                $399</div>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="14"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                        </td>
                                                                                                        <td className="img"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="15"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="4" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="40" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    {/*
																		<!-- END Table --> */}
                                                                                </td>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="30"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                    <th className="column"
                                                                        style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0"
                                                                        bgcolor="#2d2d31" width="240">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="30"></td>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="50" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    {/*
																		<!-- Row --> */}
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td className="h5-white"
                                                                                                style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:16px; line-height:24px; text-align:left; font-weight:bold"
                                                                                                width="16" valign="top">&bull;</td>
                                                                                            <td valign="top">
                                                                                                <div className="h5-white"
                                                                                                    style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:16px; line-height:24px; text-align:left; font-weight:bold">
                                                                                                    <strong>Height:</strong>
                                                                                                </div>
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0"
                                                                                                    className="spacer"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                    <tr>
                                                                                                        <td height="2"
                                                                                                            className="spacer"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                            &nbsp;</td>
                                                                                                    </tr>
                                                                                                </table>

                                                                                                <div className="text-white"
                                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                                    4.86 inches (123.4 mm)</div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    {/*
																		<!-- END Row --> */}
                                                                                    {/*
																		<!-- Row --> */}
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td className="h5-white"
                                                                                                style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:16px; line-height:24px; text-align:left; font-weight:bold"
                                                                                                width="16" valign="top">&bull;</td>
                                                                                            <td valign="top">
                                                                                                <div className="h5-white"
                                                                                                    style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:16px; line-height:24px; text-align:left; font-weight:bold">
                                                                                                    <strong>Width:</strong>
                                                                                                </div>
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0"
                                                                                                    className="spacer"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                    <tr>
                                                                                                        <td height="2"
                                                                                                            className="spacer"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                            &nbsp;</td>
                                                                                                    </tr>
                                                                                                </table>

                                                                                                <div className="text-white"
                                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                                    2.31 inches (58.6 mm)</div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    {/*
																		<!-- END Row --> */}
                                                                                    {/*
																		<!-- Row --> */}
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td className="h5-white"
                                                                                                style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:16px; line-height:24px; text-align:left; font-weight:bold"
                                                                                                width="16" valign="top">&bull;</td>
                                                                                            <td valign="top">
                                                                                                <div className="h5-white"
                                                                                                    style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:16px; line-height:24px; text-align:left; font-weight:bold">
                                                                                                    <strong>Depth:</strong>
                                                                                                </div>
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0"
                                                                                                    className="spacer"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                    <tr>
                                                                                                        <td height="2"
                                                                                                            className="spacer"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                            &nbsp;</td>
                                                                                                    </tr>
                                                                                                </table>

                                                                                                <div className="text-white"
                                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                                    0.24 inch (6.1 mm)</div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    {/*
																		<!-- END Row --> */}
                                                                                    {/*
																		<!-- Row --> */}
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td className="h5-white"
                                                                                                style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:16px; line-height:24px; text-align:left; font-weight:bold"
                                                                                                width="16" valign="top">&bull;</td>
                                                                                            <td valign="top">
                                                                                                <div className="h5-white"
                                                                                                    style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:16px; line-height:24px; text-align:left; font-weight:bold">
                                                                                                    <strong>Weight:</strong>
                                                                                                </div>
                                                                                                <table width="100%" border="0"
                                                                                                    cellspacing="0" cellpadding="0"
                                                                                                    className="spacer"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                    <tr>
                                                                                                        <td height="2"
                                                                                                            className="spacer"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                            &nbsp;</td>
                                                                                                    </tr>
                                                                                                </table>

                                                                                                <div className="text-white"
                                                                                                    style="color:#ffffff; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                                    3.10 ounces (88 grams)</div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    {/*
																		<!-- END Row --> */}
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="50" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                                <td className="content-spacing"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="30"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td className="content-spacing-pink" style="font-size:0pt; line-height:0pt; text-align:left"
                                                bgcolor="#2d2d31">&nbsp;
								</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        {/*
			<!-- END Section 5 --> */}

                        {/*
			<!-- Section 6 --> */}
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f6f6f6">
                            <tr>
                                <td align="center">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell">
                                        <tr>
                                            <td className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" className="spacer"
                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                    <tr>
                                                        <td height="40" className="spacer"
                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>

                                                <div className="h2-center"
                                                    style="color:#403f45; font-family:Arial,sans-serif, 'HG'; font-size:26px; line-height:30px; text-align:center; font-weight:bold">
                                                    Latest form The Blog</div>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" className="spacer"
                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                    <tr>
                                                        <td height="28" className="spacer"
                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>

                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th className="column-top"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0"
                                                            width="200">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <div className="img-center"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center">
                                                                            <img src="images/image8.jpg" border="0" width="200"
                                                                                height="150" alt="" />
                                                                        </div>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" bgcolor="#2d2d31">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="20"></td>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div className="h5-white"
                                                                                        style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:16px; line-height:24px; text-align:left; font-weight:bold">
                                                                                        Pellentesque lacinia placerat diam a blandit
																		</div>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="15" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div className="text-light-grey2"
                                                                                        style="color:#f6f6f6; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                        Donec tristique lorem cursus consectetur
                                                                                        porta. Praesent posuere orci at odio aliquet
																			feugiat</div>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    {/*
																		<!-- Button --> */}
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td align="left">
                                                                                                <table className="center" border="0"
                                                                                                    cellspacing="0" cellpadding="0"
                                                                                                    bgcolor="#ed5258">
                                                                                                    <tr>
                                                                                                        <td className="content-spacing"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            height="34" width="22">
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="5"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                            <div className="button2"
                                                                                                                style="color:#ffffff; font-family:Arial,sans-serif; font-size:11px; line-height:18px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                                                <a href="#"
                                                                                                                    target="_blank"
                                                                                                                    className="link-white"
                                                                                                                    style="color:#ffffff; text-decoration:none"><span
                                                                                                                        className="link-white"
                                                                                                                        style="color:#ffffff; text-decoration:none">Read
																											more</span></a>
                                                                                                            </div>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="3"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                        </td>
                                                                                                        <td className="content-spacing"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="22"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    {/*
																		<!-- END Button --> */}
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="20"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th className="column-top"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0"
                                                            width="25">
                                                            <div style="font-size:0pt; line-height:0pt;" className="mobile-br-25">
                                                            </div>
                                                        </th>
                                                        <th className="column-top"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0"
                                                            width="200">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <div className="img-center"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center">
                                                                            <img src="images/image9.jpg" border="0" width="200"
                                                                                height="150" alt="" />
                                                                        </div>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" bgcolor="#2d2d31">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="20"></td>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div className="h5-white"
                                                                                        style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:16px; line-height:24px; text-align:left; font-weight:bold">
                                                                                        Pellentesque lacinia placerat diam a blandit
																		</div>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="15" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div className="text-light-grey2"
                                                                                        style="color:#f6f6f6; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                        Donec tristique lorem cursus consectetur
                                                                                        porta. Praesent posuere orci at odio aliquet
																			feugiat</div>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    {/*
																		<!-- Button --> */}
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td align="left">
                                                                                                <table className="center" border="0"
                                                                                                    cellspacing="0" cellpadding="0"
                                                                                                    bgcolor="#ed5258">
                                                                                                    <tr>
                                                                                                        <td className="content-spacing"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            height="34" width="22">
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="5"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                            <div className="button2"
                                                                                                                style="color:#ffffff; font-family:Arial,sans-serif; font-size:11px; line-height:18px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                                                <a href="#"
                                                                                                                    target="_blank"
                                                                                                                    className="link-white"
                                                                                                                    style="color:#ffffff; text-decoration:none"><span
                                                                                                                        className="link-white"
                                                                                                                        style="color:#ffffff; text-decoration:none">Read
																											more</span></a>
                                                                                                            </div>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="3"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                        </td>
                                                                                                        <td className="content-spacing"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="22"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    {/*
																		<!-- END Button --> */}
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="20"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th className="column-top"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0"
                                                            width="25">
                                                            <div style="font-size:0pt; line-height:0pt;" className="mobile-br-25">
                                                            </div>
                                                        </th>
                                                        <th className="column-top"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <div className="img-center"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center">
                                                                            <img src="images/image10.jpg" border="0" width="200"
                                                                                height="150" alt="" />
                                                                        </div>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" bgcolor="#2d2d31">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="20"></td>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div className="h5-white"
                                                                                        style="color:#f6f6f6; font-family:Arial,sans-serif, 'HG'; font-size:16px; line-height:24px; text-align:left; font-weight:bold">
                                                                                        Pellentesque lacinia placerat diam a blandit
																		</div>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="15" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div className="text-light-grey2"
                                                                                        style="color:#f6f6f6; font-family:Arial,sans-serif, 'Gudea'; font-size:14px; line-height:21px; text-align:left">
                                                                                        Donec tristique lorem cursus consectetur
                                                                                        porta. Praesent posuere orci at odio aliquet
																			feugiat</div>
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    {/*
																		<!-- Button --> */}
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td align="left">
                                                                                                <table className="center" border="0"
                                                                                                    cellspacing="0" cellpadding="0"
                                                                                                    bgcolor="#ed5258">
                                                                                                    <tr>
                                                                                                        <td className="content-spacing"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            height="34" width="22">
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="5"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                            <div className="button2"
                                                                                                                style="color:#ffffff; font-family:Arial,sans-serif; font-size:11px; line-height:18px; text-align:center; text-transform:uppercase; font-weight:bold">
                                                                                                                <a href="#"
                                                                                                                    target="_blank"
                                                                                                                    className="link-white"
                                                                                                                    style="color:#ffffff; text-decoration:none"><span
                                                                                                                        className="link-white"
                                                                                                                        style="color:#ffffff; text-decoration:none">Read
																											more</span></a>
                                                                                                            </div>
                                                                                                            <table width="100%"
                                                                                                                border="0"
                                                                                                                cellspacing="0"
                                                                                                                cellpadding="0"
                                                                                                                className="spacer"
                                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                <tr>
                                                                                                                    <td height="3"
                                                                                                                        className="spacer"
                                                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                        </td>
                                                                                                        <td className="content-spacing"
                                                                                                            style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                                            width="22"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    {/*
																		<!-- END Button --> */}
                                                                                    <table width="100%" border="0" cellspacing="0"
                                                                                        cellpadding="0" className="spacer"
                                                                                        style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                        <tr>
                                                                                            <td height="20" className="spacer"
                                                                                                style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="20"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" className="spacer"
                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                    <tr>
                                                        <td height="40" className="spacer"
                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <div className="hide-for-mobile">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="60" className="img" style="font-size:0pt; line-height:0pt; text-align:left"
                                        bgcolor="#ed5258">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ed5258">
                            <tr>
                                <td className="m-td" style="font-size:0pt; line-height:0pt; text-align:left">&nbsp;</td>
                                <td align="center" width="650" className="auto">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" className="mobile-shell">
                                        <tr>
                                            <td className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th className="column-top"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0"
                                                            width="240">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td className="img"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="60" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <div className="img-m-center"
                                                                            style="font-size:0pt; line-height:0pt"><a href="#"
                                                                                target="_blank"><img
                                                                                    src="images/free_footer_logo.png" border="0"
                                                                                    width="125" height="18" alt="" /></a></div>
                                                                        <div style="font-size:0pt; line-height:0pt;"
                                                                            className="mobile-br-15"></div>

                                                                        <div style="font-size:0pt; line-height:0pt;"
                                                                            className="mobile-br-15"></div>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th className="column-top"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0"
                                                            bgcolor="#2d2d31">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="right">
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="60" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <table className="center" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="32">
                                                                                    <a href="https://www.facebook.com/SMQCgoogle/"
                                                                                        target="_blank">
                                                                                        <img src="images/ico4_facebook.jpg"
                                                                                            border="0" width="18" height="21"
                                                                                            alt="" />
                                                                                    </a>
                                                                                </td>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="32">
                                                                                    <a href="https://mobile.twitter.com/OscarVa96229360"
                                                                                        target="_blank">
                                                                                        <img src="images/ico4_twitter.jpg"
                                                                                            border="0" width="18" height="21"
                                                                                            alt="" />
                                                                                    </a>
                                                                                </td>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="32">
                                                                                    <a href="https://goo.gl/maps/ogCXtfoq6sXqgs1b7"
                                                                                        target="_blank">
                                                                                        <img src="images/ico4_gplus.jpg" border="0"
                                                                                            width="18" height="21" alt="" />
                                                                                    </a>
                                                                                </td>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="32">
                                                                                    <a href="https://www.youtube.com/channel/UCSnYERmeZDilOz3ydEabDFA?view_as=subscriber"
                                                                                        target="_blank">
                                                                                        <img src="images/ico4_youtube.jpg"
                                                                                            border="0" width="18" height="21"
                                                                                            alt="" />
                                                                                    </a>
                                                                                </td>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="32">
                                                                                    <a href="https://www.instagram.com/solumobil_bueno/"
                                                                                        target="_blank">
                                                                                        <img src="images/ico4_instagram.jpg"
                                                                                            border="0" width="18" height="21"
                                                                                            alt="" />
                                                                                    </a>
                                                                                </td>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="32">
                                                                                    <a href="https://pin.it/6inYjOo"
                                                                                        target="_blank">
                                                                                        <img src="images/ico4_pinterest.jpg"
                                                                                            border="0" width="18" height="21"
                                                                                            alt="" />
                                                                                    </a>
                                                                                </td>
                                                                                {/* <!-- <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="32">
                                                                                    <a href="#" target="_blank">
                                                                                        <img src="images/ico4_linkedin.jpg"
                                                                                            border="0" width="18" height="21"
                                                                                            alt="" />
                                                                                    </a>
                                                                                </td>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left"
                                                                                    width="18">
                                                                                    <a href="#" target="_blank">
                                                                                        <img src="images/ico4_rss.jpg" border="0"
                                                                                            width="18" height="21" alt="" />
                                                                                    </a>
                                                                                </td> --> */}
                                                                            </tr>
                                                                        </table>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="30" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td className="m-td" style="font-size:0pt; line-height:0pt; text-align:left" bgcolor="#2d2d31">
                                    &nbsp;</td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ed5258">
                            <tr>
                                <td className="m-td" style="font-size:0pt; line-height:0pt; text-align:left">&nbsp;</td>
                                <td align="center" width="650" className="auto">
                                    <table width="650" align="center" border="0" cellspacing="0" cellpadding="0"
                                        className="mobile-shell">
                                        <tr>
                                            <td className="td"
                                                style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th className="column-top-black"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0"
                                                            width="240">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td className="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                    <td>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td className="img"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left">
                                                                                    <div className="text-footer"
                                                                                        style="color:#ffffff; font-family:Arial,sans-serif, 'HG'; font-size:14px; line-height:18px; text-align:left">
                                                                                        Copyright &copy; 2018 Company Name</div>
                                                                                    <div style="font-size:0pt; line-height:0pt;"
                                                                                        className="mobile-br-15"></div>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div style="font-size:0pt; line-height:0pt;"
                                                                            className="mobile-br-15"></div>

                                                                    </td>
                                                                    <td className="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th className="column-top"
                                                            style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0"
                                                            bgcolor="#2d2d31">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td className="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                    <td align="right">
                                                                        <div className="text-footer-r2"
                                                                            style="color:#ffffff; font-family:Arial,sans-serif, 'HG'; font-size:12px; line-height:16px; text-align:right; font-weight:bold">
                                                                            East Pixel Bld. 99, Creative City 9000, Republic of
																Design</div>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="20" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                        <div className="text-footer-r"
                                                                            style="color:#ffffff; font-family:Arial,sans-serif, 'HG'; font-size:14px; line-height:18px; text-align:right; text-transform:uppercase">
                                                                            <a href="#" target="_blank" className="link-white-u"
                                                                                style="color:#ffffff; text-decoration:underline"><span
                                                                                    className="link-white-u"
                                                                                    style="color:#ffffff; text-decoration:underline">Update</span></a>
																&nbsp; &nbsp;
																<a href="#" target="_blank" className="link-white-u"
                                                                                style="color:#ffffff; text-decoration:underline"><span
                                                                                    className="link-white-u"
                                                                                    style="color:#ffffff; text-decoration:underline">Unsubscribe</span></a>
                                                                        </div>
                                                                        <table width="100%" border="0" cellspacing="0"
                                                                            cellpadding="0" className="spacer"
                                                                            style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                            <tr>
                                                                                <td height="60" className="spacer"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%; mso-cellspacing:0px; mso-padding-alt:0px 0px 0px 0px">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                    <td className="content-spacing"
                                                                        style="font-size:0pt; line-height:0pt; text-align:left"
                                                                        width="1"></td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td className="m-td" style="font-size:0pt; line-height:0pt; text-align:left" bgcolor="#2d2d31">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </>
    );
}

export default ProductHTMLEmail;
