import React from 'react'
import { Tooltip } from '@material-ui/core'

const TooltipMsg = ({ text, placement, element, style }) => {

    return (
        <Tooltip title={text} placement={placement} style={style}>
            {element}
        </Tooltip>
    );
}

export default TooltipMsg;
