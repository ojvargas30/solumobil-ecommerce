import Link from 'next/link'
import { useContext, useState, useEffect } from 'react'
import { DataContext } from '../../store/GlobalState'
import { addToCart } from '../../store/Actions'
import ConfirmMsg from '../ConfirmMsg'
import { currencyFormat } from '../../utils/funcs'

const ModernProductCard = ({ product, handleCheck }) => {

    const { state, dispatch } = useContext(DataContext)
    const { cart, auth, selectedCurrency, currencies } = state

    // Confirmación de acción
    const [isOpen, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleDialogClose = () => {
        setOpen(false);
    };

    const userLink = () => {
        return (
            <a
                className="cursor-pointer"
                disabled={product.inStock === 0 ? true : false}
                onClick={() => dispatch(addToCart(product, cart, 'warning'))}
            >
                Añadir al carrito
            </a>
        )
    }

    useEffect(() => {

        $(document).ready(function () {
            $(".largeGrid").click(function () {
                $(this).find("a").addClass("active");
                $(".smallGrid a").removeClass("active");

                $(".modern-product")
                    .addClass("large")
                    .each(function () { });
                setTimeout(function () {
                    $(".info-large").show();
                }, 200);
                setTimeout(function () {
                    $(".view_gallery").trigger("click");
                }, 400);

                return false;
            });

            $(".smallGrid").click(function () {
                $(this).find("a").addClass("active");
                $(".largeGrid a").removeClass("active");

                $("div.modern-product").removeClass("large");
                $(".make3D").removeClass("animate");
                $(".info-large").fadeOut("fast");
                setTimeout(function () {
                    $("div.flip-back").trigger("click");
                }, 400);
                return false;
            });

            $(".smallGrid").click(function () {
                $(".modern-product").removeClass("large");
                return false;
            });

            $(".colors-large a").click(function () {
                return false;
            });

            $(".modern-product").each(function (i, el) {
                // Lift card and show stats on Mouseover
                $(el)
                    .find(".make3D")
                    .hover(
                        function () {
                            $(this).parent().css("z-index", "20");
                            $(this).addClass("animate");
                            $(this)
                                .find("div.carouselNext, div.carouselPrev")
                                .addClass("visible");
                        },
                        function () {
                            $(this).removeClass("animate");
                            $(this).parent().css("z-index", "1");
                            $(this)
                                .find("div.carouselNext, div.carouselPrev")
                                .removeClass("visible");
                        }
                    );

                // Flip card to the back side
                $(el)
                    .find(".view_gallery")
                    .click(function () {
                        $(el).find("div.carouselNext, div.carouselPrev").removeClass("visible");
                        $(el).find(".make3D").addClass("flip-10");
                        setTimeout(function () {
                            $(el)
                                .find(".make3D")
                                .removeClass("flip-10")
                                .addClass("flip90")
                                .find("div.shadow")
                                .show()
                                .fadeTo(80, 1, function () {
                                    $(el).find(".product-front, .product-front div.shadow").hide();
                                });
                        }, 50);

                        setTimeout(function () {
                            $(el).find(".make3D").removeClass("flip90").addClass("flip190");
                            $(el)
                                .find(".product-back")
                                .show()
                                .find("div.shadow")
                                .show()
                                .fadeTo(90, 0);
                            setTimeout(function () {
                                $(el)
                                    .find(".make3D")
                                    .removeClass("flip190")
                                    .addClass("flip180")
                                    .find("div.shadow")
                                    .hide();
                                setTimeout(function () {
                                    $(el).find(".make3D").css("transition", "100ms ease-out");
                                    $(el).find(".cx, .cy").addClass("s1");
                                    setTimeout(function () {
                                        $(el).find(".cx, .cy").addClass("s2");
                                    }, 100);
                                    setTimeout(function () {
                                        $(el).find(".cx, .cy").addClass("s3");
                                    }, 200);
                                    $(el)
                                        .find("div.carouselNext, div.carouselPrev")
                                        .addClass("visible");
                                }, 100);
                            }, 100);
                        }, 150);
                    });

                // Flip card back to the front side
                $(el)
                    .find(".flip-back")
                    .click(function () {
                        $(el).find(".make3D").removeClass("flip180").addClass("flip190");
                        setTimeout(function () {
                            $(el).find(".make3D").removeClass("flip190").addClass("flip90");

                            $(el)
                                .find(".product-back div.shadow")
                                .css("opacity", 0)
                                .fadeTo(100, 1, function () {
                                    $(el).find(".product-back, .product-back div.shadow").hide();
                                    $(el).find(".product-front, .product-front div.shadow").show();
                                });
                        }, 50);

                        setTimeout(function () {
                            $(el).find(".make3D").removeClass("flip90").addClass("flip-10");
                            $(el).find(".product-front div.shadow").show().fadeTo(100, 0);
                            setTimeout(function () {
                                $(el).find(".product-front div.shadow").hide();
                                $(el)
                                    .find(".make3D")
                                    .removeClass("flip-10")
                                    .css("transition", "100ms ease-out");
                                $(el).find(".cx, .cy").removeClass("s1 s2 s3");
                            }, 100);
                        }, 150);
                    });

                makeCarousel(el);
            });

            $(".add-cart-large").each(function (i, el) {
                $(el).click(function () {
                    var carousel = $(this).parent().parent().find(".carousel-container");
                    var img = carousel.find("img").eq(carousel.attr("rel"))[0];
                    var position = $(img).offset();

                    var productName = $(this).parent().find("h4").get(0).innerHTML;

                    $("body").append('<div class="floating-cart"></div>');
                    var cart = $("div.floating-cart");
                    $("<img src='" + img.src + "' class='floating-image-large' />").appendTo(
                        cart
                    );

                    $(cart)
                        .css({ top: position.top + "px", left: position.left + "px" })
                        .fadeIn("slow")
                        .addClass("moveToCart");
                    setTimeout(function () {
                        $("body").addClass("MakeFloatingCart");
                    }, 800);

                    setTimeout(function () {
                        $("div.floating-cart").remove();
                        $("body").removeClass("MakeFloatingCart");

                        var cartItem =
                            "<div class='cart-item'><div class='img-wrap'><img src='" +
                            img.src +
                            "' alt='' /></div><span>" +
                            productName +
                            "</span><strong>$39</strong><div class='cart-item-border'></div><div class='delete-item'></div></div>";

                        $("#cart .empty").hide();
                        $("#cart").append(cartItem);
                        $("#checkout").fadeIn(500);

                        $("#cart .cart-item")
                            .last()
                            .addClass("flash")
                            .find(".delete-item")
                            .click(function () {
                                $(this)
                                    .parent()
                                    .fadeOut(300, function () {
                                        $(this).remove();
                                        if ($("#cart .cart-item").size() == 0) {
                                            $("#cart .empty").fadeIn(500);
                                            $("#checkout").fadeOut(500);
                                        }
                                    });
                            });
                        setTimeout(function () {
                            $("#cart .cart-item").last().removeClass("flash");
                        }, 10);
                    }, 1000);
                });
            });

            /* ----  Image Gallery Carousel   ---- */
            function makeCarousel(el) {
                var carousel = $(el).find(".carousel ul");
                var carouselSlideWidth = 315;
                var carouselWidth = 0;
                var isAnimating = false;
                var currSlide = 0;
                $(carousel).attr("rel", currSlide);

                // building the width of the casousel
                $(carousel)
                    .find("li")
                    .each(function () {
                        carouselWidth += carouselSlideWidth;
                    });
                $(carousel).css("width", carouselWidth);

                // Load Next Image
                $(el)
                    .find("div.carouselNext")
                    .on("click", function () {
                        var currentLeft = Math.abs(parseInt($(carousel).css("left")));
                        var newLeft = currentLeft + carouselSlideWidth;
                        if (newLeft == carouselWidth || isAnimating === true) {
                            return;
                        }
                        $(carousel).css({
                            left: "-" + newLeft + "px",
                            transition: "300ms ease-out"
                        });
                        isAnimating = true;
                        currSlide++;
                        $(carousel).attr("rel", currSlide);
                        setTimeout(function () {
                            isAnimating = false;
                        }, 300);
                    });

                // Load Previous Image
                $(el)
                    .find("div.carouselPrev")
                    .on("click", function () {
                        var currentLeft = Math.abs(parseInt($(carousel).css("left")));
                        var newLeft = currentLeft - carouselSlideWidth;
                        if (newLeft < 0 || isAnimating === true) {
                            return;
                        }
                        $(carousel).css({
                            left: "-" + newLeft + "px",
                            transition: "300ms ease-out"
                        });
                        isAnimating = true;
                        currSlide--;
                        $(carousel).attr("rel", currSlide);
                        setTimeout(function () {
                            isAnimating = false;
                        }, 300);
                    });
            }

            $(".sizes a span, .categories a span").each(function (i, el) {
                $(el).append('<span class="x"></span><span class="y"></span>');

                $(el)
                    .parent()
                    .on("click", function () {
                        if ($(this).hasClass("checked")) {
                            $(el).find(".y").removeClass("animate");
                            setTimeout(function () {
                                $(el).find(".x").removeClass("animate");
                            }, 50);
                            $(this).removeClass("checked");
                            return false;
                        }

                        $(el).find(".x").addClass("animate");
                        setTimeout(function () {
                            $(el).find(".y").addClass("animate");
                        }, 100);
                        $(this).addClass("checked");
                        return false;
                    });
            });

            $(".add_to_cart").click(function () {
                var productCard = $(this).parent();
                var position = productCard.offset();
                var productImage = $(productCard).find("img").get(0).src;
                var productName = $(productCard).find(".product_name").get(0).innerHTML;

                $("body").append('<div class="floating-cart"></div>');
                var cart = $("div.floating-cart");
                productCard.clone().appendTo(cart);
                $(cart)
                    .css({ top: position.top + "px", left: position.left + "px" })
                    .fadeIn("slow")
                    .addClass("moveToCart");
                setTimeout(function () {
                    $("body").addClass("MakeFloatingCart");
                }, 800);
                setTimeout(function () {
                    $("div.floating-cart").remove();
                    $("body").removeClass("MakeFloatingCart");

                    var cartItem =
                        "<div class='cart-item'><div class='img-wrap'><img src='" +
                        productImage +
                        "' alt='' /></div><span>" +
                        productName +
                        "</span><strong>$39</strong><div class='cart-item-border'></div><div class='delete-item'></div></div>";

                    $("#cart .empty").hide();
                    $("#cart").append(cartItem);
                    $("#checkout").fadeIn(500);

                    $("#cart .cart-item")
                        .last()
                        .addClass("flash")
                        .find(".delete-item")
                        .click(function () {
                            $(this)
                                .parent()
                                .fadeOut(300, function () {
                                    $(this).remove();
                                    if ($("#cart .cart-item").size() == 0) {
                                        $("#cart .empty").fadeIn(500);
                                        $("#checkout").fadeOut(500);
                                    }
                                });
                        });
                    setTimeout(function () {
                        $("#cart .cart-item").last().removeClass("flash");
                    }, 10);
                }, 1000);
            });
        });

    }, []);

    const adminLink = () => {
        return (
            <>
                <Link href={`create/${product._id}`}>
                    <a className="btn btn-lg btn-purple"
                        style={{ marginRight: '5px', flex: 1 }}>
                        <i className="fas fa-edit"></i>
                    </a>
                </Link>

                <button className="btn btn-lg btn-danger"
                    style={{ marginLeft: '5px', flex: 1 }}
                    onClick={() => {
                        handleClickOpen()
                        dispatch({
                            type: 'ADD_CONFIRM',
                            payload: [{
                                data: '',
                                id: product._id,
                                title: product.title,
                                type: 'INACTIVE_PRODUCT'
                            }]
                        })
                    }}
                >
                    <i className="fas fa-trash"></i>
                </button>
            </>
        )
    }

    return (
        <>
            <div className="modern-product my-3 mx-2">
                <div className="info-large" title={product.title}>
                    <h4 title={product.title}>{product.title}</h4>
                    <div className="sku">
                        PRODUCT SKU: <strong>89356</strong>
                    </div>

                    <div className="price-big">
                        {/* <span>$43</span> */}
                        {
                            selectedCurrency.selected == 'cop'
                                ? currencyFormat(product.price)
                                : selectedCurrency.selected == 'usd'
                                    ? currencyFormat((product.price / currencies.usd).toFixed(2), 'en-US', "USD")
                                    : ''
                        }
                    </div>

                    <h3>COLORS</h3>
                    <div className="colors-large">
                        <ul>
                            <li><a href="" style={{ background: "#222" }}><span></span></a></li>
                            <li><a href="" style={{ background: "#6e8cd5" }}><span></span></a></li>
                            <li><a href="" style={{ background: "#9b887b" }}><span></span></a></li>
                            <li><a href="" style={{ background: "#44c28d" }}><span></span></a></li>
                        </ul>
                    </div>

                    <h3>SIZE</h3>
                    <div className="sizes-large">
                        <span>XS</span>
                        <span>S</span>
                        <span>M</span>
                        <span>L</span>
                        <span>XL</span>
                        <span>XXL</span>
                    </div>

                    {/* <button className="add-cart-large">Añadir al carrito</button> */}

                </div>
                <div className="make3D">
                    <div className="product-front">
                        <div className="shadow"></div>
                        <img className="h-100" src={product.images[0].url} alt={product.title} />
                        <div className="image_overlay"></div>
                        <div className="add_to_cart">
                            {
                                !auth.user || auth.user.role !== 'admin'
                                    ? userLink()
                                    : adminLink()
                            }
                        </div>
                        <div className="view_gallery">Ver galería</div>
                        <div className="stats">
                            <div className="stats-container">
                                <span className="product_price">
                                    {
                                        selectedCurrency.selected == 'cop'
                                            ? currencyFormat(product.price)
                                            : selectedCurrency.selected == 'usd'
                                                ? currencyFormat((product.price / currencies.usd).toFixed(2), 'en-US', "USD")
                                                : ''
                                    }
                                </span>
                                <Link
                                    // as={`product/${product.title.replace(/ /g, "-")}`}
                                    as={`/product/${product._id}`}
                                    href={`/product/${product._id}`}
                                >
                                    <a>
                                        <span className="product_name hover-blue" title={product.title}>
                                            {
                                                product.title.length < 40
                                                    ? product.title.replace(/\b\w/g, l => l.toUpperCase())
                                                    : product.title.replace(/\b\w/g, l => l.toUpperCase()).slice(0, `-${product.title.length - 39}`)
                                            }
                                        </span>
                                    </a>
                                </Link>
                                <p>
                                    {
                                        (product.desc.length < 105)
                                            ? product.desc
                                            : product.desc.slice(0, `-${product.desc.length - 105}`)
                                    }
                                </p>

                                {/* <div className="product-options">
                                    <strong>SIZES</strong>
                                    <span>XS, S, M, L, XL, XXL</span>
                                    <strong>COLORS</strong>
                                    <div className="colors">
                                        <div className="c-blue"><span></span></div>
                                        <div className="c-red"><span></span></div>
                                        <div className="c-white"><span></span></div>
                                        <div className="c-green"><span></span></div>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                    </div>

                    <div className="product-back">
                        <div className="shadow"></div>
                        <div className="carousel">
                            <ul className="carousel-container" rel="0" style={{ width: "945px" }}>
                                {
                                    product.images.map((img, index) => (
                                        <li key={index}>
                                            <img className="h-100 w-100" src={img.url} alt={img.url} />
                                        </li>
                                    ))
                                }
                            </ul>
                            <div className="arrows-perspective">
                                <div className="carouselPrev">
                                    <div className="y"></div>
                                    <div className="x"></div>
                                </div>
                                <div className="carouselNext">
                                    <div className="y"></div>
                                    <div className="x"></div>
                                </div>
                            </div>
                        </div>
                        <div className="flip-back">
                            <div className="cy"></div>
                            <div className="cx"></div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default ModernProductCard;
