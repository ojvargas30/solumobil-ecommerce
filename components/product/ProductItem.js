import Link from 'next/link'
import { useContext, useState } from 'react'
import { DataContext } from '../../store/GlobalState'
import { addToCart } from '../../store/Actions'
import ConfirmMsg from '../ConfirmMsg'
import { currencyFormat } from '../../utils/funcs'

const ProductItem = ({ product, handleCheck }) => {

    const { state, dispatch } = useContext(DataContext)
    const { cart, auth, selectedCurrency, currencies } = state

    // Confirmación de acción
    const [isOpen, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleDialogClose = () => {
        setOpen(false);
    };

    const userLink = () => {
        return (
            <a
                className="btnItemBuy cursor-pointer"
                disabled={product.inStock === 0 ? true : false}
                onClick={() => dispatch(addToCart(product, cart, 'warning'))}
            >
                Añadir al carrito
            </a>
        )
    }

    const adminLink = () => {
        return (
            <>
                <Link href={`create/${product._id}`}>
                    <a className="btn btn-secondary"
                        style={{ marginRight: '5px', flex: 1 }}>
                        <i className="fas fa-edit"></i>
                    </a>
                </Link>

                <button className="btn btn-danger"
                    style={{ marginLeft: '5px', flex: 1 }}
                    onClick={() => {
                        handleClickOpen()
                        dispatch({
                            type: 'ADD_CONFIRM',
                            payload: [{
                                data: '',
                                id: product._id,
                                title: product.title,
                                type: 'INACTIVE_PRODUCT'
                            }]
                        })
                    }}
                >
                    <i className="fas fa-trash"></i>
                </button>
            </>
        )
    }

    return (
        <div
            className="itemCard">
            {
                auth.user && auth.user.role === 'admin' &&
                <input
                    type="checkbox"
                    checked={product.checked}
                    className="position-absolute cursor-pointer"
                    style={{ height: '20px', width: '20px', right: '1%', top: '1%', zIndex: '200' }}
                    onChange={() => handleCheck(product._id)}
                />
            }
            <div
                className="corner-ribbon top-left sticky red shadow" style={{ fontSize: "0.7rem" }}>
                En oferta
            </div>
            <Link href={`/product/${product._id}`}>
                <a>
                    <div
                        className="img-box">
                        <img src={product.images[0].url} alt={product.images[0].url} />
                    </div>
                </a>
            </Link>
            <div
                className="detailsItem">
                <Link href={`/product/${product._id}`}>
                    <a>
                        <h2
                            className="pt-2" title={product.title}>
                            <div className="w-40" title={product.title}>
                                {
                                    product.title.length < 36
                                        ? product.title.replace(/\b\w/g, l => l.toUpperCase())
                                        : product.title.replace(/\b\w/g, l => l.toUpperCase()).slice(0, `-${product.title.length - 35}`)
                                }
                            </div>
                            <div></div>

                            <span title={product.desc}>
                                {
                                    (product.desc.length < 105)
                                        ? product.desc
                                        : product.desc.slice(0, `-${product.desc.length - 105}`)
                                }
                            </span>
                        </h2>
                        <div
                            className="price">


                            {
                                selectedCurrency.selected == 'cop'
                                    ? currencyFormat(product.price)
                                    : selectedCurrency.selected == 'usd'
                                        ? currencyFormat((product.price / currencies.usd).toFixed(2), 'en-US', "USD")
                                        : ''
                            }
                        </div>
                        {
                            product.inStock > 0
                                ? <p className="text-success"> En Stock</p>
                                : <p className="text-danger"> Fuera de Stock</p>
                        }
                    </a>
                </Link>

                {
                    !auth.user || auth.user.role !== 'admin'
                        ? userLink()
                        : adminLink()
                }
            </div>

            <ConfirmMsg
                isOpen={isOpen}
                handleClose={handleDialogClose}
                subtitle='¿Quieres eliminar este producto?'
            />
        </div>
    );
}

export default ProductItem;
