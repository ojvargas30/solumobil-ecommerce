import { postData } from '../../utils/fetchData'
// import { useState, useContext } from 'react'
// import { DataContext } from '../../store/GlobalState'

const Newsletter = () => {

    // const { state, dispatch } = useContext(DataContext)

    const [email, setEmail] = useState('')

    const registerUserNewsletter = async () => {

        if (!email)
            return dispatch({ type: 'NOTIFY', payload: { error: 'El correo no puede quedar vacío' } })

        dispatch({ type: 'NOTIFY', payload: { loading: true } })

        const res = await postData('usernewsletter', { email })
        if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back
        return dispatch({ type: 'NOTIFY', payload: { success: res.msg } })
    }

    return (
        <div className="wrap-footer-item footer-item-second">
            <h3 className="item-header">REGÍSTRESE PARA EL BOLETÍN</h3>
            <div className="item-content">
                <div className="wrap-newletter-footer">
                    <form className="frm-newletter" id="frm-newletter">
                        <input
                            type="email"
                            className="input-email"
                            name="email"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            placeholder="Ingrese su correo"
                        />
                        <button type="button" onClick={registerUserNewsletter}
                            className="btn-submit">Suscríbirse</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Newsletter;
