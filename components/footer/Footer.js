// import Newsletter from '../components/newsletter/Newsletter'
// import { useState, useContext } from 'react'
// import { DataContext } from '../../store/GlobalState'
// import { postData } from '../../utils/fetchData'
// import { useRouter } from 'next/router'
import Link from 'next/link'

const Footer = () => {
    // const { dispatch } = useContext(DataContext)
    const currentYear = new Date().getFullYear()
    // const router = useRouter()
    // const [email, setEmail] = useState('')

    // const registerUserNewsletter = async e => {
    //     try {
    //         dispatch({ type: 'NOTIFY', payload: { loading: true } })

    //         const res = await postData('user/newsletter', { email })

    //         if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

    //         dispatch({ type: 'NOTIFY', payload: { success: res.msg } })

    //         router.push('/')

    //     } catch (err) {
    //         dispatch({ type: 'NOTIFY', payload: { error: err } })
    //     }
    // }

    return (
        <>


            {/* FOOTER */}
            <footer id="footer">
                <div className="wrap-footer-content footer-style-1">

                    <div className="wrap-function-info">
                        <div className="container">
                            <ul>
                                <li className="fc-info-item" title="Por compras mayores a 200.000 COP">
                                    <i className="fa fa-truck" title="Por compras mayores a 200.000 COP" aria-hidden="true"></i>
                                    <div className="wrap-left-info" title="Por compras mayores a 200.000 COP">
                                        <h4 className="fc-name" title="Por compras mayores a 200.000 COP">DOMICILIO O RECOGIDA</h4>
                                        <p className="fc-desc" title="Por compras mayores a 200.000 COP">Bogotá, Colombia</p>
                                    </div>

                                </li>
                                <li className="fc-info-item">
                                    <i className="fas fa-recycle" aria-hidden="true"></i>
                                    <div className="wrap-left-info">
                                        <h4 className="fc-name">Garantía</h4>
                                        <p className="fc-desc">30 días devolución de dinero</p>
                                    </div>

                                </li>
                                <li className="fc-info-item">
                                    <i className="fas fa-credit-card" aria-hidden="true"></i>
                                    <div className="wrap-left-info">
                                        <h4 className="fc-name">PAGO SEGURO</h4>
                                        <p className="fc-desc">Asegure su pago en línea</p>
                                    </div>

                                </li>
                                <li className="fc-info-item">
                                    <i className="fa fa-life-ring" aria-hidden="true"></i>
                                    <div className="wrap-left-info">
                                        <h4 className="fc-name">SOPORTE EN LÍNEA</h4>
                                        <p className="fc-desc">Contamos con soporte 24/7</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {/* <!--End function info--> */}

                    <div className="main-footer-content">

                        <div className="container">

                            <div className="row">

                                <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <div className="wrap-footer-item">
                                        <h3 className="item-header">Detalles de contacto</h3>
                                        <div className="item-content">
                                            <div className="wrap-contact-detail">
                                                <ul>
                                                    <li>
                                                        <i className="fa fa-map-marker" aria-hidden="true"></i>
                                                        <p className="contact-txt">
                                                            <a href="https://goo.gl/maps/ogCXtfoq6sXqgs1b7" target="_blank">
                                                                Cra. 116c #66a-16, Bogotá
                                                            </a>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <i className="fa fa-phone" aria-hidden="true"></i>
                                                        <p className="contact-txt">
                                                            <a href="tel:573208557457" target="_blank">
                                                                (+57) 3208557457
                                                            </a>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <i className="fa fa-envelope" aria-hidden="true"></i>
                                                        <p className="contact-txt">
                                                            <a href="mailto:oscarjaviervargas@hotmail.com" target="_blank">
                                                                oscarjaviervargas@hotmail.com
                                                            </a>
                                                        </p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">

                                    <div className="wrap-footer-item">
                                        <h3 className="item-header">LÍNEA DIRECTA</h3>
                                        <div className="item-content">
                                            <div className="wrap-hotline-footer">
                                                {/* <span className="desc">Llámanos gratis</span> */}

                                                <a title="Línea de atención: (+57) 3208557457 - (+57) 3133043714" target="_blank"
                                                    href="https://api.whatsapp.com/send?phone=573208557457&text=%C2%A1Saludos! vengo de la web y quisiera saber mas de sus productos." >
                                                    <b className="phone-number">(+57) 3208557457 - (+57) 3118330760</b>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    {/* <div className="wrap-footer-item footer-item-second">
                                        <h3 className="item-header">REGÍSTRESE PARA EL BOLETÍN</h3>
                                        <div className="item-content">
                                            <div className="wrap-newletter-footer">
                                                <form className="frm-newletter" id="frm-newletter">
                                                    <input
                                                        type="email"
                                                        className="input-email"
                                                        name="email"
                                                        value={email}
                                                        onChange={e => setEmail(e.target.value)}
                                                        placeholder="Ingrese su correo"
                                                    />
                                                    <button type="button" onClick={registerUserNewsletter}
                                                        className="btn-submit">Suscríbirse</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div> */}

                                    {/* <Newsletter /> */}

                                </div>

                                <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 box-twin-content ">
                                    <div className="row">
                                        <div className="wrap-footer-item twin-item">
                                            <h3 className="item-header">Mi cuenta</h3>
                                            <div className="item-content">
                                                <div className="wrap-vertical-nav">
                                                    <ul>
                                                        <li className="menu-item">
                                                            <a href="/profile" className="link-term">
                                                                Mi cuenta
                                                            </a>
                                                        </li>
                                                        {/* <li className="menu-item">
                                                                    <a href="#" className="link-term">
                                                                        Marcas
                                                                    </a>
                                                                </li>
                                                                <li className="menu-item">
                                                                    <a href="#" className="link-term">
                                                                        Certificados de regalo
                                                                    </a>
                                                                </li>
                                                                <li className="menu-item">
                                                                    <a href="#" className="link-term">
                                                                        Afiliados
                                                                    </a>
                                                                </li>
                                                                <li className="menu-item">
                                                                    <a href="#" className="link-term">
                                                                        Lista de deseos
                                                                    </a>
                                                                </li> */}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="wrap-footer-item twin-item">
                                            <h3 className="item-header">Información</h3>
                                            <div className="item-content">
                                                <div className="wrap-vertical-nav">
                                                    <ul>
                                                        <li className="menu-item">
                                                            <a href="tel:3208557457" className="link-term">
                                                                Contacta con nosotros
                                                            </a>
                                                        </li>
                                                        {/* <li className="menu-item">
                                                                    <a href="#" className="link-term">
                                                                        Devoluciones
                                                                    </a>
                                                                </li> */}
                                                        <li className="menu-item">
                                                            <a href="https://goo.gl/maps/ogCXtfoq6sXqgs1b7" className="link-term">
                                                                Ubicación en el mapa
                                                            </a>
                                                        </li>
                                                        {/* <li className="menu-item">
                                                                    <a href="#" className="link-term">
                                                                        Especiales
                                                                    </a>
                                                                </li> */}
                                                        <li className="menu-item">
                                                            <a href="/profile" className="link-term">
                                                                Historial de ordenes
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="row">

                                <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <div className="wrap-footer-item">
                                        <h3 className="item-header">Usamos pagos seguros:
                                            {/* <i className="fab fa-paypal fa-2x text-secondary"></i> */}
                                        </h3>
                                        <div className="item-content pb-3 px-3 mb-1">
                                            <div className="wrap-list-item wrap-gallery">
                                                <img src="https://res.cloudinary.com/linda-leblanc/image/upload/v1625352508/esolumobil/we0hykqwg1h9z9ukop1v.png" style={{ maxWidth: '150px' }} />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12 pb-4">
                                    <div className="wrap-footer-item">
                                        <h3 className="item-header">Redes sociales</h3>
                                        <div className="item-content">
                                            <div className="wrap-list-item social-network">
                                                <ul>
                                                    <li>
                                                        <a href="https://api.whatsapp.com/send?phone=573208557457&text=%C2%A1Hola!%20Quisiera%20saber%20mas%20acerca%20de%20los%20servicios%20y%20productos%20tecnologicos%20que%20ofrece"
                                                            className="link-to-item"
                                                            target="_blank"
                                                            title="Whatsapp">
                                                            <i className="fab fa-whatsapp"
                                                                aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://goo.gl/maps/ogCXtfoq6sXqgs1b7"
                                                            className="link-to-item"
                                                            target="_blank"
                                                            title="Ubicación">
                                                            <i className="fas fa-map-marker-alt"
                                                                aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.youtube.com/channel/UCSnYERmeZDilOz3ydEabDFA?view_as=subscriber"
                                                            className="link-to-item"
                                                            target="_blank"
                                                            title="Youtube">
                                                            <i className="fab fa-youtube"
                                                                aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://mobile.twitter.com/OscarVa96229360"
                                                            className="link-to-item"
                                                            target="_blank"
                                                            title="twitter">
                                                            <i className="fab fa-twitter"
                                                                aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.facebook.com/SMQCgoogle/"
                                                            className="link-to-item"
                                                            target="_blank"
                                                            title="facebook">
                                                            <i className="fab fa-facebook"
                                                                aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://pin.it/6inYjOo"
                                                            className="link-to-item"
                                                            target="_blank"
                                                            title="pinterest">
                                                            <i className="fab fa-pinterest"
                                                                aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://www.instagram.com/solumobil_bueno/"
                                                            className="link-to-item"
                                                            target="_blank"
                                                            title="instagram">
                                                            <i className="fab fa-instagram"
                                                                aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://vimeo.com/user140988817"
                                                            className="link-to-item"
                                                            target="_blank"
                                                            title="vimeo">
                                                            <i className="fab fa-vimeo"
                                                                aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {/* <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                            <div className="wrap-footer-item">
                                                <h3 className="item-header">Descargar la aplicación</h3>
                                                <div className="item-content">
                                                    <div className="wrap-list-item apps-list">
                                                        <ul>
                                                            <li>
                                                                <a href="#" className="link-to-item"
                                                                    title="our application on apple store">
                                                                    <figure>
                                                                        <img src="assets/images/brands/apple-store.png"
                                                                            alt="apple store" width="128" height="36" />
                                                                    </figure>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" className="link-to-item"
                                                                    title="our application on google play store">
                                                                    <figure>
                                                                        <img src="assets/images/brands/google-play-store.png"
                                                                            alt="google play store" width="128" height="36" />
                                                                    </figure>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> */}

                            </div>
                        </div>

                        {/* <div className="wrap-back-link">
                                    <div className="container">
                                        <div className="back-link-box">
                                            <h3 className="backlink-title">ENLACES RÁPIDOS</h3>
                                            <div className="back-link-row">
                                                <ul className="list-back-link" >
                                                    <li><span className="row-title">Mobiles:</span></li>
                                                    <li><a href="#" className="redirect-back-link" title="mobile">Mobiles</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="yphones">YPhones</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Gianee Mobiles GL">Gianee Mobiles GL</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Mobiles Karbonn">Mobiles Karbonn</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Mobiles Viva">Mobiles Viva</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Mobiles Intex">Mobiles Intex</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Mobiles Micrumex">Mobiles Micrumex</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Mobiles Bsus">Mobiles Bsus</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Mobiles Samsyng">Mobiles Samsyng</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Mobiles Lenova">Mobiles Lenova</a></li>
                                                </ul>

                                                <ul className="list-back-link" >
                                                    <li><span className="row-title">Tablets:</span></li>
                                                    <li><a href="#" className="redirect-back-link" title="Plesc YPads">Plesc YPads</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Samsyng Tablets" >Samsyng Tablets</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Qindows Tablets" >Qindows Tablets</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Calling Tablets" >Calling Tablets</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Micrumex Tablets" >Micrumex Tablets</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Lenova Tablets Bsus" >Lenova Tablets Bsus</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Tablets iBall" >Tablets iBall</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Tablets Swipe" >Tablets Swipe</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Tablets TVs, Audio" >Tablets TVs, Audio</a></li>
                                                </ul>

                                                <ul className="list-back-link" >
                                                    <li><span className="row-title">Fashion:</span></li>
                                                    <li><a href="#" className="redirect-back-link" title="Sarees Silk" >Sarees Silk</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="sarees Salwar" >sarees Salwar</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Suits Lehengas" >Suits Lehengas</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Biba Jewellery" >Biba Jewellery</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Rings Earrings" >Rings Earrings</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Diamond Rings" >Diamond Rings</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Loose Diamond Shoes" >Loose Diamond Shoes</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="BootsMen Watches" >BootsMen Watches</a></li>
                                                    <li><a href="#" className="redirect-back-link" title="Women Watches" >Women Watches</a></li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div> */}

                    </div>

                    <div className="coppy-right-box">
                        <div className="container">
                            <div className="coppy-right-item item-left">
                                <p className="coppy-right-text">&copy; Solumobil 2019 - {currentYear} Todos los derechos reservados.</p>
                            </div>
                            <div className="coppy-right-item item-right">
                                <div className="wrap-nav horizontal-nav">
                                    <ul>
                                        <li className="menu-item">
                                            <Link href="https://tec.solumobil.com/" scroll={false}>
                                                <a className="link-term" target="_blank">
                                                    Acerca de nosotros
                                                </a>
                                            </Link>
                                        </li>
                                        {/*
                                        <li className="menu-item">
                                            <a href="privacy-policy.html" className="link-term">
                                                Política de privacidad
                                            </a>
                                        </li>
                                        <li className="menu-item">
                                            <a href="terms-conditions.html" className="link-term">
                                                Terminos y condiciones
                                            </a>
                                        </li>
                                        <li className="menu-item">
                                            <a href="return-policy.html" className="link-term">
                                                Política de devoluciones
                                            </a>
                                        </li> */}
                                    </ul>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    );
}

export default Footer;
