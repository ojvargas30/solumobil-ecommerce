import { useRef, useEffect, useContext } from 'react';
import { patchData } from '../utils/fetchData'
import { DataContext } from '../store/GlobalState'
import { updateItem } from '../store/Actions'

const paypalBtn = ({ order }) => {

    const { state, dispatch } = useContext(DataContext)

    const refPaypalBtn = useRef();
    const { auth, orders, currencies } = state;

    // console.log(currencies.usd)
    // console.log((order.total / currencies.usd).toFixed(2))

    useEffect(() => {
        if (window.myButton) window.myButton.close();
        window.myButton = window.paypal
            .Buttons({
                createOrder: function (data, actions) {
                    // This function sets up the details of the transaction, including the amount and line item details.
                    return actions.order.create({
                        purchase_units: [{
                            amount: {
                                value: (order.total / currencies.usd).toFixed(2)
                            }
                        }]
                    });
                },
                onApprove: function (data, actions) {
                    // Loader
                    dispatch({ type: 'NOTIFY', payload: { loading: true } })

                    // This function captures the funds from the transaction.
                    return actions.order.capture().then(function (details) {

                        // console.log(details)

                        // This function shows a transaction success message to your buyer.
                        patchData(`order/payment/${order._id}`, {
                            paymentId: details.payer.payer_id
                        }, auth.token)
                            .then(res => {
                                if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

                                // Vaciar el carrito
                                // dispatch({ type: 'ADD_CART', payload: [] })

                                // const newOrder = {
                                //     ...res.newOrder,
                                //     user: auth.user
                                // }

                                // Añadir orden en perfil
                                // dispatch({ type: 'ADD_ORDERS', payload: [...orders, newOrder] })

                                dispatch(updateItem(orders, order._id, {
                                    ...order,
                                    paid: true,
                                    dateOfPayment: new Date(details.create_time),
                                    paymentId: details.payer.payer_id,
                                    method: 'Paypal'
                                }, 'ADD_ORDERS'))

                                return dispatch({ type: 'NOTIFY', payload: { success: res.msg } })
                            })

                    });
                }
            });

        window.myButton.render(refPaypalBtn.current);
        //This function displays Smart Payment Buttons on your web page.
    }, [])

    return <div ref={refPaypalBtn}></div>
}

export default paypalBtn;
