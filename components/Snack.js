import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

const Snack = ({ msg, bgColor, handleClose }) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Snackbar open={true}>
                <Alert onClose={handleClose} severity={bgColor}> {/* error,warning,info,success */}
                    {msg}
                </Alert>
            </Snackbar>
        </div>
    );
}

export default Snack;