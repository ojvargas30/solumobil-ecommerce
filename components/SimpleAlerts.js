import Alert from '@material-ui/lab/Alert';

const SimpleAlerts = ({ type = "success", msg }) => {

  console.log(type);
  console.log(typeof type);
  console.log(msg);

  return (
    <Alert severity={type ? type : 'success'}>{msg}</Alert>
  );
}

export default SimpleAlerts