import React from 'react';
import Button from '@material-ui/core/Button';

const Btn = (props) => {
    return (
        <div className="centerBtn align-items-center text-center">
            <Button
                variant="contained"
                color={(props.color) ? props.color : 'secondary'}
                className={`${props.classN} cursor-pointer`}
                type={(props.type) ? props.type : 'submit'}
                disabled={props.disabled}
            >
                {props.text}
            </Button>
        </div>
    );
}

export default Btn;
