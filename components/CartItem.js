import Link from 'next/link';
import { decrease, increase } from '../store/Actions'
import ConfirmMsg from './ConfirmMsg'
import { useState } from 'react'
import { currencyFormat } from '../utils/funcs';

const CartItem = ({ item, dispatch, cart, state }) => {

    const [isOpen, setOpen] = useState(false);

    const { selectedCurrency, currencies } = state

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleDialogClose = () => {
        setOpen(false);
    };

    return (
        <li className="pr-cart-item">
            <div className="product-image">
                <figure>
                    <img src={item.images[0].url} alt={item.images[0].url} />
                </figure>
            </div>
            <div className="product-name">
                <Link href={`/product/${item._id}`}>
                    <a className="link-to-product" >{item.title}</a>
                </Link>
                {/* <p><i className="fas fa-dollar-sign"></i> {item.quantity * item.price}</p> */}
                {
                    item.inStock > 0
                        ? <p className="">En Stock
                        {/* {item.inStock} */}
                        </p>
                        : <p className="text-danger">Fuera de Stock</p>
                }
            </div>
            <div className="price-field produtc-price">
                <p className="price">
                    {
                        selectedCurrency.selected == 'cop'
                            ? currencyFormat((item.quantity * item.price))
                            : selectedCurrency.selected == 'usd'
                                ? currencyFormat(((item.quantity * item.price) / currencies.usd).toFixed(2), 'en-US', "USD")
                                : ''
                    }
                </p>
            </div>
            <div className="quantity">
                <div className="quantity-input">
                    <input type="text" name="product-quatity"
                    // value={item.quantity}
                    // onChange={console.log('nextjsss')}
                    // pattern="[0-9]*"
                    />

                    {/* INCREMENTAR */}
                    <button onClick={() => dispatch(increase(cart, item._id))}
                        disabled={item.quantity === item.inStock ? true : false}
                        className="btn align-items-center hover-white text-center
                        justify-content-center d-flex">
                        <i className="fas fa-plus fa-sm hover-white"></i>
                    </button>

                    {/* DECREMENTAR */}
                    <button onClick={() => dispatch(decrease(cart, item._id))}
                        disabled={item.quantity === 1 ? true : false}
                        className="btn align-items-center hover-white text-center
                        justify-content-center d-flex">
                        <i className="fas fa-minus fa-sm hover-white"></i>
                    </button>
                </div>
            </div>
            <div className="price-field sub-total">
                <p className="price">
                    {
                        selectedCurrency.selected == 'cop'
                            ? currencyFormat((item.quantity * item.price))
                            : selectedCurrency.selected == 'usd'
                                ? currencyFormat(((item.quantity * item.price) / currencies.usd).toFixed(2), 'en-US', "USD")
                                : ''
                    }
                </p>
            </div>
            <div className="delete">
                <span className="btn align-items-center hover-white text-center
                        justify-content-center d-flex" title="Quitar del carrito"
                    onClick={() => {

                        handleClickOpen()
                        dispatch({
                            type: 'ADD_CONFIRM',
                            payload: [{ data: cart, id: item._id, title: item.title, type: 'ADD_CART' }]
                        })

                    }}
                >
                    {/* <span>Quitar del carrito</span> */}
                    <i className="fas fa-times-circle fa-lg text-danger"></i>
                </span>
            </div>

            <ConfirmMsg isOpen={isOpen} handleClose={handleDialogClose} />
        </li>
    );
}

export default CartItem;
