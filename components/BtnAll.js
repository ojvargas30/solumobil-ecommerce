import React from 'react';
import Button from '@material-ui/core/Button';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { red, blue, lightGreen, green } from '@material-ui/core/colors'

const themes = {
    red: createMuiTheme({ palette: { primary: red } }),
    blue: createMuiTheme({ palette: { primary: blue } }),
    green: createMuiTheme({ palette: { primary: green } }),
    lightGreen: createMuiTheme({ palette: { primary: lightGreen } })
}

const BtnAll = (props) => {
    return (
        <>
            <MuiThemeProvider theme={
                (themes.hasOwnProperty(props.bgcolor))
                    ? themes[props.bgcolor]
                    : themes[blue]
            }>
                <div className="centerBtn align-items-center text-center">
                    <Button variant="contained" color="primary" className={props.classN}>
                        {props.text}
                    </Button>
                </div>
            </MuiThemeProvider>
        </>
    );
}

export default BtnAll;
