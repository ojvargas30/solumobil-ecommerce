import ResponsiveTable from 'material-ui-next-responsive-table'

const RTable = ({ data, columns }) => {

    console.log('TABLE DATA AND COLUMNS');
    console.log(data);
    console.log(columns);
    console.log('END TABLE DATA AND COLUMNS');

    return (
        <ResponsiveTable
            data={data}
            columns={columns}
        />
    );
}

export default RTable;
