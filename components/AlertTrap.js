const AlertTrap = ({ type, msg, children }) => {
    return (
        <div className={`alert alert-${type} d-flex justify-content-between align-items-center`}
            role="alert">{msg}
            {children ? children : ''}
        </div>
    );
}

export default AlertTrap;
