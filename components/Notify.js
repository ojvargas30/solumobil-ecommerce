import { useContext } from 'react';
import { DataContext } from '../store/GlobalState'
import Loading from './Loading'
import Snack from './Snack'

const Notify = () => {

    const { state, dispatch } = useContext(DataContext),
        { notify } = state;

    return (
        <>
            {notify.loading && <Loading />}
            {notify.error &&
                <Snack
                    msg={notify.error}
                    bgColor="error"
                    handleClose={() => dispatch({ type: 'NOTIFY', payload: {} })}

                />}
            {notify.warning &&
                <Snack
                    msg={notify.warning}
                    bgColor="warning"
                    handleClose={() => dispatch({ type: 'NOTIFY', payload: {} })}

                />}
            {notify.info &&
                <Snack
                    msg={notify.info}
                    bgColor="info"
                    handleClose={() => dispatch({ type: 'NOTIFY', payload: {} })}

                />}
            {notify.success &&
                <Snack
                    msg={notify.success}
                    bgColor="success"
                    handleClose={() => dispatch({ type: 'NOTIFY', payload: {} })}
                />
            }
        </>
    );
}

export default Notify;
