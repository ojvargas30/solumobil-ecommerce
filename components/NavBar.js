import { useContext } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { DataContext } from '../store/GlobalState'
import Cookie from 'js-cookie'
// import AlertConfirm from './AlertConfirm'
import Filter from './Filter'
import TooltipMsg from './TooltipMsg'
// import { keepSelectedCurrency } from '../store/Actions'

const NavBar = () => {

    const router = useRouter(),
        { state, dispatch } = useContext(DataContext), // Usar el contexto Global

        { auth, cart, selectedCurrency } = state,

        handleLogout = () => {
            // const confirm = window.confirm("¿Quieres cerrar sesión?");

            // if (confirm) {
            Cookie.remove('refreshtoken', { path: 'api/auth/accessToken' })
            localStorage.removeItem('firstLogin')
            dispatch({ type: "AUTH", payload: {} })
            dispatch({ type: "NOTIFY", payload: { success: "Cierre de sesión existoso" } })

            return router.push('/')
            // }
        },

        adminRouter = () => {
            return (
                <>
                    <li className="menu-item" >
                        <Link href="/users">
                            <a title="Usuarios"> <i className="fas fa-user-edit"></i> Usuarios</a>
                        </Link>
                    </li>

                    <li className="menu-item" >
                        <Link href="/create">
                            <a title="Productos"> <i className="fas fa-box"></i> Productos</a>
                        </Link>
                    </li>

                    <li className="menu-item" >
                        <Link href="/categories">
                            <a title="Categorías"> <i className="fas fa-mobile"></i> Categorías</a>
                        </Link>
                    </li>

                    <li className="menu-item" >
                        <Link href="/brands">
                            <a title="Marcas"> <i className="fab fa-xbox"></i> Marcas</a>
                        </Link>
                    </li>
                </>
            );
        },

        isActive = r => { return r === router.pathname ? " active" : "" },

        loggedRouter = () => {
            return (
                <li className="menu-item lang-menu menu-item-has-children parent">
                    <Link href="#">
                        <a title="Usuario Nombre" className="d-flex align-items-center">
                            <span className="img label-before">
                                <img className="rounded-circle"
                                    src={(auth.user) ? auth.user.avatar : ''}
                                    alt={(auth.user) ? auth.user.avatar : ''} width="22" />
                            </span>
                            {(auth.user) ? auth.user.name : ''}
                        </a>
                    </Link>
                    <ul className="submenu lang" >

                        <li className="menu-item" >
                            <Link href="/profile">
                                <a title="Perfil">
                                    <i className="fas fa-store"></i> Perfil y Ordenes
                                </a>
                            </Link>
                        </li>
                        {
                            auth.user.role === 'admin' && adminRouter()
                        }
                        <li className="menu-item" >
                            <a href="#" onClick={handleLogout}>
                                <i className="fas fa-sign-out-alt"></i> Salir
                            </a>
                        </li>
                    </ul>
                </li>
            )
        },

        changeCurrency = (currency, txtCurrency) => {
            if (currency != selectedCurrency.selected) {
                // dispatch(keepSelectedCurrency(currency))
                dispatch({ type: 'CHANGE_CURRENCY', payload: { selected: currency } })

                return dispatch({ type: 'NOTIFY', payload: { success: `Moneda cambiada a ${txtCurrency}` } })
            }
        }

    return (
        <div>
            <Link href="/cart">
                <a>
                    <div className="cart-rubber-signal cursor-pointer">
                        <i className="fas fa-shopping-cart p-3 fa-3x solumobil-red position-relative cart_rubber_icon">
                            <span
                                className="cart-rubber-badge position-absolute badge border border-light rounded-pill">
                                {cart.length}
                                {/* <span class="visually-hidden">unread messages</span> */}
                            </span>
                        </i>
                    </div>
                </a>
            </Link>

            {/* <!-- mobile menu --> */}
            <div className="mercado-clone-wrap">
                <div className="mercado-panels-actions-wrap">
                    <Link href="#">
                        <a className="mercado-close-btn mercado-close-panels text-decoration-none text-lg fas fa-times" ></a>
                    </Link>
                </div>
                <div className="mercado-panels"></div>
            </div>

            {/* <!--START header--> */}
            <header id="header" className="header header-style-1 mb-3">
                <div className="container-fluid">
                    <div className="row px-0">
                        <div className="topbar-menu-area">
                            <div className="container">
                                <div className="topbar-menu left-menu">
                                    <ul>
                                        <li className="menu-item" >
                                            <Link href="/">
                                                <a title="Línea de atención: (+57) 3208557457 - (+57) 3133043714" target="_blank"
                                                    href="https://api.whatsapp.com/send?phone=573208557457&text=%C2%A1Saludos! vengo de la web y quisiera saber mas de sus productos." >
                                                    <span className="icon label-before fa fa-mobile fa-xs"></span>
                                                    Línea de atención: (+57) 3208557457
                                                </a>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                                <div className="topbar-menu right-menu">
                                    <ul>
                                        {
                                            Object.keys(auth).length === 0
                                                ?
                                                <>
                                                    <li className="menu-item" >
                                                        <Link href="/signin">
                                                            <a className={isActive('signin')} title="Iniciar Sesión">
                                                                Iniciar Sesión
                                                            </a>
                                                        </Link>
                                                    </li>

                                                    <li className="menu-item">
                                                        <Link href="/signup">
                                                            <a className={isActive('signup')} title="Registrarse">
                                                                Registrarse
                                                            </a>
                                                        </Link>
                                                    </li>
                                                </>
                                                : loggedRouter()
                                        }

                                        <TooltipMsg text={`Tu carrito Solumobil (${cart.length})`} placement="bottom" element={
                                            <li className="menu-item">
                                                <Link href="/cart">
                                                    <a title="Carrito Solumobil">
                                                        <i className="fas fa-shopping-cart fa-lg position-relative"></i>
                                                    </a>
                                                </Link>
                                            </li>
                                        } />

                                        <li className="menu-item lang-menu menu-item-has-children parent">
                                            <a title="Español" className="d-flex align-items-center cursor-pointer">
                                                <span className="img label-before">
                                                    <img src="/media/icon/countriesFlags/co.ico" alt="lang-es" width="21" />
                                                </span>
                                                Español
                                                <i className="fa fa-angle-down" aria-hidden="true"></i>
                                            </a>
                                            <ul className="submenu lang" >
                                                <li className="menu-item" >
                                                    <a title="Español" className="d-flex align-items-center cursor-pointer">
                                                        <span className="img label-before">
                                                            <img src="/media/icon/countriesFlags/co.ico" alt="lang-es" width="21" />
                                                        </span>
                                                        Español
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="menu-item menu-item-has-children parent" >
                                            <Link href="/">
                                                <a title={
                                                    selectedCurrency.selected == "cop"
                                                        ? "Peso Colombiano (COP)"
                                                        : selectedCurrency.selected == "usd"
                                                            ? "Dolar (USD)"
                                                            : ""
                                                } href="#"
                                                >
                                                    {
                                                        selectedCurrency.selected == "cop"
                                                            ? "Peso Colombiano (COP)"
                                                            : selectedCurrency.selected == "usd"
                                                                ? "Dolar (USD)"
                                                                : ""
                                                    }
                                                </a>
                                            </Link>
                                            <ul className="submenu curency">
                                                {
                                                    selectedCurrency.selected != "cop" && <li className="menu-item cursor-pointer">
                                                        <a title="Peso Colombiano (COP)"
                                                            onClick={() => changeCurrency("cop", "Peso Colombiano (COP)")}>
                                                            Peso Colombiano (COP)
                                                        </a>
                                                    </li>
                                                }

                                                {
                                                    selectedCurrency.selected != "usd" && <li className="menu-item cursor-pointer">
                                                        <a title="Dolar (USD)"
                                                            onClick={() => changeCurrency("usd", "Dolar (USD)")}>
                                                            Dolar (USD)
                                                        </a>
                                                    </li>
                                                }


                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        {/* BUSCADOR CON FILTROS */}
                        <Filter state={state} />
                        {/* END BUSCADOR CON FILTROS */}

                    </div>
                </div >
            </header >
            {/* END HEADER */}

        </div >
    );
}

export default NavBar;
