import Link from 'next/link'
import AlertTrap from './AlertTrap'
import { getEsDate } from '../utils/funcs'
import PaypalBtn from './paypalBtn'
import { patchData } from '../utils/fetchData'
import { updateItem } from '../store/Actions'
import { currencyFormat } from '../utils/funcs'

const OrderDetail = ({ orderDetail, state, dispatch }) => {

    const { auth, orders, currencies, selectedCurrency } = state

    const handleDelivered = (order) => {
        dispatch({ type: 'NOTIFY', payload: { loading: true } })

        // Actualizar el campo delivered de la orden
        patchData(`order/delivered/${order._id}`, null, auth.token)
            .then(res => {

                if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

                console.log(res)

                const { paid, delivered, dateOfPayment, method } = res.result

                // Actualiza la info de solo la orden que se le pasa con res.result
                dispatch(updateItem(orders, order._id, {
                    ...order, paid, delivered, dateOfPayment, method
                }, 'ADD_ORDERS'))

                return dispatch({ type: 'NOTIFY', payload: { success: res.msg } })
            })
    }

    console.log(orderDetail)

    if (!auth.user) return null;

    return (
        <>
            {/* Detalle de la orden */}
            {
                orderDetail.map(order => (
                    <div key={order._id} style={{ margin: '20px auto' }} className="row justify-content-center">
                        <div className={`text-uppercase my-3 ${!order.paid ? 'col-md-6' : ''}`} style={{ maxWidth: '600px' }}>
                            <h2 className="text-break">Orden&nbsp;
                                <small>
                                    {order._id}
                                </small>
                            </h2>

                            <div className="mt-4 text-secondary">
                                <h3>Envío</h3>

                                <p>Nombre: {order.user ? order.user.name : <span className="text-danger">Sin nombre</span>}</p>
                                <p>Correo: {order.user ? order.user.email : ''}</p>
                                <p>Dirección: {order.address ? order.address : ''}</p>
                                <p>Teléfono: {order.mobile ? order.mobile : ''}</p>

                                <AlertTrap
                                    type={order.delivered ? "success" : "danger"}
                                    msg={
                                        order.delivered
                                            ? `Entregado el ${getEsDate(order.updatedAt)}`
                                            : 'Sin entregar'
                                    }
                                    children={
                                        auth.user.role === 'admin' && !order.delivered &&

                                        <button
                                            className="btn btn-dark text-uppercase"
                                            onClick={() => handleDelivered(order)}>
                                            Marcar como entregado
                                        </button>
                                    }
                                />

                                <h3>Pago</h3>
                                {
                                    order.method && <h6>Método: <em>{order.method}</em> </h6>
                                }

                                {
                                    order.paymentId && <p>Id: <em>{order.paymentId}</em> </p>
                                }

                                {/* Pagado o no pagado */}
                                <AlertTrap
                                    type={order.paid ? "success" : "danger"}
                                    msg={
                                        order.paid
                                            ? `Pagado el ${getEsDate(order.dateOfPayment)}`
                                            : 'Sin pagar'
                                    }
                                />

                                <div>
                                    <h4>Artículos del pedido</h4>

                                    {/* Recorremos los articulos del pedido */}
                                    {
                                        order.cart.map(item => (
                                            <div
                                                key={item._id}
                                                style={{ maxWidth: "550px" }}
                                                className="row border-bottom mx-0 p-2 justify-content-between align-items-center">

                                                <img
                                                    src={item.images[0].url}
                                                    alt={item.images[0].url}
                                                    style={{ width: '60px', height: '55px', objectFit: 'cover' }}
                                                />

                                                <h5 className="flex-fill text-secondary px-3 m-0"
                                                    style={{ display: 'contents' }}>
                                                    <Link href={`/product/${item._id}`}>
                                                        <a>{item.title}</a>
                                                    </Link>
                                                </h5>

                                                <span
                                                    className="text-blue m-0"
                                                    style={{ display: 'contents' }}>

                                                    {item.quantity} x  {
                                                        selectedCurrency.selected == 'cop'
                                                            ? currencyFormat(item.price)
                                                            : selectedCurrency.selected == 'usd'
                                                                ? currencyFormat((item.price / currencies.usd).toFixed(2), 'en-US', "USD")
                                                                : ''
                                                    } =  {
                                                        selectedCurrency.selected == 'cop'
                                                            ? currencyFormat((item.quantity * item.price)) + "COP"
                                                            : selectedCurrency.selected == 'usd'
                                                                ? currencyFormat(((item.quantity * item.price) / currencies.usd).toFixed(2), 'en-US', "USD") + "USD"
                                                                : ''
                                                    }
                                                </span>
                                            </div>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>

                        <div className={`${!order.paid && auth.user.role !== 'admin' ? 'col-md-6' : ''}`}>
                            {
                                !order.paid && auth.user.role !== 'admin' &&
                                <div className="p-4">
                                    <h2 className="mb-4 text-uppercase">Total: {
                                        selectedCurrency.selected == 'cop'
                                            ? currencyFormat(order.total) + "COP"
                                            : selectedCurrency.selected == 'usd'
                                                ? currencyFormat((order.total / currencies.usd).toFixed(2), 'en-US', "USD") + "USD"
                                                : ''
                                    }</h2>
                                    <PaypalBtn currency="USD" order={order} />
                                </div>
                            }
                        </div>
                    </div>
                ))
            }
        </>
    );
}

export default OrderDetail;