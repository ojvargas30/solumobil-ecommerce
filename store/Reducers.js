import { ACTIONS } from './Actions'

const reducers = (state, action) => {
    switch (action.type) {
        case ACTIONS.NOTIFY:
            return {
                ...state,
                notify: action.payload // Si viene vacio no ejecuta la alerta
            }
        case ACTIONS.AUTH:
            return {
                ...state,
                auth: action.payload
            }
        case ACTIONS.ADD_CART:
            return {
                ...state,
                cart: action.payload
            };
        case ACTIONS.ADD_MODAL:
            return {
                ...state,
                modal: action.payload
            };
        case ACTIONS.ADD_CONFIRM:
            return {
                ...state,
                confirm: action.payload
            }
        case ACTIONS.ADD_ORDERS:
            return {
                ...state,
                orders: action.payload
            }
        case ACTIONS.ADD_USERS:
            return {
                ...state,
                users: action.payload
            }
        case ACTIONS.ADD_CATEGORIES:
            return {
                ...state,
                categories: action.payload
            }
        case ACTIONS.ADD_BRANDS:
            return {
                ...state,
                brands: action.payload
            }
        case ACTIONS.ADD_CURRENCIES:
            return {
                ...state,
                currencies: action.payload
            }
        case ACTIONS.CHANGE_CURRENCY:
            return {
                ...state,
                selectedCurrency: action.payload
            }
        default:
            return state;
    }
}

export default reducers