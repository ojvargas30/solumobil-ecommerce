// import { useContext } from 'react'
// import { DataContext } from './GlobalState'

export const ACTIONS = {
    NOTIFY: 'NOTIFY',
    AUTH: 'AUTH',
    ADD_CART: 'ADD_CART',
    ADD_CONFIRM: 'ADD_CONFIRM',
    ADD_MODAL: 'ADD_MODAL',
    ADD_ORDERS: 'ADD_ORDERS',
    ADD_USERS: 'ADD_USERS',
    ADD_CATEGORIES: 'ADD_CATEGORIES',
    ADD_BRANDS: 'ADD_BRANDS',
    ADD_CURRENCIES: 'ADD_CURRENCIES',
    CHANGE_CURRENCY: 'CHANGE_CURRENCY',
}

// export const keepSelectedCurrency = currency => {

//     // localStorage.setItem('currency', currency)

//     return ({ type: 'CHANGE_CURRENCY', payload: { selected: currency } })
// }

export const addToCart = (product, cart, type) => {

    // const { dispatch } = useContext(DataContext)

    if (product.inStock === 0)
        return ({ type: 'NOTIFY', payload: { [type]: 'Este producto esta fuera de Stock' } })

    // Validar que no se encuentre ya en el carrito
    const check = cart.every(item => {
        return item._id !== product._id
    })

    if (!check)
        return ({
            type: 'NOTIFY',
            payload: { [type]: 'El producto ya ha sido añadido al carrito' }
        })

    // dispatch({ type: 'NOTIFY', payload: { success: `Se añadio el producto al carrito` } })

    return ({ type: 'ADD_CART', payload: [...cart, { ...product, quantity: 1 }], endsuccess: true })
}

export const decrease = (data, id) => {

    const newData = [...data]

    newData.forEach(item => {
        if (item._id === id && item.quantity > 1) item.quantity -= 1
    })

    return ({ type: 'ADD_CART', payload: newData })
}

export const increase = (data, id) => {

    const newData = [...data]

    newData.forEach(item => {
        if (item._id === id) item.quantity += 1
    })

    return ({ type: 'ADD_CART', payload: newData })
}

// QUITAR SOLO UN ARTICULO DEL CARRITO
export const deleteItem = (globalStateData, id, type) => {
    const newData = globalStateData.filter(item => item._id !== id)
    return ({ type, payload: newData })
}

// Actualiza la info del elemento por id que se le pasa con res.result (FETCH)
export const updateItem = (globalStateData, id, backendResultToUpdateView, type) => {
    const newData = globalStateData.map(item => (item._id === id ? backendResultToUpdateView : item))
    return ({ type, payload: newData })
}