import { createContext, useReducer, useEffect } from 'react'
import reducers from './Reducers'
import { getData } from '../utils/fetchData'
import { convertCurrency } from '../utils/funcs'

export const DataContext = createContext()

/*
- PARA FETCH SIEMPRE EN SINGULAR LAS PETICIONES

*/

export const DataProvider = ({ children }) => {

    const initialState = {
        notify: {},
        auth: {},
        cart: [],
        confirm: [],
        modal: [],
        orders: [],
        users: [],
        categories: [],
        brands: [],
        currencies: {},
        selectedCurrency: {},
    };

    const [state, dispatch] = useReducer(reducers, initialState)
    const { cart, auth, selectedCurrency } = state

    // moneda
    useEffect(() => {
        const currency = localStorage.getItem('currency')

        if (currency)
            dispatch({ type: 'CHANGE_CURRENCY', payload: { selected: currency } })
        else if (!currency || currency == undefined)
            dispatch({ type: 'CHANGE_CURRENCY', payload: { selected: 'cop' } })
    }, [])

    useEffect(() => {

        localStorage.setItem('currency', selectedCurrency.selected ? selectedCurrency.selected : 'cop')
    }, [selectedCurrency])
    // END moneda

    // OBTENER VALOR DEL DOLAR USD para pago PAYPAL
    useEffect(() => {
        // Entonces, se multiplica la cantidad de pesos que deseas convertir a dólares
        //  por el valor de 1 peso en dólares.
        // Para la conversión de 23 pesos, en nuestro ejemplo, debes multiplicar 23 veces 0,082
        // async function fetchExchangeAPI() {
        //     dispatch({ type: 'ADD_CURRENCIES', payload: { usd: await convertCurrency(1, 'COP', 'USD') } })
        // }

        // fetchExchangeAPI()
        // https://www.cuidatudinero.com/13087701/como-convertir-pesos-a-dolares
        (async () => {
            // dispatch({ type: 'ADD_CURRENCIES', payload: { usd: await convertCurrency(1, 'COP', 'USD') } })
            dispatch({ type: 'ADD_CURRENCIES', payload: { usd: await convertCurrency(1, 'USD', 'COP') } })
        })()

        // const localCurrency = localStorage.getItem("currency")

        // dispatch({ type: 'CHANGE_CURRENCY', payload: { selected: localCurrency } })

    }, [])

    // LOGUEO Y TRAER CATEGORÍAS
    useEffect(() => {
        const firstLogin = localStorage.getItem("firstLogin")

        if (firstLogin) {
            getData('auth/accessToken').then(res => {

                if (res.err) return localStorage.removeItem("firstLogin")

                dispatch({
                    type: "AUTH",
                    payload: {
                        token: res.access_token,
                        user: res.user
                    }
                })
            })
        }

        getData('category').then(res => {

            if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

            // console.log(res.categories);
            dispatch({
                type: "ADD_CATEGORIES",
                payload: res.categories
            })
        })

        getData('brand').then(res => {

            if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } }) // erro del back

            // console.log(res.categories);
            dispatch({
                type: "ADD_BRANDS",
                payload: res.brands
            })
        })
    }, [])

    // CARRITO
    useEffect(() => {
        // Obtenemos la info del carrito de compra del LocalStorage y la mostramos
        const __next__cart01__vargas = JSON.parse(localStorage.getItem('__next__cart01__vargas'))


        if (__next__cart01__vargas)
            dispatch({ type: 'ADD_CART', payload: __next__cart01__vargas })

    }, [])

    useEffect(() => {
        // Guardamos la info del carrito de compra en el LocalStorage
        localStorage.setItem('__next__cart01__vargas', JSON.stringify(cart))
    }, [cart])
    // END CARRITO

    // OBTENER ORDENES Y USUARIOS
    useEffect(() => {
        if (auth.token) {
            getData('order', auth.token)
                .then(res => {
                    if (res.err)
                        return dispatch({ type: 'NOTIFY', payload: { error: res.err } })

                    // Despachamos las ordenes y las ponemos en el estado Global - DataProvider Component
                    dispatch({ type: 'ADD_ORDERS', payload: res.orders })
                })

            // OBTENER USUARIOS
            if (auth.user.role === 'admin') {
                getData('user', auth.token)
                    .then(res => {
                        if (res.err) return dispatch({ type: 'NOTIFY', payload: { error: res.err } })

                        // console.log(res)
                        // Despachamos las USUARIOS y las ponemos en el estado Global - DataProvider Component
                        dispatch({ type: 'ADD_USERS', payload: res.users })
                    })
            }
            // END OBTENER USUARIOS
        } else {
            dispatch({ type: 'ADD_ORDERS', payload: [] })
            dispatch({ type: 'ADD_USERS', payload: [] })
        }
    }, [auth.token]) // Variables externas necesarias
    // END ORDENES Y USUARIOS

    return (
        < DataContext.Provider value={{ state, dispatch }} >
            {children}
        </ DataContext.Provider>
    )
}