module.exports = {
    // api: {
    //     bodyParser: {
    //         sizeLimit: '1mb',
    //     },
    // },
    env: {
        "CLIENT_URL": process.env.BASE_URL, // https://ecommerce.solumobil.com - http://localhost:3000 window.location.hostname - https://solumobil-ecommerce2.vercel.app

        // MONGO CONNECTION
        // MONGODB PRODUCTION
        // "MONGODB_URL": "mongodb+srv://dovj:holaqueHace1@cluster0.dt7xa.mongodb.net/solumobil?retryWrites=true&w=majority",
        // END MONGODB PRODUCTION

        // MONGODB LOCALHOST
        "MONGODB_URL": process.env.MONGODB_URL,
        // END MONGODB LOCALHOST
        // END MONGO CONNECTION

        // TOKENS
        "ACTIVATE_ACCOUNT_TOKEN_SECRET": process.env.ACTIVATE_ACCOUNT_TOKEN_SECRET,
        "ACCESS_TOKEN_SECRET": process.env.ACCESS_TOKEN_SECRET,
        "REFRESH_TOKEN_SECRET": process.env.REFRESH_TOKEN_SECRET,
        // END TOKENS

        // PAYPAL KEYS
        "PAYPAL_CLIENT_ID": process.env.PAYPAL_CLIENT_ID,
        // END PAYPAL KEYS

        // CLOUDINARY API
        "CLOUD_UPDATE_PRESET": process.env.CLOUD_UPDATE_PRESET,

        "CLOUD_NAME": process.env.CLOUD_NAME,

        "CLOUD_API": process.env.CLOUD_API,
        // END CLOUDINARY API

        // NODEMAILER KEYS
        // "MAIL_PASSWORD": process.env.MAIL_PASSWORD "Hotdompro092_",
        // NODEMAILER KEYS

        // SENDGRID MAILER KEYS
        "MAIL_USERNAME": process.env.MAIL_USERNAME,
        "MAIL_CLIENTID": process.env.MAIL_CLIENTID,
        "MAIL_SECRET": process.env.MAIL_SECRET,
        "SENDGRID_APIKEY": process.env.SENDGRID_APIKEY,
        // END SENDGRID MAILER KEYS

        // OAUTH KEYS
        "OAUTH_CLIENTID": process.env.OAUTH_CLIENTID,
        "OAUTH_SECRET": process.env.OAUTH_SECRET,
        // END OAUTH KEYS

        // SOCIAL KEYS
        "GOOGLE_SECRET": process.env.GOOGLE_SECRET,
        "FB_SECRET": process.env.FB_SECRET,
        // END SOCIAL KEYS

        // GOOGLE ANALYTICS
        "NEXT_PUBLIC_GOOGLE_ANALYTICS": process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS,
        // END GOOGLE ANALYTICS

        // VONAGE
        "VONAGE_KEY": process.env.VONAGE_KEY,
        "VONAGE_SECRET": process.env.VONAGE_SECRET
        // END VONAGE

        // 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
        // "MAIL_USERNAME": "oscar.vargas.desarrollo@solumobil.com",
        // "MAIL_PASSWORD": "5?Thui69"
        // "MAIL_USERNAME": "adtestjavis@gmail.com",
        // "MAIL_PASSWORD": "delfines_123",
        // "DB_USER": "ecom",
        // "DB_PASS": "xN87js5%",
        // "DB_NAME": "esolumobil",
        // "DB_HOST": "ecommerce.solumobil.com",
    },
    // webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    //     config.plugins.push(new webpack.IgnorePlugin(/^pg-native$/));
    //     config.node = {
    //         ...(config.node || {}),
    //         net: 'empty',
    //         tls: 'empty',
    //         dns: 'empty',
    //         fs: 'empty',
    //     };
    //     return config;
    // },
};

/**
 * 1. Los modelos deben llevar el campo isActive para SoftDelete
 *
 *
 *
 *
 *
 *
 *
 *
*/
